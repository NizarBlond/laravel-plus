# Change Log

## [2.0.0] - 2020-05-15

* Remove Dingo API (causes JWT problems in 7.x)
* Add Lumen 7.x support