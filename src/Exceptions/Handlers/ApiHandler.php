<?php

namespace NizarBlond\LaravelPlus\Exceptions\Handlers;

use NizarBlond\LaravelPlus\Traits\RestApiHelpers;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Throwable;

class ApiHandler extends ExceptionHandler
{
    use RestApiHelpers;

    /**
     * The http request.
     *
     * @var \Illuminate\Http\Request
     */
    private $_request;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->_request = app('request');
    }

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $e
     * @return void
     */
    public function report(Throwable $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request, Throwable $e)
    {
        if ($this->isApiRequest($this->_request)) {
            return $this->getJsonResponseForException($this->_request, $e);
        }

        $this->logWarning(
            sprintf(
                "A non-API request exception detected: uri=%s, error=%s",
                $this->_request ? $this->_request->getUri() : 'n/a',
                $e->getMessage()
            )
        );

        return $this->badRequest(
            'Whoops! Something went wrong.',
            'client',
            $e->getCode(),
            get_class($e),
            $e->status ?? 400
        );
    }
}
