<?php

namespace NizarBlond\LaravelPlus\Exceptions;

use Exception as PhpException;

class Exception extends PhpException
{
    /**
     * The exception data field.
     *
     * @var mixed
     */
    protected $data;

    /**
     * The exception constructor.
     *
     * Reference:
     * https://www.php.net/manual/en/class.exception.php
     *
     * @param string|Exception $error
     * @param mixed $data
     * @param Exception $previous
     * @return void
     */
    public function __construct($error, $data = [], $code = 0, PhpException $previous = null)
    {
        $message = $error;
        if ($error instanceof PhpException) {
            $message = $error->getMessage();
        }

        if (!empty($data)) {
            $message = sprintf(
                '%s (data=%s)',
                $message,
                is_string($data) ? $data : json_encode($data)
            );
        }

        parent::__construct($message, $code, $previous);

        $this->data = $data;
    }

    /**
     * Returns the exception data.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Throws an exception of the inherited type.
     *
     * @param string|Exception $error
     * @param mixed $data
     * @return void
     * @throws ExceptionBase
     */
    public static function throw($error, $data = null)
    {
        throw new static($error, $data);
    }
}
