<?php

namespace NizarBlond\LaravelPlus\Clients;

use NizarBlond\LaravelPlus\Abstracts\HttpClientBase;
use NizarBlond\LaravelPlus\Support\Req;
use Exception;

class CloudflareClient extends HttpClientBase
{
    /**
     * The account token.
     *
     * @var string
     */
    private $accountToken;

    /**
     * The base url.
     */
    const BASE_URL = 'https://api.cloudflare.com/client/v4';

    /**
     * Constructor.
     */
    public function __construct(string $token, $debug = false)
    {
        $this->accountToken = $token;
        parent::__construct(null, $debug);
    }

    /**
     * Lists all accounts.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/accounts-list-accounts
     *
     * @return array
     */
    public function listAccounts()
    {
        return $this->makeGetApiRequest('/accounts');
    }

    /**
     * Verifies token.
     *
     * Reference:
     * https://developers.cloudflare.com/api/operations/user-api-tokens-verify-token
     *
     * @return array
     */
    public function verifyUserToken()
    {
        return $this->makeGetApiRequest('/user/tokens/verify');
    }

    /**
     * Get token details.
     *
     * Reference:
     * https://developers.cloudflare.com/api/operations/user-api-tokens-token-details
     *
     * @return array
     */
    public function getTokenDetails(string $tokenId)
    {
        return $this->makeGetApiRequest('/user/tokens/' . $tokenId);
    }

    /**
     * Lists user tokens.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/user-api-tokens-list-tokens
     *
     * @return array
     */
    public function listUserTokens()
    {
        return $this->makeGetApiRequest('/user/tokens');
    }

    /**
     * Creates a user token.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/user-api-tokens-create-token
     *
     * @return array
     */
    public function createUserToken(array $params)
    {
        return $this->makePostApiRequest('/user/tokens', $params);
    }

    /**
     * Rolls a user token.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/user-api-tokens-roll-token
     * 
     * @param string $tokenId
     * @return array
     */
    public function rollUserToken(string $tokenId)
    {
        return $this->makePutApiRequest('/user/tokens/' . $tokenId . '/value');
    }

    /**
     * Deletes user token.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/user-api-tokens-delete-token
     * 
     * @param string $tokenId
     * @return array
     */
    public function deleteUserToken(string $tokenId)
    {
        return $this->makeDeleteApiRequest('/user/tokens/' . $tokenId);
    }

    /**
     * Gets zone settings.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/zone-settings-get-all-zone-settings
     *
     * @param string $zoneId
     * @return array
     */
    public function getZoneSettings(string $zoneId)
    {
        return $this->makeGetApiRequest('/zones/' . $zoneId . '/settings');
    }

    /**
     * Updates zone settings.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/zone-settings-edit-zone-settings-info
     *
     * @param string $zoneId
     * @param array $settings
     * @return array
     */
    public function updateZoneSettings(string $zoneId, array $settings)
    {
        $items = [];
        foreach ($settings as $key => $value) {
            $_value = $value === 'yes' ? 'on' : ($value === 'no' ? 'off' : $value);
            if (is_numeric($value)) {
                $_value = (int) $value;
            }
            $items[] = [ 'id' => $key, 'value' => $_value ];
        }
        return $this->makePatchApiRequest('/zones/' . $zoneId . '/settings', ['items' => $items]);
    }

    /**
     * Updates bot management settings.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/bot-management-for-a-zone-update-config
     *
     * @param string $zoneId
     * @param array $settings
     * @return array
     */
    public function updateZoneBotManagementSettings(string $zoneId, array $settings)
    {
        return $this->makePutApiRequest('/zones/' . $zoneId . '/bot_management', $settings);
    }

    /**
     * Lists permission groups.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/permission-groups-list-permission-groups
     *
     * @return array
     */
    public function listPermissionGroups()
    {
        return $this->makeGetApiRequest('/user/tokens/permission_groups');
    }

    /**
     * Reruns zone activation check.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/put-zones-zone_id-activation_check
     *
     * @param string $zoneId
     * @return array
     */
    public function rerunZoneActivationCheck(string $zoneId)
    {
        return $this->makePutApiRequest('/zones/' . $zoneId . '/activation_check');
    }

    /**
     * Purges zone cache.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/zone-purge
     * 
     * @param string $zoneId
     * @param array $prefixes
     * @return array
     */
    public function purgeZoneCache(string $zoneId, array $prefixes = [])
    {
        $params = empty($prefixes) ? ['purge_everything' => true] : ['prefixes' => $prefixes];
        return $this->makePostApiRequest('/zones/' . $zoneId . '/purge_cache', $params);
    }

    /**
     * Creates a zone.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/post-zones
     * 
     * @param array $params
     * @return array
     */
    public function createZone(array $params = [])
    {
        Req::validInputOrFail($params, [
            'account_id'    => 'required|string',
            'name'          => 'required|string',
            'type'          => 'required|string|in:full',
        ]);
        return $this->makePostApiRequest('/zones', $params);
    }

    /**
     * Gets zone details.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/zones-0-get
     *
     * @param string $zoneId
     * @return array
     */
    public function getZoneDetails(string $zoneId)
    {
        return $this->makeGetApiRequest('/zones/' . $zoneId);
    }

    /**
     * Deletes a zone.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/zones-0-delete
     *
     * @param string $zoneId
     * @return array
     */
    public function deleteZone(string $zoneId)
    {
        return $this->makeDeleteApiRequest('/zones/' . $zoneId);
    }

    /**
     * Lists all zones.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/zones-get
     *
     * @param array $params
     * @return array
     */
    public function listZones(array $params = [])
    {
        return $this->makeGetApiRequest('/zones', $params);
    }

    /**
     * Lists zone DNS records.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/dns-records-for-a-zone-list-dns-records
     * 
     * @param string $zoneId
     * @return array
     */
    public function listZoneDNSRecords(string $zoneId)
    {
        return $this->makeGetApiRequest('/zones/' . $zoneId . '/dns_records');
    }
    
    /**
     * Creates a DNS record.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/create-dns-record
     * 
     * @param string $zoneId
     * @param array $params
     * @return array
     */
    public function createDNSRecord(string $zoneId, array $params)
    {
        Req::validInputOrFail($params, [
            'type'      => 'required|string',
            'name'      => 'required|string',
            'content'   => 'required|string',
        ]);
        return $this->makePostApiRequest('/zones/' . $zoneId . '/dns_records', $params);
    }

    /**
     * Updates a DNS record.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/dns-records-for-a-zone-patch-dns-record
     * 
     * @param string $zoneId
     * @param string $dnsRecordId
     * @param array $params
     * @return array
     */
    public function updateDNSRecord(string $zoneId, string $dnsRecordId, array $params)
    {
        Req::validInputOrFail($params, [
            'type'      => 'required|string',
            'name'      => 'required|string',
            'content'   => 'required|string',
            'ttl'       => 'nullable|integer',
            'proxied'   => 'nullable|boolean',
            'comment'   => 'nullable|string',
        ]);
        return $this->makePatchApiRequest('/zones/' . $zoneId . '/dns_records/' . $dnsRecordId, $params);
    }

    /**
     * Deletes a DNS record.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/delete-dns-record
     * 
     * @param string $zoneId
     * @param string $recordId
     * @return array
     */
    public function deleteDNSRecord(string $zoneId, string $recordId)
    {
        return $this->makeDeleteApiRequest('/zones/' . $zoneId . '/dns_records/' . $recordId);
    }

    /**
     * Fetches all Cache Rules in a zone.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/cache-rules-list-cache-rules
     *
     * @param string $zoneId
     * @return array
     */
    public function listZoneCacheRules(string $zoneId)
    {
        return $this->makeGetApiRequest('/zones/' . $zoneId . '/rulesets/phases/http_request_cache_settings/entrypoint');
    }

    /**
     * Updates Cache Rules in a zone.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/cache-rules-update-cache-rules
     * 
     * @param string $zoneId
     * @param array $rules
     * @return array
     */
    public function updateZoneCacheRules(string $zoneId, array $params)
    {
        Req::validInputOrFail($params, [
            'rules.*.action'            => 'required|string',
            'rules.*.expression'        => 'required|string',
            'rules.*.description'       => 'required|string',
            'rules.*.enabled'           => 'nullable|boolean',
            'rules.*.action_parameters' => 'nullable|array',
        ]);
        return $this->makePutApiRequest('/zones/' . $zoneId . '/rulesets/phases/http_request_cache_settings/entrypoint', $params);
    }

    /**
     * Sets zone under attack mode.
     * 
     * Reference:
     * https://developers.cloudflare.com/api/operations/zone-settings-edit-zone-settings-info
     * 
     * @param string $zoneId
     * @param string $level
     * @return array
     */
    public function setZoneSecurityLevel(string $zoneId, string $level)
    {
        $supportedLevels = ['essentially_off', 'low', 'medium', 'high', 'under_attack'];
        if (!in_array($level, $supportedLevels)) {
            $this->exception('Invalid security level: ' . $level);
        }

        return $this->updateZoneSettings($zoneId, [
            'security_level' => $level,
        ]);
    }

    /**
     * Sends an API request.
     *
     * @param string $uri
     * @param array $method
     * @param array $params
     * @return mixed
     */
    protected function makeRequest($uri, $method, $params = [])
    {
        $apiUrl = self::BASE_URL . '/' . ltrim($uri, '/');
        $params['Accept'] = 'application/json';
        $params['headers'] = [
            'Authorization' => 'Bearer ' . $this->accountToken,
        ];
        try {
            return parent::makeRequest($apiUrl, $method, $params);
        } catch (Exception $e) {
            $errorSummary = $this->getErrorSummary();
            $message = $errorSummary['body']['errors'][0]['message'] ?? $e->getMessage();
            $this->exception('Cloudflare API error: ' . $message);
        }
    }
}
