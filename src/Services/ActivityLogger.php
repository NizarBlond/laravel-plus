<?php

namespace NizarBlond\LaravelPlus\Services;

use NizarBlond\LaravelPlus\Models\ActivityLog;
use NizarBlond\LaravelPlus\Support\Proc;
use NizarBlond\LaravelPlus\Traits\ClassHelpers;
use NizarBlond\LaravelPlus\Config\LaravelPlus as LPConfig;
use NizarBlond\LaravelPlus\Config\ActivityLogger as ALConfig;
use NizarBlond\LaravelPlus\Constants\ErrorLevel;
use NizarBlond\LaravelPlus\Constants\RecordPriority;
use Cache;

class ActivityLogger
{
    use ClassHelpers;

    /**
     * Records an activity to the database.
     *
     * @param integer       $scheduledAt
     * @param integer       $startedAt
     * @param string        $name
     * @param array         $input
     * @param array         $output
     * @param string        $type
     * @param integer       $priority
     * @param array         $logs
     * @param string        $duration
     * @param string        $destination
     * @param string        $guid
     * @param string        $errorMessage
     * @param ErrorLevel    $errorLevel
     * @param string        $errorStack
     * @param boolean       $errorAllowRepeats
     *
     * @return ActivityLog              The created activity log instance.
     */
    public static function record(
        $scheduledAt,
        $startedAt,
        $name,
        $input,
        $output,
        $type,
        $priority = RecordPriority::NORMAL,
        $logs = [],
        $duration = null,
        $destination = null,
        $guid = null,
        $errorMessage = null,
        $errorLevel = ErrorLevel::NO_ERROR,
        $errorStack = null,
        $errorAllowRepeats = true
    ) {
        if (! ALConfig::isEnabledForModule($type)) {
            return; // Logger is disabled for module
        }

        if (!$errorAllowRepeats && self::isMessageBeingRepeated($errorMessage)) {
            return; // Message is already cached
        }

        $activity                   = new ActivityLog;
        $activity->scheduled_at     = $scheduledAt;
        $activity->started_at       = $startedAt;
        $activity->host_id          = LPConfig::hostId();
        $activity->host_name        = LPConfig::hostName();
        $activity->host_user        = Proc::currentUser(true);
        $activity->dest_id          = $destination;
        $activity->guid             = $guid;
        $activity->name             = $name;
        $activity->type             = $type;
        $activity->input            = $input;
        $activity->output           = $output;
        $activity->logs             = $logs;
        $activity->duration_s       = $duration;
        $activity->error_msg        = $errorMessage;
        $activity->error_level      = $errorLevel;
        $activity->error_stack      = $errorStack;
        $activity->priority         = $priority;
        $activity->save();

        return $activity;
    }

    /**
     * Records a failed activity to the database.
     *
     * @param integer           $scheduledAt
     * @param integer           $startedAt
     * @param string            $name
     * @param string            $input
     * @param Exception|string  $error
     * @param string            $type
     * @param integer           $priority
     * @param array             $logs
     * @param string            $duration
     * @param string            $output
     * @param string            $destination
     * @param string            $guid
     * @param integer           $errorLevel
     * @param boolean           $errorAllowRepeats
     *
     * @return ActivityLog      The created activity log instance.
     */
    public static function recordFailure(
        $scheduledAt,
        $startedAt,
        $name,
        $input,
        $e,
        $type,
        $priority = RecordPriority::HIGH,
        $logs = [],
        $duration = null,
        $output = null,
        $destination = null,
        $guid = null,
        $errorLevel = ErrorLevel::NORMAL,
        $errorAllowRepeats = true
    ) {
        if (is_null($e)) {
            self::exception("Invalid failed activity error.");
        }

        return self::record(
            $scheduledAt,
            $startedAt,
            $name,
            $input,
            $output,
            $type,
            $priority,
            $logs,
            $duration,
            $destination,
            $guid,
            is_string($e) ? $e : $e->getMessage(),
            $errorLevel,
            is_string($e) ? null : $e->getTraceAsString(),
            $errorAllowRepeats
        );
    }

    /**
     * Checks whether the error message is being repeated which
     * means it was previously cached to prevent flooding the
     * database with the same error message.
     *
     * @param string $message
     *
     * @return boolean
     */
    private static function isMessageBeingRepeated($message)
    {
        if (empty($message) || !is_string($message)) {
            return false;
        }

        $seconds = ALConfig::getTableErrorMsgNoRepeat();
        if ($seconds <= 0) {
            return false;
        }

        $cacheKey = "lp_al_" . hash('ripemd160', $message);
        if (empty(Cache::get($cacheKey))) {
            Cache::put($cacheKey, time(), $seconds);
            return false;
        }

        return true;
    }
}
