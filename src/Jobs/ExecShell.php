<?php

namespace NizarBlond\LaravelPlus\Jobs;

use NizarBlond\LaravelPlus\Abstracts\JobBase;
use NizarBlond\LaravelPlus\Support\Proc;

class ExecShell extends JobBase
{
    /**
     * The list of required config.
     *
     * @var array
     */
    protected $requiredConfigs = [
        'dir',
        'context'
    ];

    /**
     * Execute the job logic.
     *
     * @return mixed
     */
    protected function execute()
    {
        $suppressError = $this->config['suppress_error'] ?? false;
        $suppressFailedLog = $this->config['suppress_failed_log'] ?? false;

        $output = Proc::runCommand(
            $this->config['context'],
            $this->config['timeout'] ?? null,
            !$suppressError,
            !$suppressFailedLog,
            $this->config['dir']
        );

        // Avoid returning false in order not to save output as '0' in database
        return ($suppressError && $output === false) ? null : $output;
    }
}
