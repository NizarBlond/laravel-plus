<?php

namespace NizarBlond\LaravelPlus\Jobs\Git;

use NizarBlond\LaravelPlus\Support\Git;

class GitClone extends GitBase
{
    /**
     * Execute the job logic.
     *
     * @return string
     */
    protected function execute()
    {
        $output = Git::clone(
            $this->config['url'],
            $this->config['branch'],
            $this->keys,
            $this->config['dir'],
            $this->config['args'] ?? []
        );

        $this->setPermissions();

        return $output;
    }
}
