<?php

namespace NizarBlond\LaravelPlus\Jobs\Git;

use NizarBlond\LaravelPlus\Support\Git;

class GitPull extends GitBase
{
    /**
     * Execute the job logic.
     *
     * @return string
     */
    protected function execute()
    {
        $output = Git::pull(
            $this->config['dir'],
            $this->config['branch'],
            $this->keys,
            $this->config['service']
        );

        $this->setPermissions();

        return $output;
    }
}
