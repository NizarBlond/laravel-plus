<?php

namespace NizarBlond\LaravelPlus\Jobs\Git;

use NizarBlond\LaravelPlus\Abstracts\JobBase;
use NizarBlond\LaravelPlus\Support\Proc;
use NizarBlond\LaravelPlus\Support\Git;
use NizarBlond\LaravelPlus\Support\File;

abstract class GitBase extends JobBase
{
    /**
     * The list of required config.
     *
     * @var array
     */
    protected $requiredConfigs = [
        'url',
        'dir',
        'branch',
        'key',
        'service'
    ];

    /**
     * SSH keys array.
     *
     * @var array
     */
    protected $keys;

    /**
     * Indicates if the listener success should be logged.
     *
     * @var bool
     */
    public $logSuccess = true;

    /**
     * Initializes the job right before execution and after validation succeeded.
     *
     * @return void
     */
    protected function init()
    {
        $this->keys = Git::createKeyFiles($this->config['key']);
        parent::init();
    }

    /**
     * Sets user as owner of the git directory.
     *
     * @return void
     */
    protected function setPermissions()
    {
        if (empty($this->config['user']) || empty($this->config['group'])) {
            return;
        }
        $cmd = sprintf(
            "chown -R %s:%s '%s'",
            $this->config['user'],
            $this->config['group'],
            $this->config['dir']
        );
        Proc::runCommand($cmd);
    }

    /**
     * Handle the job completion (success/error) event.
     *
     * @param   Exception|null
     *
     * @return  void
     */
    protected function onComplete($error = null)
    {
        foreach ($this->keys as $key) {
            File::delete($key);
        }
    }
}
