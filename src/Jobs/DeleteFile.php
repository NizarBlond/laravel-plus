<?php

namespace NizarBlond\LaravelPlus\Jobs;

use NizarBlond\LaravelPlus\Abstracts\JobBase;
use NizarBlond\LaravelPlus\Support\File;

class DeleteFile extends JobBase
{
    /**
    * The list of required config.
    *
    * @var array
    */
   protected $requiredConfigs = [
       'filepath',
   ];

   /**
    * Execute the job logic.
    *
    * @return mixed
    */
   protected function execute()
   {
        return File::delete(
            $this->config['filepath'],
            $this->config['trash'] ?? true
        );
   }
}
