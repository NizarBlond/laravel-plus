<?php

namespace NizarBlond\LaravelPlus\Jobs;

use NizarBlond\LaravelPlus\Abstracts\JobBase;

class EmptyJob extends JobBase
{
    /**
     * Executes the job.
     *
     * @return void
     */
    protected function execute()
    {
        $this->log("I am an empty job.");
    }
}
