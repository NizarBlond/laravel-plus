<?php

namespace NizarBlond\LaravelPlus;

use Illuminate\Support\ServiceProvider;
use NizarBlond\LaravelPlus\Support\Time;
use NizarBlond\LaravelPlus\Support\File;
use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Config\LaravelPlus as LPConfig;

class LaravelPlusServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // File Publishing
        $this->publishes([
            __DIR__.'/../database/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->publishes([
            __DIR__.'/../config/laravel-plus.php' => config_path('laravel-plus.php')
        ], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if (LPConfig::dbMonitorEnabled()) {
            $this->registerDBMonitir();
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        //
    }

    /**
     * Register the DB monitor that logs all queries in a file with the specified
     * time interval format.
     */
    private function registerDBMonitir()
    {
        global $argv;

        // Set the file path to store the queries in.
        $path = sprintf(
            '%s/queries.%s.log',
            storage_path('logs'),
            date(LPConfig::dbMonitorDateFormat())
        );

        // Register the listener to log all queries.
        \Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) use ($path, $argv) {
            // Format the log line.
            $line = sprintf(
                '[%s] (%s) %s [%sms] %s',
                Time::now(),
                is_array($argv) ? ($argv[1] ?? 'n/a') : 'n/a',
                getmypid(),
                $query->time,
                $query->sql
            );
            // Add the bindings if any.
            if (isset($query->bindings) && count($query->bindings) > 0) {
                $query->bindings = array_map(function ($value) {
                    return Str::limit($value, 10, '...');
                }, $query->bindings);
                $line .= ' [' . implode(', ', $query->bindings) . ']';
            }
            // Create the file if it doesn't exist and append the line.
            if (!File::exists($path)) {
                File::touch($path, 0777);
            }
            File::append($path, $line . PHP_EOL);
        });
    }
}
