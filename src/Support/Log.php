<?php

namespace NizarBlond\LaravelPlus\Support;

use NizarBlond\LaravelPlus\Config\Log as LogConfig;
use Log as LaravelLog;

class Log extends Base
{
    /**
     * Array of logs data.
     *
     * @var array
     */
    private static $_logData = [];

    /**
     * PHP process identifier.
     *
     * @var string
     */
    private static $_processId = null;

    /**
     * Handle non existing methods.
     *
     * @param   string  $name
     * @param   mixed   $args
     *
     * @return  string
     */
    public static function __callStatic($name, $args)
    {
        $message = !empty($args) && !empty($args[0]) ? $args[0] : "";
        $context = !empty($args) && !empty($args[1]) ? $args[1] : [];
        $level   = $name;

        return self::log($message, $context, $level);
    }

    /**
     * Returns the log data.
     *
     * @return array
     */
    public static function getLogData()
    {
        return self::$_logData;
    }

    /**
     * Inserts log data.
     *
     * @param   string  $message
     * @param   array   $context
     * @param   string  $level
     *
     * @return  array|null
     */
    public static function addLogData($message, $context = [], $level = 'n/a')
    {
        self::$_logData[] = self::createLogData($message, $context, $level);
    }

    /**
     * Inserts log data.
     *
     * @param   string  $message
     * @param   array   $context
     * @param   string  $level
     *
     * @return  array
     */
    public static function createLogData($message, $context = [], $level = 'n/a')
    {
        return [
            'time'      => gmdate('Y-m-d H:i:s', time()),
            'level'     => $level,
            'message'   => $message,
            'context'   => $context
        ];
    }

    /**
     * Compares log levels.
     *
     * @param   string  $level1
     * @param   string  $level2
     *
     * @return  int     Returns -1 if level1 is greater, 0 if equal and 1 if smaller than level2.
     */
    public static function compareLevels($level1, $level2)
    {
        $levelToPoint = array_flip([ 'debug', 'info', 'warning', 'error', 'critical', ]);
        $level1Point = $levelToPoint[$level1] ?? -1;
        $level2Point = $levelToPoint[$level2] ?? -1;
        return $level1Point === $level2Point ? 0 : ($level1Point > $level2Point ? -1 : 1);
    }

    /**
     * Log a message to the logs.
     *
     * @param   string          $message
     * @param   array|object    $context
     * @param   string          $level
     *
     * @return  void
     */
    public static function log($message, $context = [], string $level = "info")
    {
        if ($level === 'error') {
            $message = self::errorAsString($message);
        }

        if (is_object($context)) {
            $context = (array) $context;
        }

        if (LogConfig::logProcessId()) {
            $message = sprintf('[%s] %s', self::processId(), $message);
        }

        LaravelLog::log($level, $message, $context);
    }

    /**
     * Returns a unique process ID.
     *
     * @return string
     */
    public static function processId()
    {
        if (!self::$_processId) {
            $key =  ($_SERVER['REMOTE_ADDR'] ?? '') .
                    ($_SERVER['REMOTE_PORT'] ?? '') .
                    ($_SERVER['REQUEST_TIME'] ?? '');
            self::$_processId = sprintf("%08x", abs(crc32($key)));
        }
        return self::$_processId;
    }

    /**
     * Log an error to the logs.
     *
     * @param   string|Exception    $message
     * @param   array               $context
     *
     * @return  void
     */
    public static function error($e, $context = [])
    {
        $message = self::errorAsString($e);
        return self::log($message, $context, "error");
    }

    /**
     * Prints message and data to the console.
     *
     * @param   string    $message
     * @param   array     $data
     * @param   string    $level
     *
     * @return  void
     */
    public static function print($message, $context = [], ?string $level = null)
    {
        if (empty($message)) {
            $message = 'Empty message';
        }

        if ($level) {
            switch ($level) {
                case 'error':
                case 'critical':
                    $message = self::colorize($message, 'white', 'red');
                    break;
                case 'warning':
                    $message = self::colorize($message, 'yellow');
                    break;
                case 'info':
                    $message = self::colorize($message, 'blue');
                    break;
                case 'success':
                    $message = self::colorize($message, 'green');
                    break;
                case 'debug':
                    $message = self::colorize($message, 'cyan');
                    break;
                case 'verbose':
                    $message = self::colorize($message, 'magenta');
                    break;
                default:
                    $message = self::colorize($message);
                    break;
            }
        }

        print_r($message . PHP_EOL);

        if (!empty($context)) {
            print_r($context);
        }
    }

    /**
     * Colorizes a message.
     *
     * Reference:
     * https://joshtronic.com/2013/09/02/how-to-use-colors-in-command-line-output/
     *
     * @param   string  $message
     * @param   string  $foreground
     * @param   string  $background
     *
     * @return  string
     */
    public static function colorize(string $message, ?string $foreground = null, ?string $background = null)
    {
        $foregroundCode = '';
        switch (strtolower($foreground ?? '')) {
            case 'yellow':
                $foregroundCode = '1;33';
                break;
            case 'blue':
                $foregroundCode = '1;34';
                break;
            case 'red':
                $foregroundCode = '1;31';
                break;
            case 'green':
                $foregroundCode = '0;32';
                break;
            case 'magenta':
                $foregroundCode = '1;35';
                break;
            case 'cyan':
                $foregroundCode = '1;36';
                break;
            case 'white':
            default:
                $foregroundCode = '1;37';
                break;
        }
        $backgroundCode = '';
        switch (strtolower($background ?? '')) {
            case 'yellow':
                $backgroundCode = '43';
                break;
            case 'blue':
                $backgroundCode = '44';
                break;
            case 'cyan':
                $backgroundCode = '46';
                break;
            case 'red':
                $backgroundCode = '41';
                break;
            case 'green':
                $backgroundCode = '42';
                break;
            case 'magenta':
                $backgroundCode = '45';
                break;
            case 'black':
            default:
                $backgroundCode = '40';
                break;
        }
        return sprintf("\e[%s;%sm%s\e[0m", $foregroundCode, $backgroundCode, $message);
    }

    /**
     * Parses the laravel log file.
     *
     * Reference:
     * https://stackoverflow.com/questions/28864020/parse-multiline-log-entries-using-a-regex
     *
     * @param string|null $file
     * @param int $lastN
     * @return array
     */
    public static function parseFile(?string $file = null, int $lastN = 0)
    {
        // file
        if (!$file) {
            $defaultChannel = config('logging.default');
            $config = config('logging.channels.' . $defaultChannel);
            $file = $config['path'];
        } elseif (Url::isRelative($file)) {
            Str::joinPaths(storage_path(), 'logs', basename($file));
        }

        // match
        $content = File::read($file);
        if (is_null($content)) {
            return false;
        }
        $pattern = '/^\[(?P<time>[\d\-:\s]+)\]\s(?P<env>\w+).(?P<level>[A-Z]+):\s\[?(?P<proc>[a-z0-9]+)?\]?\s?(?P<content>[\s\S]*?)\n*(?=^\[[\d\-:\s]+\]|\z)/sm';
        preg_match_all($pattern, $content, $matches);

        // parse
        $result = [];
        $groups = ['time', 'env', 'level', 'proc', 'content'];
        $linesNumber = count($matches[$groups[0]]);
        for ($i=0; $i < $linesNumber; $i++) { 
            $line = [];
            foreach ($groups as $group) {
                $line[$group] = $matches[$group][$i];
            }
            $result[] = $line;
        }

        return $lastN > 0 ? Arr::tail($result, $lastN) : $result;
    }

    /**
     * Returns an error message in string format.
     *
     * @param   mixed   $error
     *
     * @return  string
     */
    private static function errorAsString($error)
    {
        if (is_object($error) && method_exists($error, 'getMessage')) {
            $error = $error->getMessage();
        }
        return !empty($error) ? $error : "";
    }
}
