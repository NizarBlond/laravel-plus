<?php

namespace NizarBlond\LaravelPlus\Support;

use NizarBlond\LaravelPlus\Traits\ClassHelpers;
use Illuminate\Support\Arr as IlluminateArr;
use Illuminate\Support\Collection as IlluminateCollection;
use stdClass;
use Exception;

class Arr extends IlluminateArr
{
    use ClassHelpers;

    /**
     * Returns the unique values of the specified array.
     *
     * @param array $array
     * @param bool $preserveKeys
     * @return  array
     */
    public static function unique(array $array, bool $preserveKeys = false)
    {
        return array_unique($array, $preserveKeys ? SORT_REGULAR : SORT_STRING);
    }

    /**
     * Merges the specified arrays.
     *
     * @param mixed $a
     * @param mixed $b
     * @return array
     */
    public static function merge($a, $b, bool $unique = false)
    {
        if (empty($a)) {
            $a = [];
        }
        if (empty($b)) {
            $b = [];
        }
        $merged = array_merge($a, $b);
        return $unique ? self::unique($merged) : $merged;
    }

    /**
     * Returns the first key of the specified array.
     *
     * @param   array   $arr
     *
     * @return  string
     */
    public static function firstKey(array $arr)
    {
        return self::first(array_keys($arr));
    }

    /**
     * Returns the keys of the specified array.
     *
     * @param   array   $arr
     *
     * @return  array
     */
    public static function keys(array $arr)
    {
        return array_keys($arr);
    }

    /**
     * Returns the head of the specified array.
     *
     * @param   array   $arr
     * @param   integer $n
     *
     * @return  array
     */
    public static function head(array $arr, $length = 100)
    {
        $size = count($arr);
        if ($size <= $length) {
            return array_slice($arr, 0);
        }

        return array_slice($arr, 0, $length);
    }

    /**
     * Returns the tail of the specified array.
     *
     * @param   array   $arr
     * @param   integer $n
     *
     * @return  array
     */
    public static function tail(array $arr, $length = 100)
    {
        $size = count($arr);
        if ($size <= $length) {
            return array_slice($arr, 0);
        }

        return array_slice($arr, $size - $length);
    }

    /**
     * Returns only the specified array columns.
     *
     * @param   array     $columns
     *
     * @return  array
     */
    public static function columns(array $arr, $columns)
    {
        foreach ($arr as $key1 => $value1) {
            if (empty($value1)) {
                continue;
            }
            foreach ($value1 as $key2 => $value2) {
                if (!in_array($key2, $columns)) {
                    unset($arr[$key1][$key2]);
                }
            }
        }
        return $arr;
    }

    /**
     * Replaces array values recursively.
     *
     * @param   array     $data
     * @param   array     $replaceArr
     *
     * @return  array
     */
    public static function replaceDataRec($data, $replaceArr)
    {
        if (empty($data)) {
            return $data;
        }
        
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $data[$key] = self::replaceDataRec($value, $replaceArr);
            } elseif (is_string($value)) {
                foreach ($replaceArr as $varKey => $varValue) {
                    $value = str_replace($varKey, $varValue, $value);
                }
                $data[$key] = $value;
            }
        }

        return $data;
    }

    /**
     * Removes all numeric keys from the specified array.
     * 
     * @param   array   $array
     *
     * @return  array
     */
    public static function forgetNumericKeys(array &$array)
    {
        foreach (Arr::keys($array) as $key) {
            if (is_numeric($key)) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    /**
     * Validates array keys are available.
     *
     * @param   array       $arr
     * @param   array       $requiredKeys
     * @param   boolean     $verifyNotEmpty
     *
     * @return  boolean
     *
     * @throws  \Exception
     */
    public static function validateFields(array $arr, array $requiredKeys, $verifyNotEmpty = true)
    {
        foreach ($requiredKeys as $key) {
            if (! array_key_exists($key, $arr)) {
                self::exception("The key '$key' is missing.");
            }
            if ($verifyNotEmpty && empty($arr[$key])) {
                self::exception("The key '$key' has empty value.");
            }
        }

        return true;
    }

    /**
     * Walks over array/object recursively.
     *
     * Source: https://gist.github.com/mlconnor/2989980
     *
     * @param   array|object    $obj        The object or array.
     * @param   function        $closure    The closure function(value, is_key).
     * @param   string          $currKey    The current key being handled (or null if the
     *                                      currently handled object is value).
     * @return  boolean
     */
    public static function walkRecursive($obj, $closure, $currKey = null)
    {
        if (is_object($obj)) {
            $newObj = new stdClass();
            foreach ($obj as $property => $value) {
                $newProperty = $closure($property, true);
                if (is_null($newProperty)) {
                    continue;
                }
                $newValue = self::walkRecursive($value, $closure, $property);
                $newObj->$newProperty = $newValue;
            }
            return $newObj;
        } else if (is_array($obj)) {
            $newArray = array();
            foreach ($obj as $key => $value) {
                $key = $closure($key, true);
                if (is_null($key)) {
                    continue;
                }
                $newArray[$key] = self::walkRecursive($value, $closure, $key);
            }
            return $newArray;
        } else {
            return $closure($obj, false, $currKey);
        }
    }

    /**
     * Walks over array recursively and deletes all specified keys.
     *
     * @param   array   $arr    The array.
     * @param   array   $keys   The keys to modify their values.
     *
     * @return  array
     */
    public static function removeKeysRecursive($arr, $keys)
    {
        if (! is_array($arr) || empty($keys)) {
            return $arr;
        }

        return self::walkRecursive($arr, function ($value, $isKey, $vKey = null) use ($keys) {
            // If the value is not key we don't do anything
            if (! $isKey) {
                return $value;
            }

            // Check whether to remove the key
            if (in_array($value, $keys)) {
                return null; // Indicates removing the key
            }

            // Return they key as is
            return $value;
        });
    }

    /**
     * Walks over array recursively and modifies the values of specific keys.
     *
     * @param   array       $arr        The array.
     * @param   array       $keys       The keys to modify their values.
     * @param   function    $closure    The modification function($v).
     *
     * @return  array
     */
    public static function modifyKeyValuesRecursive($arr, $keys, $closure)
    {
        if (! is_array($arr) || empty($keys)) {
            return $arr;
        }

        return self::walkRecursive(
            $arr,
            function ($value, $isKey, $vKey = null) use ($keys, $closure) {

                // If the value is key we do nothing
                if ($isKey) {
                    return $value;
                }

                // Check whether the key's value should be modified
                if (in_array($vKey, $keys, true /* check needle type */)) {
                    return $closure($value);
                }

                // Return they value as is
                return $value;
            }
        );
    }

    /**
     * Checks if the specified arrays have similar data.
     *
     * @param   array|null  $a
     * @param   array|null  $b
     * @param   array       $diffs
     * @param   bool        $strict
     *
     * @return  bool
     */
    public static function isDataEqual(?array $a, ?array $b, ?array &$diffs = [], bool $strict = false)
    {
        $diffs = [];

        // If both are null 
        if (empty($a) && empty($b)) {
            return true;
        }

        // If one is empty and the other isn't
        if (empty($a) || empty($b)) {
            return false;
        }

        // If one is null and the other is array
        if (!is_array($a) || !is_array($b)) {
            return false;
        }

        // If any of the arrays is complex then do a deep comparison
        if (self::isComplex($a) && self::isComplex($b)) {
            $curr = '';
            return self::isComplexEqualWithAllDiffs($a, $b, $diffs, $strict);
        }

        // If any of the arrays is not complex and the other is then false (this skips the
        // array_diff()'s error "Array to string conversion" when using complex arrays).
        if (self::isComplex($a) || self::isComplex($b)) {
            return false;
        }

        // If primitive values and strict is provided then disregard indexes in comparison
        if (!$strict) {
            $diffs = array_diff($a, $b);
            return $diffs === array_diff($b, $a);
        }

        // Otherwise, check if they have the same key/value pairs (disregard order and type)
        return $a == $b;
    }

    /**
     * Checks if the content of the specified arrays is equal regardless of the order of
     * the keys. However, if the keys are different then it is not considered equal.
     *
     * For example:
     *
     * [ "str1", "str2" ] is not equal to [ "str2", "str1" ]
     *
     * @param   mixed   $a
     * @param   mixed   $b
     * @param   string  $curr
     * @param   array   $diffs
     * @param   bool    $strict
     *
     * @return  bool
     */
    public static function isComplexEqual(
        $a,
        $b,
        ?array &$diffs = null,
        string &$curr = '',
        bool $strict = false,
        string $delimiter = '.'
    ) {
        if (is_null($diffs)) {
            $diffs = [];
        }

        if (!is_array($b) || !is_array($a)) {
            if ($strict) {
                return $a === $b;
            }
            return $a == $b || Time::equals($a, $b); // support Carbon
        }

        foreach ($b as $key => $value) {
            $prev = $curr;
            $curr = $curr === '' ? $key : ($curr . $delimiter . $key);
            if (!self::exists($a, $key) ||
                !self::isComplexEqual($a[$key], $b[$key], $diffs, $curr, $strict, $delimiter)
            ) {
                $diffs[$curr] = $diffs[$curr] ?? [
                    'a' => $a[$key] ?? null,
                    'b' => $b[$key] ?? null
                ];
                return false;
            }
            $curr = $prev;
        }

        foreach ($a as $key => $value) {
            $prev = $curr;
            $curr = $curr === '' ? $key : ($curr . $delimiter . $key);
            if (!self::exists($b, $key) ||
                !self::isComplexEqual($a[$key], $b[$key], $diffs, $curr, $strict, $delimiter)
            ) {
                $diffs[$curr] = $diffs[$curr] ?? [
                    'a' => $a[$key] ?? null,
                    'b' => $b[$key] ?? null
                ];
                return false;
            }
            $curr = $prev;
        }

        return true;
    }

    /**
     * Returns whether the complex is equal with all the differences.
     *
     * @param   array   $a1
     * @param   array   $a2
     * @param   array   $diffs
     * @param   bool    $strict
     *
     * @return  bool
     */
    public static function isComplexEqualWithAllDiffs(
        array $a,
        array $b,
        ?array &$diffs,
        bool $strict = false,
        string $delimiter = '.'
    ) {
        // Create a temporary delimiter in order to handle arrays with dot in keys
        $_delimiter = sprintf('_%s_', Str::randomString(3));

        // Perform the first comparison
        $curr = '';
        if (self::isComplexEqual($a, $b, $diffs, $curr, $strict, $_delimiter)) {
            return true;
        }

        // Perform loops by removing the different keys (depth=1) from the arrays and compare
        // again so we can get all the differences.
        $allDiffs = [];
        $notEqual = true;
        while ($notEqual) {
            $allDiffs = array_merge($allDiffs, $diffs);
            foreach (array_keys($diffs) as $diffKey) {
                $_diffKey = Arr::first(explode($_delimiter, $diffKey));
                if (!Arr::exists($a, $_diffKey) && !Arr::exists($b, $_diffKey)) {
                    // For developers: if any error happens do not allow it to enter an infinite
                    // loop, the diffs might be wrong but at least we get a notification.
                    self::logError("$_diffKey not found.", [ 'a' => $a, 'b' => $b ]);
                    $notEqual = false;
                    break;
                }
                unset($a[$_diffKey]);
                unset($b[$_diffKey]);
                $curr = '';
                $diffs = null;
                if (self::isComplexEqual($a, $b, $diffs, $curr, $strict, $_delimiter)) {
                    $notEqual = false;
                    break;
                }
            }
        }

        // Replace the temporary delimiter with the expected one
        if ($delimiter !== $_delimiter) {
            foreach ($allDiffs as $key => $value) {
                if (Str::contains($key, $_delimiter)) {
                    $newKey = str_replace($_delimiter, $delimiter, $key);
                    $allDiffs[$newKey] = $value;
                    unset($allDiffs[$key]);
                }
            }
        }

        $diffs = $allDiffs;
        return false;
    }

    /**
     * Checks whether the first array is subset of the second.
     *
     * @param   array   $a1
     * @param   array   $a2
     *
     * @return  boolean
     */
    public static function isSubsetOf(array $a1, array $a2)
    {
        return array_intersect($a1, $a2) == $a2;
    }

    /**
     * Sorts an associative array by key without preserving keys.
     *
     * @param   array   $array
     * @param   string  $callback
     * @param   string  $descending
     * @return  array
     */
    public static function sortBy(array $array, $callback = null, bool $descending = false)
    {
        $result = IlluminateCollection::make($array)->sortBy($callback, SORT_REGULAR, $descending)->all();
        return array_values($result);
    }

    /**
     * Groups array by the specified key.
     *
     * @param   array   $a
     * @param   string  $key
     *
     * @return  array
     */
    public static function groupBy(array $a, $key)
    {
        $groupedArr = [];
        foreach ($a as $val) {
            if (!isset($val[$key])) {
                continue;
            }
            $groupedArr[$val[$key]][] = $val;
        }

        return $groupedArr;
    }

    /**
     * Returns the full difference between the provided arrays.
     *
     * @param   array  $a
     * @param   array  $b
     *
     * @return  array
     */
    public static function fullDiff(array $a, array $b)
    {
        return array_values(array_merge(
            array_diff($a, $b),
            array_diff($b, $a)
        ));
    }

    /**
     * A wrapper of PHP's intersect that does not maintain keys.
     *
     * @param   array  $a
     * @param   array  $b
     *
     * @return  array
     */
    public static function intersect(array $a, array $b)
    {
        return array_values(array_intersect($a, $b));
    }

    /**
     * Filters empty values.
     *
     * @param   array  $array
     *
     * @return  array
     */
    public static function filterEmpty(array $array)
    {
        return array_filter($array, function ($v) {
            return !empty($v);
        });
    }

    /**
     * Filters null values.
     *
     * @param   array  $array
     *
     * @return  array
     */
    public static function filterNull(array $array)
    {
        return array_filter($array, function ($v) {
            return !is_null($v);
        });
    }

    /**
     * Filter the array using the given callback.
     *
     * @param  array $array
     * @param  callable|null $callback
     * @return array
     */
    public static function filter($array, ?callable $callback = null, bool $preserveKeys = false)
    {
        $filtered = $callback ? self::where($array, $callback) : array_filter($array);
        return $preserveKeys ? $filtered : array_values($filtered);
    }

    /**
     * Maps the array using the given callback.
     *
     * @param  array  $array
     * @param  callable  $callback
     * @return array
     */
    public static function map($array, callable $callback)
    {
        return array_map($callback, $array);
    }

    /**
     * Reverses the array.
     *
     * @param  array  $array
     * @return array
     */
    public static function reverse($array)
    {
        return array_reverse($array);
    }

    /**
     * Tells whether the array is a complex.
     *
     * A complex array is one that contains a key that is not integer or
     * a value that is array or object.
     *
     * @param   array|null  $array
     *
     * @return  array
     */
    public static function isComplex(?array $array)
    {
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                if (!is_int($key) || is_array($value) || is_object($value)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Randomly shuffle the indexes.
     * 
     * @param   array|Collection    $array
     *
     * @return  array
     */
    public static function randomShuffle($array)
    {
        if ($array instanceof IlluminateCollection) {
            return $array->shuffle();
        }
        return collect($array)->shuffle()->all();
    }
}
