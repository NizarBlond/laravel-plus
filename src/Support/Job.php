<?php

namespace NizarBlond\LaravelPlus\Support;

class Job extends Base
{
    /**
     * Dispatch a job to the default queue.
     *
     * @param   object  $job
     * @return  integer
     */
    public static function dispatch($job)
    {
        return dispatch($job);
    }

    /**
     * Execute the job immediately.
     *
     * @param   object  $job
     * @return  mixed
     */
    public static function run($job)
    {
        return $job->handle();
    }
}
