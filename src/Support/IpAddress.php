<?php

namespace NizarBlond\LaravelPlus\Support;

use IPTools\IP;
use IPTools\Range;
use Exception;
use Cache;

class IpAddress extends Base
{
    /**
     * Resolves the hostname.
     *
     * @param string $ip
     * @return string
     */
    public static function resolveHost(string $ip)
    {
        $host = gethostbyaddr($ip);
        return $host === $ip ? null : $host;
    }

    /**
     * Checks whether the IP address is IPv6.
     *
     * @param string $ip
     * @return bool
     */
    public static function isV6(string $ip)
    {
        try {
            $ip = new IP($ip);
        } catch (Exception $e) {
            return false;
        }
        return $ip->getVersion() === IP::IP_V6;
    }

    /**
     * Checks whether the IP address is IPv4.
     *
     * @param string $ip
     * @return bool
     */
    public static function isV4(string $ip)
    {
        try {
            $ip = new IP($ip);
        } catch (Exception $e) {
            return false;
        }
        return $ip->getVersion() === IP::IP_V4;
    }

    /**
     * Checks whether the IP address format is valid.
     *
     * @param string
     * @return bool
     */
    public static function isValid(string $ip)
    {
        return self::isV4($ip) || self::isV6($ip);
    }

    /**
     * Checks whether the IP range is valid.
     * 
     * @param string $range
     * @return bool
     */
    public static function isValidRange(string $range)
    {
        try {
            Range::parse($range);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Checks whether the IP address is contained in the specified range.
     *
     * @param string $ip
     * @param string $range
     * @return bool
     */
    public static function isInRange(string $ip, string $range)
    {
        if (empty($range)) {
            return false;
        }

        try {
            $rangeAsObj = Range::parse($range);
            return $rangeAsObj->contains(new IP($ip));
        } catch (Exception $e) {
            Log::error("Failed to check '$ip' in '$range': {$e->getMessage()}");
            return false;
        }
    }

    /**
     * Checks whether the IP address is localhost.
     *
     * @param string $ip
     * @return bool
     */
    public static function isLocalhost(string $ip)
    {
        return in_array($ip, [
            '127.0.0.1',
            'localhost',
            '::1',
        ]);
    }

    /**
     * Checks whether the IP address is of a trusted reverse proxy service.
     *
     * Supported services:
     * - cloudflare
     * - cloudfront
     *
     * @param string $ip
     * @param string $version
     * @return bool
     */
    public static function isTrustedReverseProxy(string $ip, string $version = 'v4', ?string &$range = null)
    {
        $services = [
            'v4' => [
                'https://www.cloudflare.com/ips-v4',
                'https://d7uri8nf7uskq.cloudfront.net/tools/list-cloudfront-ips'
            ],
            'v6' => [
                'https://www.cloudflare.com/ips-v6'
            ]
        ];

        $range = null;
        foreach ($services[$version] ?? [] as $rangesUrl) {
            $ranges = self::listServiceIpRanges($rangesUrl);
            foreach ($ranges as $_range) {
                if (self::isInRange($ip, $_range)) {
                    $range = $_range;
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Lists the IP range per service.
     *
     * @param string $rangesUrl
     * @return array
     */
    private static function listServiceIpRanges(string $rangesUrl)
    {
        $cacheKey = sprintf("lp_ip_%s", Str::slugify($rangesUrl));
        $cacheValue = Cache::get($cacheKey);
        if (!empty($cacheValue)) {
            return $cacheValue;
        }

        if (Str::contains($rangesUrl, 'cloudfront')) {
            $ranges = File::read($rangesUrl) ?? false;
            if ($ranges) {
                $ranges = Str::jsonDecode($ranges);
                $ranges = $ranges['CLOUDFRONT_GLOBAL_IP_LIST'] ?? [];
            }
        } elseif (Str::contains($rangesUrl, 'cloudflare')) {
            // Read ranges list from URL into array
            $ranges = File::read($rangesUrl, true);
        } else {
            self::exception('Invalid service.');
        }

        if (!$ranges) {
            Log::error("Failed to read data from $rangesUrl.");
            return [];
        }

        // Normalize ranges array items
        $ranges = array_map(function ($range) {
            return preg_replace('/[^0-9\.\/]/s', '', $range);
        }, $ranges);

        Cache::put($cacheKey, $ranges, 86400); // 24 hours
        return $ranges;
    }
}
