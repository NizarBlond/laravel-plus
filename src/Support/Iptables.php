<?php

namespace NizarBlond\LaravelPlus\Support;

use SplFileObject;
use Exception;

class Iptables extends Base
{
    // indexes
    const LINE_NUM = 0;
    const TARGET = 1;
    const PROTOCOL = 2;
    const OPT = 3;
    const ADDRESS = 4;

    // chains
    const INPUT_CHAIN = 'lp-INPUT';
    const DROP_CHAIN = 'lp-DROP';

    // packet
    const PACKET_IDENTIFIER = 'laravel-plus';

    /**
     * Bans the given IP by adding a DROP rule to the custom INPUT chain.
     * 
     * @param string $ip
     * @param array $ports
     * @return bool
     */
    public static function banIp(string $ip, array $ports = [])
    {
        if (self::isIpBanned($ip)) {
            return false;
        }

        if (empty($ports)) {
            self::exec(sprintf(
                '-I %s -s %s -j %s',
                self::INPUT_CHAIN,
                $ip,
                self::DROP_CHAIN
            ));
        } else {
            $ports = implode(',', $ports);
            self::exec(sprintf(
                '-I %s -p tcp -s %s -m multiport --dports %s -j %s',
                self::INPUT_CHAIN,
                $ip,
                $ports,
                self::DROP_CHAIN
            ));
        }

        return true;
    }

    /**
     * Unbans the given IP by removing the DROP rule from the custom INPUT chain.
     * 
     * @param string $ip
     * @param bool $onlyFirst
     * @return int
     */
    public static function unbanIp(string $ip, bool $onlyFirst = false)
    {
        $rules = self::listInputRules($ip, self::DROP_CHAIN); // rules with the ip
        if (empty($rules)) {
            self::logWarning(sprintf('Failed to DROP %s: rule not found.', $ip));
            return 0;
        }
        foreach ($rules as $rule) {
            self::exec(sprintf('-D %s %s', self::INPUT_CHAIN, $rule[self::LINE_NUM]));
            if ($onlyFirst) {
                return 1;
            }
        }
        return count($rules);
    }

    /**
     * Checks if the given IP is banned.
     * 
     * @param string $ip
     * @param string|null $protocol
     * @return bool
     */
    public static function isIpBanned(string $ip, ?string $protocol = null)
    {
        return in_array($ip, self::listBannedIps($protocol));
    }

    /**
     * Lists the banned IPs.
     * 
     * @param string|null $protocol
     * @return array
     */
    public static function listBannedIps(?string $protocol = null)
    {
        return array_map(function ($rule) {
            return $rule[self::ADDRESS];
        }, self::listInputRules(null, self::DROP_CHAIN, $protocol));
    }

    /**
     * Lists the input rules in the given chain.
     * 
     * @param string|null $ip
     * @param string|null $target
     * @param string|null $protocol
     * @param string $chain
     * @return array
     */
    public static function listInputRules(
        ?string $ip = null,
        ?string $target = null,
        ?string $protocol = null,
        ?string $chain = self::INPUT_CHAIN
    ) {
        // List input chain
        $list = self::exec(sprintf(
            '-L %s -n --line-numbers | tail -n +3 | awk -v OFS="," \'{print $1,$2,$3,$4,$5,$6}\'',
            $chain
        ));
        // Parse lines data
        $lines = array_map(function ($line) {
            return explode(',', $line);
        }, array_filter(explode(PHP_EOL, $list)));
        // Filter by IP address / Host
        if (!empty($ip)) {
            $lines = Arr::where($lines, function ($line) use ($ip) {
                return $line[self::ADDRESS] === $ip;
            });
        }
        // Filter by target
        if (!empty($target)) {
            $lines = Arr::where($lines, function ($line) use ($target) {
                return $line[self::TARGET] === $target;
            });
        }
        // Filter by protocol
        if (!empty($protocol)) {
            $lines = Arr::where($lines, function ($line) use ($protocol) {
                return $line[self::PROTOCOL] === $protocol;
            });
        }
        return $lines;
    }

    /**
     * Initialize the custom chain configuration.
     * 
     * @return void
     */
    public static function initCustomChain()
    {
        // input
        self::removeCustomInputRuleFromInputChain();
        self::dropCustomInputChain();
        self::createCustomInputChain();
        self::insertCustomInputRuleToInputChain();
        self::insertReturnRuleToCustomInputChain();

        // drop
        self::dropCustomDropChain();
        self::createCustomDropChain();
        self::insertLogRuleToCustomDropChain();
        self::insertDropRuleToCustomDropChain();
    }

    /**
     * Tests the custom chain configuration and fixes it if needed.
     * 
     * @param array $errors
     * @param bool $applyFix
     * @return void
     */
    public static function testCustomChain(?array &$errors = [], bool $applyFix = true)
    {
        $errors = [];

        // Verify custom input chain exists
        if (!self::chainExists(self::INPUT_CHAIN)) {
            $errors[] = sprintf('%s chain not found.', self::INPUT_CHAIN);
            if ($applyFix) {
                self::createCustomInputChain();
            }
        }

        // Verify the default INPUT chain includes "lp-INPUT tcp" rule.
        $inputRules = self::listInputRules(null, self::INPUT_CHAIN, 'tcp', 'INPUT');
        if (empty($inputRules)) {
            $errors[] = sprintf('%s rule is missing from INPUT chain.', self::INPUT_CHAIN);
            if ($applyFix) {
                self::insertCustomInputRuleToInputChain();
            }
        }

        // Verify the "RETURN all" rule exists in custom input chain.
        $returnRule = self::listInputRules(null, 'RETURN', 'all');
        if (empty($returnRule)) {
            $errors[] = sprintf('RETURN rule is missing from %s chain', self::INPUT_CHAIN);
            if ($applyFix) {
                self::insertReturnRuleToCustomInputChain();
            }
        }

        // Verify custom drop chain exists.
        if (!self::chainExists(self::DROP_CHAIN)) {
            $errors[] = sprintf('%s chain not found.', self::DROP_CHAIN);
            if ($applyFix) {
                self::createCustomDropChain();
            }
        }

        // Verify custom drop chain includes "LOG all" and "DROP all" rules.
        $dropRules = Iptables::listInputRules(null, null, 'all', Iptables::DROP_CHAIN);
        if (count($dropRules) !== 2) {
            $errors[] = sprintf('%s chain is not configured properly.', self::DROP_CHAIN);
            if ($applyFix) {
                try {
                    self::dropCustomDropChain();
                } catch (Exception $e) {
                    self::logError(sprintf('Failed to drop %s iptables chain: %s', self::DROP_CHAIN, $e->getMessage()));
                }
                try {
                    self::createCustomDropChain();
                } catch (Exception $e) {
                    self::logError(sprintf('Failed to create %s iptables chain: %s.', self::DROP_CHAIN, $e->getMessage()));
                }
                self::insertLogRuleToCustomDropChain();
                self::insertDropRuleToCustomDropChain();
            }
        }

        return empty($errors);
    }

    /**
     * Checks if the specified chain exists.
     * 
     * @param string $chain
     * @return bool
     */
    public static function chainExists(string $chain)
    {
        try {
            self::exec(sprintf('-L %s -n', $chain), false);
        } catch (Exception $e) {
            if (Str::contains($e->getMessage(), 'No chain/target/match by that name')) {
                return false;
            }
            throw $e;
        }
        return true;
    }

    /**
     * Flushes the specified chain.
     * 
     * @param string $chain
     * @return bool
     */
    private static function flushChain(string $chain)
    {
        try {
            self::exec(sprintf('-F %s', $chain), false);
        } catch (Exception $e) {
            self::logWarning(sprintf('Failed to flush %s chain: %s', $chain, $e->getMessage()));
            return false;
        }
        return true;
    }

    /**
     * Deletes the specified chain.
     * 
     * @param string $chain
     * @return bool
     */
    private static function deleteChain(string $chain)
    {
        self::flushChain($chain);

        try {
            self::exec(sprintf('-X %s', $chain), false);
        } catch (Exception $e) {
            self::logWarning(sprintf('Failed to delete %s chain: %s', $chain, $e->getMessage()));
            return false;
        }
        return true;
    }

    /**
     * Removes the custom input chain rule from the default INPUT chain.
     * 
     * @return void
     */
    private static function removeCustomInputRuleFromInputChain()
    {
        try {
            self::exec(sprintf(
                '-D INPUT -p tcp -m multiport --dports 80,443 -j %s',
                self::INPUT_CHAIN
            ), false);
        } catch (Exception $e) {
            self::logWarning(sprintf(
                'Failed to delete %s chain rule: %s',
                self::INPUT_CHAIN,
                $e->getMessage()
            ));
        }
    }

    /**
     * Drops the custom input chain.
     * 
     * @return void
     */
    private static function dropCustomInputChain()
    {
        self::deleteChain(self::INPUT_CHAIN);
    }

    /**
     * Creates a custom input chain.
     * 
     * @return void
     */
    private static function createCustomInputChain()
    {
        self::exec(sprintf('-N %s', self::INPUT_CHAIN), false);
    }

    /**
     * Inserts (head) a custom input chain rule to the default INPUT chain.
     *
     * Expected rule:
     * lp-INPUT   tcp  --  0.0.0.0/0    0.0.0.0/0   multiport dports 80,443
     *
     * @return void
     */
    private static function insertCustomInputRuleToInputChain()
    {
        self::exec(sprintf('-I INPUT -p tcp -m multiport --dports 80,443 -j %s', self::INPUT_CHAIN), false);
    }

    /**
     * Appends (tail) a RETURN rule to the custom input chain.
     * 
     * Expected rule:
     * RETURN   all  --  0.0.0.0/0  0.0.0.0/0
     *
     * @return void
     */
    private static function insertReturnRuleToCustomInputChain()
    {
        self::exec(sprintf('-A %s -s %s -j RETURN', self::INPUT_CHAIN, '0.0.0.0/0'), false);
    }

    /**
     * Creates the custom drop chain.
     * 
     * @return void
     */
    private static function createCustomDropChain()
    {
        self::exec(sprintf('-N %s', self::DROP_CHAIN), false);
    }

    /**
     * Drops the custom drop chain.
     *
     * @return void
     */
    private static function dropCustomDropChain()
    {
        self::deleteChain(self::DROP_CHAIN);
    }

    /**
     * Appends (tail) a LOG rule to the custom drop chain.
     *
     * Expected rule:
     * LOG  all  --  0.0.0.0/0  0.0.0.0/0   ...
     *
     * @return void
     */
    private static function insertLogRuleToCustomDropChain()
    {
        self::exec(sprintf(
            '-A %s -m limit --limit 5/min -j LOG --log-prefix "%s" --log-level %s',
            self::DROP_CHAIN,
            sprintf('[%s] dropped ', self::PACKET_IDENTIFIER),
            6
        ), false);
    }

    /**
     * Appends (tail) a DROP rule to the custom drop chain.
     *
     * Expected rule:
     * DROP all  --  0.0.0.0/0  0.0.0.0/0
     *
     * @return void
     */
    private static function insertDropRuleToCustomDropChain()
    {
        self::exec(sprintf('-A %s -j DROP', self::DROP_CHAIN), false);
    }

    /**
     * Executes the iptables command.
     * 
     * @param string $cmd
     * @param bool $retry
     * @param int $unlockWait
     * @return void
     */
    private static function exec(string $cmd, bool $retry = true, int $unlockWait = 5)
    {
        // The "-w" option will make the program wait for the xtables lock for $unlockWait
        // seconds until the exclusive lock can be obtained.
        $_cmd = sprintf('/sbin/iptables -w %s %s', $unlockWait, $cmd);

        try {
            return Proc::runCommandWithoutLog($_cmd);
        } catch (Exception $e) {
            if ($retry && Str::contains($e->getMessage(), 'No chain/target/match by that name')) {
                self::initCustomChain();
                return self::exec($cmd, false);
            }
            self::exception($e->getMessage());
        }
    }
}
