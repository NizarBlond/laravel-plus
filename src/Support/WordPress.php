<?php

namespace NizarBlond\LaravelPlus\Support;

use Exception;

class WordPress extends Base
{
    /**
     * Parses the specified command and returns the arguments and options.
     *
     * @param string    $command
     * @return array
     */
    public static function parseWpCliCommand(string $command)
    {
        // Remove 'wp' from the beginning of the command
        $command = trim(Str::replaceFirst('wp ', '', $command));

        // Extract the command arguments (starting with --).
        $args = [];
        // Get all arguments that don't use quotes, have = but don't assign values
        $pattern = '/\s+(--[^\s]+)\s*=\s*(?:--|$)/';
        if (preg_match_all($pattern, $command, $matches)) {
            foreach ($matches[1] as $name) {
                $args[$name] = '';
                $command = preg_replace('/'.$name.'\s*=\s*/', ' ', $command);
            }
        }
        // Get all arguments that don't use quotes
        $pattern = '/\s+(--[^\s]+)\s*=\s*([^\'"\s]+)\s*/';
        if (preg_match_all($pattern, $command, $matches)) {
            $args = array_merge($args, array_combine($matches[1], $matches[2]));
            $command = preg_replace($pattern, ' ', $command);
        }
        // Get all arguments that use single quotes
        $pattern = '/\s+(--[^\s]+)\s*=\s*\'([^\']+)\'/';
        if (preg_match_all($pattern, $command, $matches)) {
            $args = array_merge($args, array_combine($matches[1], $matches[2]));
            $command = preg_replace($pattern, ' ', $command);
        }
        // Get all arguments that use double quotes
        $pattern = '/\s+(--[^\s]+)\s*=\s*\"([^"]+)\"/';
        if (preg_match_all($pattern, $command, $matches)) {
            $args = array_merge($args, array_combine($matches[1], $matches[2]));
            $command = preg_replace($pattern, ' ', $command);
        }
        // Get all arguments with no value
        $pattern = '/(?:^|\s)(--[^\s=]+)/';
        if (preg_match_all($pattern, $command, $matches)) {
            foreach ($matches[1] as $name) {
                $args[$name] = null;
            }
            $command = preg_replace($pattern, ' ', $command);
        }

        // At this point, the command should only contain options.
        $commandWithoutArgs = $command;

        // Extract all options in order, one by one, and consider single and double quotes as one option.
        $options = [];
        $maxIterations = 100;
        while (!empty($commandWithoutArgs)) {
            // Ensure that the command doesn't begin with a space.
            $commandWithoutArgs = trim($commandWithoutArgs);
            if (Str::startsWith($commandWithoutArgs, '"')) {
                // If begins with a double quote, then find the closing quote and add the whole string as an option.
                $pattern = '/^"([^"]+)"/';
                if (preg_match($pattern, $commandWithoutArgs, $matches)) {
                    $options[] = $matches[1]; // Add the non-quoted string as an option.
                    $commandWithoutArgs = preg_replace($pattern, '', $commandWithoutArgs); // Remove the option from the command.
                }
            } elseif (Str::startsWith($commandWithoutArgs, "'")) {
                // If begins with a single quote, then find the closing quote and add the whole string as an option.
                $pattern = "/^'([^']+)'/";
                if (preg_match($pattern, $commandWithoutArgs, $matches)) {
                    $options[] = $matches[1]; // Add the non-quoted string as an option.
                    $commandWithoutArgs = preg_replace($pattern, '', $commandWithoutArgs); // Remove the option from the command.
                }
            } else {
                // Otherwise, find the first space and add the string before it as an option.
                $pattern = '/^([^"\'\s]+)/';
                if (preg_match($pattern, $commandWithoutArgs, $matches)) {
                    $options[] = $matches[1]; // Add the whole string as an option.
                    $commandWithoutArgs = preg_replace($pattern, '', $commandWithoutArgs); // Remove the option from the command.
                }
            }
            $maxIterations--;
            if ($maxIterations <= 0) {
                self::exception('Failed to parse the command options.');
            }
        }

        return [
            'main'      => $options[0] ?? self::exception('Failed to retrieve the command name.'),
            'args'      => $args,
            'options'   => array_slice($options, 1),
        ];
    }

    /**
     * Checks whether the packages have same version.
     *
     * @param string    $dir1
     * @param string    $dir2
     * @param string    &$ver1
     * @param string    &$ver2
     * @return bool
     */
    public static function isSamePackageVersion(
    	string $dir1,
    	string $dir2,
    	&$ver1 = null,
    	&$ver2 = null
    ) {
        if ($dir1 === $dir2) {
            self::exception('Packages cannot be the same.');
        }

        $meta1 = self::parsePackageMetadata($dir1, 1);
        if (empty($meta1)) {
            self::exception(sprintf('Failed to parse %s package metadata.', $dir1));
        }

        $meta2 = self::parsePackageMetadata($dir2, 1);
        if (empty($meta2)) {
            self::exception(sprintf('Failed to parse %s package metadata.', $dir2));
        }

        // Sometimes the package has no version and that's ok
        $ver1 = $meta1['Version'] ?? null;
        $ver2 = $meta2['Version'] ?? null;

        return $ver1 === $ver2;
    }

    /**
     * Checks whether the path is a Wordpress core folder.
     *
     * @param string    $path
     * @return boolean
     */
    public static function isCoreDir(string $path)
    {
        $dirs = array_map('basename', glob(Str::joinPaths($path, "/*"), GLOB_ONLYDIR));
        return 	in_array('wp-admin', $dirs) &&
        		in_array('wp-content', $dirs) &&
        		in_array('wp-includes', $dirs);
    }

    /**
     * Generates a meta file for the package.
     * 
     * @param string    $dir
     * @param array     $includeExt
     * @param array     $excludeExt
     * @return array
     */
    public static function generatePackageMeta(string $dir, array $includeExt = [], array $excludeExt = [])
    {
        $hash = self::generatePackageHash($dir, $fileCount, $includeExt, $excludeExt);
        return [
            'created_at' => Time::now()->toDateTimeString(),
            'file_count' => $fileCount,
            'md5_hash' => $hash,
            'dir_path' => $dir,
        ];
    }

    /**
     * Generates a hash for the package.
     * 
     * @param string    $dir
     * @param int       &$fileCount
     * @param array     $includeExt
     * @param array     $excludeExt
     * @return string
     */
    public static function generatePackageHash(string $dir, int &$fileCount = null, array $includeExt = [], array $excludeExt = [])
    {
        if (!Dir::exists($dir)) {
            self::exception(sprintf('Directory %s does not exist.', $dir));
        }
        $files = Dir::scanFilesRecursive($dir, $includeExt, [], null, true);
        $hash = '';
        $fileCount = 0;
        foreach ($files ?? [] as $file) {
            if (Str::contains($file, '.git') || !empty($excludeExt) && in_array(File::ext($file), $excludeExt)) {
                continue;
            }
            $filename = basename($file);
            $hash .= md5($filename);
            $fileCount++;
        }
        return md5($hash);
    }

    /**
     * Parses the package metadata from the package's main file.
     *
     * @param string    $packageDir
     * @param int       $minValidityLevel
     * @return array|null
     */
    public static function parsePackageMetadata(string $packageDir, int $minValidityLevel)
    {
        // Guess type from dir path
        if (Str::contains($packageDir, '/plugins/')) {
            $packageType = 'plugin';
            $searchExts = [ 'php' ];
        } elseif (Str::contains($packageDir, '/themes/')) {
            $packageType = 'theme';
            $searchExts = [ 'css' ];
        } else {
            $packageType = null;
            $searchExts = [ 'php', 'css' ];
        }

        // Search possible files that include metadata
        $packageFiles = array_merge(
            Dir::files($packageDir, $searchExts),
            Dir::files(Str::joinPaths($packageDir, 'resources'), $searchExts),
        );

        // Iterate over files and try to get the metadata
        $maxValidityLevelFound = 0;
        $metadata = [];
        $_packageType = $packageType;
        foreach ($packageFiles as $file) {
            // Try get plugin and calculate validity level
            if (!$packageType || $packageType === 'plugin') {
                $fileMetadata = self::getPluginMetadata($file);
                $isValid = self::isValidMetadata($fileMetadata, $fileValidityLevel);
                if ($isValid && $fileValidityLevel > $maxValidityLevelFound) {
                    $_packageType = 'plugin';
                    $maxValidityLevelFound = $fileValidityLevel;
                    $metadata = $fileMetadata;
                }
            }
            // Try get theme and calculate validity level
            if (!$packageType || $packageType === 'theme') {
                $fileMetadata = self::getThemeMetadata($file);
                $isValid = self::isValidMetadata($fileMetadata, $fileValidityLevel);
                if ($isValid && $fileValidityLevel > $maxValidityLevelFound) {
                    $_packageType = 'theme';
                    $maxValidityLevelFound = $fileValidityLevel;
                    $metadata = $fileMetadata;
                }
            }
        }
        if ($maxValidityLevelFound < $minValidityLevel) {
            self::logWarning(sprintf('No package metadata found for %s.', $packageDir));
            return null;
        }
        $metadata['Type'] = $_packageType;
        return $metadata;
    }

    /**
     * Parses plugin metadata from file.
     *
     * @param string    $file
     * @return array
     */
    public static function getPluginMetadata(string $file)
    {
        $defaultHeaders = array(
            'Name' => 'Plugin Name',
            'PluginURI' => 'Plugin URI',
            'Version' => 'Version',
            'Description' => 'Description',
            'Author' => 'Author',
            'AuthorURI' => 'Author URI',
            'TextDomain' => 'Text Domain',
            'DomainPath' => 'Domain Path',
            'Network' => 'Network'
        );

        return self::getPluginOrThemeMetadata($file, $defaultHeaders, 'plugin');
    }

    /**
     * Parses theme metadata from file.
     *
     * @param   string    $file
     *
     * @return  array
     *
     * @throws  \Exception
     */
    public static function getThemeMetadata(string $file)
    {
        $defaultHeaders = array(
            'Name' => 'Theme Name',
            'ThemeURI' => 'Theme URI',
            'Version' => 'Version',
            'Description' => 'Description',
            'Author' => 'Author',
            'AuthorURI' => 'Author URI',
            'TextDomain' => 'Text Domain',
            'DomainPath' => 'Domain Path',
            'Network' => 'Network',
            'ParentThemeDir' => 'Template'
        );

        return self::getPluginOrThemeMetadata($file, $defaultHeaders, 'theme');
    }

    /**
     * Parses wordpress core package metadata.
     *
     * @param   string    $wpDir
     *
     * @return  array
     *
     * @throws  \Exception
     */
    public static function getCoreMetadata(string $wpDir)
    {
        $versionFile = Str::joinPaths($wpDir, "wp-includes", "version.php");
        if (!File::exists($versionFile)) {
            self::exception("Invalid version.php file.");
        }

        // Get file content
        $file_data = File::read($versionFile);

        $wpVersion = null;
        if (preg_match('/\$wp_version\s+=\s+\'([\d.]+)\';/', $file_data, $match) && $match[1]) {
            $wpVersion = $match[1];
        }

        return [
            'Name' => 'Wordpress',
            'Type' => 'core',
            'Version' => $wpVersion,
            'Description' => 'Wordpress core package.',
            'AuthorURI' => 'www.wordpress.org',
            'Author' => 'Automattic',
            'URI' => 'www.wordpress.org'
        ];
    }

    /**
     * This function was copied from wordpress core (functions.php).
     *
     * Retrieve metadata from a file.
     *
     * Searches for metadata in the first 8kiB of a file, such as a plugin or theme.
     * Each piece of metadata must be on its own line. Fields can not span multiple
     * lines, the value will get cut at the end of the first line.
     *
     * If the file data is not within that first 8kiB, then the author should correct
     * their plugin file and move the data headers to the top.
     *
     * @link https://codex.wordpress.org/File_Header
     *
     * @param string $file            Path to the file.
     * @param array  $default_headers List of headers, in the format array('HeaderKey' => 'Header Name').
     * @param string $context         Optional. If specified adds filter hook {@see 'extra_$context_headers'}.
     *                                Default empty.
     * @return array Array of file headers in `HeaderKey => Header Value` format.
     */
    public static function getPluginOrThemeMetadata(
    	string $file,
    	array $default_headers,
    	string $context = ''
    ) {
        // Get file content
        $file_data = File::read($file);

        /**
         * Filters extra file headers by context.
         *
         * The dynamic portion of the hook name, `$context`, refers to
         * the context where extra headers might be loaded.
         *
         * @param array $extra_context_headers Empty array by default.
         */
        if ($context && $extra_headers = []) {
            $extra_headers = array_combine($extra_headers, $extra_headers); // keys equal values
            $all_headers = array_merge($extra_headers, (array) $default_headers);
        } else {
            $all_headers = $default_headers;
        }

        foreach ($all_headers as $field => $regex) {
            if ($file_data && preg_match('/^[ \t\/*#@]*' . preg_quote($regex, '/') . ':(.*)$/mi', $file_data, $match) && $match[1]) {
                $all_headers[ $field ] = self::_cleanupHeaderComment($match[1]);
            } else {
                $all_headers[ $field ] = '';
            }
        }

        return $all_headers;
    }

    /**
     * Gets the package info from wordpress api.
     *
     * @param   string  $slug
     * @param   string  $type
     * @param   bool    $fixSlug
     *
     * @return  array|null
     */
    public static function getPackageInfoFromApi(string $slug, string $type, bool $fixSlug = true)
    {
        if ($type === 'plugin') {
            return self::getPluginInfoFromApi($slug, $fixSlug);
        } elseif ($type === 'theme') {
            return self::getThemeInfoFromApi($slug, $fixSlug);
        } else {
            self::exception(sprintf('Invalid package type: %s.', $type));
        }
    }

    /**
     * Gets the plugin info from wordpress api.
     *
     * @param   string  $slug
     * @param   string  $type
     * @param   bool    $fixSlug
     *
     * @return  array|null
     */
    public static function getPluginInfoFromApi(string $slug, bool $fixSlug = true)
    {
        $url = sprintf('https://api.wordpress.org/plugins/info/1.0/%s.json', $slug);
        $content = @File::download($url);
        if (empty($content)) {
            // If the slug ends with -\d then remove it and try again
            if ($fixSlug && preg_match('/^(.*)-\d+$/', $slug, $match)) {
                return self::getPluginInfoFromApi($match[1], false);
            }
            return null;
        }
        $info = Str::jsonDecode($content);
        return empty($info) ? null : $info;
    }

    /**
     * Gets the theme info from wordpress api.
     *
     * @param   string  $slug
     * @param   bool    $fixSlug
     *
     * @return  array|null
     */
    public static function getThemeInfoFromApi(string $slug, bool $fixSlug = true)
    {
        $url = sprintf('https://api.wordpress.org/themes/info/1.1/?action=theme_information&request[slug]=%s', $slug);
        $fields = [ 'versions', 'download_link', 'last_updated', 'requires_php', 'requires_wp', 'tested' ];
        foreach ($fields as $field) {
            $url .= sprintf('&request[fields][%s]=true', $field);
        }
        $content = @File::download($url);
        if (empty($content)) {
            // If the slug ends with -\d then remove it and try again
            if ($fixSlug && preg_match('/^(.*)-\d+$/', $slug, $match)) {
                return self::getThemeInfoFromApi($match[1], false);
            }
            return null;
        }
        $info = Str::jsonDecode($content);
        return empty($info) ? null : $info;
    }

    /**
     * Checks whether the given metadata is valid.
     *
     * @param   array   $metadata
     * @param 	int 	&level
     *
     * @return  boolean
     */
    private static function isValidMetadata(array $metadata, &$level)
    {
        $level = 0;
        if (! empty($metadata['Name'])) {
            $level += 2;
        }
        if (! empty($metadata['Version'])) {
            $level += 2;
        }
        if (! empty($metadata['ParentThemeDir'])) {
            $level += 1;
        }
        if (! empty($metadata['Author'])) {
            $level += 1;
        }
        return $level > 0;
    }

    /**
     * This function was copied from wordpress core (functions.php).
     *
     * Strip close comment and close php tags from file headers used by WP.
     *
     * @see https://core.trac.wordpress.org/ticket/8497
     *
     * @param string $str Header comment to clean up.
     *
     * @return string
     */
    private static function _cleanupHeaderComment(string $str)
    {
        return trim(preg_replace("/\s*(?:\*\/|\?>).*/", '', $str));
    }
}
