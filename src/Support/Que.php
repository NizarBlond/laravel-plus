<?php

namespace NizarBlond\LaravelPlus\Support;

use NizarBlond\LaravelPlus\Abstracts\JobBase;
use NizarBlond\LaravelPlus\Config\Que as QueConfig;
use Queue;
use DB;

class Que extends Base
{
    /**
     * Dispatch a job to the specified queue (or default queue).
     *
     * @param   \NizarBlond\LaravelPlus\Abstracts\JobBase   $job
     * @param   string                                      $queue
     * @return  mixed
     */
    public static function dispatch(JobBase $job, $queue = null)
    {
        $queue = !empty($queue) ? $queue : QueConfig::default();
        if ($queue) {
            $job->onQueue($queue);
        }
        return dispatch($job);
    }

    /**
     * Lists all jobs available on the specified queue (or default queue).
     *
     * @param   string  $queue
     * @param   string  $tableName
     * @return  array
     */
    public static function listJobs($queue = null, $tableName = 'jobs')
    {
        if (env('QUEUE_DRIVER') != 'database') {
            self::exception('Queue driver is not database.');
        }
        $queue = !empty($queue) ? $queue : QueConfig::default();
        return DB::select("select * from $tableName where queue = :queue", [
            'queue' => $queue
        ]);
    }

    /**
     * Lists all jobs available on the specified queue (or default queue).
     *
     * @param   string  $queue
     * @param   string  $tableName
     * @return  array
     */
    public static function info($queue = null)
    {
        $queue = !empty($queue) ? $queue : QueConfig::default();
        $queueDriver = env('QUEUE_DRIVER');
        $list = [];
        if ($queueDriver == 'database') {
            $list = DB::select("select * from jobs where queue = :queue", [
                'queue' => $queue
            ]);
        } elseif ($queueDriver == 'redis') {
            $connection = Queue::getConnection();
            $data = [];
            $data['primary'] = $connection->lrange('queues:' . $queue, 0, -1);
            $data['notify'] = $connection->lrange('queues:' . $queue . ':notify', 0, -1);
            $data['delayed'] = $connection->zrange('queues:' . $queue . ':delayed', 0, -1);
            $data['reserved'] = $connection->zrange('queues:' . $queue . ':reserved', 0, -1);
            $list = Arr::map($data, function ($item) { 
                return Arr::map($item, function ($item) { 
                    return Str::jsonDecode($item); 
                });
            });
        }   

        return [
            'queue' => $queue,
            'connection_class' => get_class($connection),
            'client_class' => get_class($connection->client()),
            'driver' => env('QUEUE_DRIVER'),
            'size' => Queue::size($queue),
            'lists' => $list,
        ];
    }

    /**
     * Returns the size of the specified queue (or default queue).
     *
     * @param   string  $queue
     * @return  int
     */
    public static function size($queue = null)
    {
        $queue = !empty($queue) ? $queue : QueConfig::default();
        return Queue::size($queue);
    }
}
