<?php

namespace NizarBlond\LaravelPlus\Support;

use Validator;

class Req extends Base
{
    /**
     * Validates request using Laravel validator and responds with
     * a custom error message.
     *
     * @param  \Illuminate\Http\Request     $request
     * @param  array                        $rules
     * @param  mixed                        $validator
     *
     * @return bool
     */
    public static function validRequest($request, $rules, &$validator = null)
    {
        return self::validInput($request->all(), $rules, $validator);
    }

    /**
     * Validates input using Laravel validator and responds with
     * a custom error message.
     *
     * @param  array    $input
     * @param  array    $rules
     * @param  mixed    $validator
     *
     * @return bool
     */
    public static function validInput($input, $rules, &$validator = null)
    {
        $validator = Validator::make($input, $rules);
        return !$validator->fails();
    }

    /**
     * Validates input using Laravel validator and fails if invalid.
     *
     * @param  array    $request
     * @param  array    $rules
     * @param  mixed    $error
     *
     * @return bool|array
     */
    public static function validInputOrFail($input, $rules, &$error = null)
    {
        if (! self::validInput($input, $rules, $validator)) {
            $errors = self::prettifyValidatorError($validator);
            if (empty($errors)) {
                self::exception('Invalid input');
            }
            $error = $errors[0]['error'];
            self::exception($error);
        }
    }

    /**
     * Validates input using Laravel validator. The function fails if
     * the input is invalid, but in case of valid input, it tries to
     * set default values for non-required fields.
     *
     * @param  array    $request
     * @param  array    $rules
     * @param  array    $defaults
     * @param  mixed    $error
     *
     * @return array
     */
    public static function validInputOrDefaults($input, $rules, $defaults = [], &$error = null)
    {
        self::validInputOrFail($input, $rules, $error);

        if (!empty($defaults)) {
            foreach ($defaults as $key => $value) {
                if (Arr::has($input, $key)) {
                    continue;
                }

                Arr::set($input, $key, $value);
            }
        }

        return $input;
    }

    /**
     * Prettify validator error response.
     *
     * @param  \Illuminate\Http\Validator  $validator
     *
     * @return array
     */
    public static function prettifyValidatorError($validator)
    {
        $result = [];

        $messages = $validator->errors()->toArray();
        if (! empty($messages)) {
            foreach ($messages as $field => $errors) {
                foreach ($errors as $error) {
                    $result[] = [
                        'field' => $field,
                        'error' => $error,
                    ];
                }
            }
        }

        return $result;
    }
}
