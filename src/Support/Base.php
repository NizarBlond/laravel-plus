<?php

namespace NizarBlond\LaravelPlus\Support;

use NizarBlond\LaravelPlus\Traits\ClassHelpers;

class Base
{
    use ClassHelpers;
}
