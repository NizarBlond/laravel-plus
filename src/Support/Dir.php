<?php

namespace NizarBlond\LaravelPlus\Support;

use NizarBlond\LaravelPlus\Config\File as FileConfig;
use Exception;

class Dir extends Base
{
    /**
     * Determines whether the file is a directory.
     *
     * @param   string  $file
     * @param   boolean $resetCache
     *
     * @return  boolean
     */
    public static function isDir($file, $resetCache = false)
    {
        if ($resetCache) {
            clearstatcache();
        }
        return is_dir($file);
    }

    /**
     * Determines whether the dir is readable.
     *
     * @param   string  $dir
     *
     * @return  boolean
     */
    public static function isReadable($dir)
    {
        return is_readable($dir);
    }

    /**
     * Determines whether the dir exists and is writable.
     *
     * @param   string  $dir
     *
     * @return  boolean
     */
    public static function isWritable($dir)
    {
        return is_writable($dir);
    }

    /**
     * Determines whether the dir is empty.
     *
     * @param   string  $dir
     *
     * @return  boolean
     */
    public static function isEmpty($dir)
    {
        return self::isDir($dir) && count(self::scandir($dir)) <= 2;
    }

    /**
     * List files and directories inside the specified path
     *
     * @param   string  $dir
     *
     * @return  array
     */
    public static function scandir($dir)
    {
        return scandir($dir);
    }

    /**
     * List files and directories inside the specified path.
     *
     * @param   string  $path
     * @param   bool    $onlyDir
     * @param   bool    $basename
     *
     * @return  array
     */
    public static function scan($path, $onlyDir = false, $basename = false)
    {
        $files = glob(Str::joinPaths($path, "/*"), $onlyDir ? GLOB_ONLYDIR : 0);
        return $basename ? array_map("basename", $files) : $files;
    }

    /**
     * List files and directories inside the specified path recursively.
     *
     * @param   string  $dir
     * @param   bool    $skipDots
     * @param   int     $maxDepth
     *
     * @return  array
     */
    public static function scanRecursive(
        string $dir,
        bool $skipDots = false,
        int $maxDepth = null,
        bool $onlyDir = false
    ) {
        if (!Dir::exists($dir)) {
            return [];
        }
        // Directory
        $di = new \RecursiveDirectoryIterator($dir);
        if ($skipDots) {
            $di->setFlags(\RecursiveDirectoryIterator::SKIP_DOTS);
        }
        // Iterator
        $it = new \RecursiveIteratorIterator($di);
        if (!is_null($maxDepth)) {
            $it->setMaxDepth($maxDepth);
        }
        // Search
        $objects = [];
        foreach ($it as $object) {
            if (!$onlyDir || $it->isDir()) {
                $objects[] = $object->getPathname();
            }
        }
        return $objects;
    }

    /**
     * Recursive files scanner.
     *
     * @param   string  $dir
     * @param   array   $extensions
     * @param   array   $exclude
     * @param   int     $maxDepth
     * @param   bool    $skipDots
     *
     * @return  array
     */
    public static function scanFilesRecursive(
        string $dir,
        array $extensions = [],
        array $exclude = [],
        int $maxDepth = null,
        bool $skipDots = false,
        bool $allowSymLinks = false
    ) {
        if (!Dir::exists($dir)) {
            return [];
        }
        // Directory
        $di = new \RecursiveDirectoryIterator($dir);
        // Exclude files / directories
        if (!empty($exclude) || !empty($extensions)) {
            $di = new \RecursiveCallbackFilterIterator($di, function ($file, $key, $it) use ($exclude, $extensions, $allowSymLinks) {
                // Allow recursion
                if ($it->hasChildren()) {
                    return true;
                }
                // Do not allow items that are not files (allow symlinks if specified)
                if (!$file->isFile() && (!$allowSymLinks || !$file->isLink())) {
                    return false;
                }
                // Do not allow extensions other than the specified
                if (!empty($extensions) && !in_array($file->getExtension(), $extensions)) {
                    return false;
                }
                // Do not allow files with excluded paths
                if (!empty($exclude) && Str::containsAny($file->getPathname(), $exclude)) {
                    return false;
                }
                return true;
            });
        }
        // Skip dots
        if ($skipDots) {
            $di->setFlags(\RecursiveDirectoryIterator::SKIP_DOTS);
        }
        // Iterator
        $it = new \RecursiveIteratorIterator($di);
        if (!is_null($maxDepth)) {
            $it->setMaxDepth($maxDepth);
        }
        // Filter
        $files = [];
        foreach ($it as $file) {
            $files[] = $file->getPathname();
        }
        return $files;
    }

    /**
     * Recursive glob function.
     *
     * @param   string  $base
     * @param   string  $pattern
     * @param   int     $flags
     *
     * @return  array
     */
    public static function glob($base, $pattern, $flags = 0)
    {
        if (substr($base, -1) !== DIRECTORY_SEPARATOR) {
            $base .= DIRECTORY_SEPARATOR;
        }

        $files = glob($base.$pattern, $flags);
        foreach (glob($base.'*', GLOB_ONLYDIR|GLOB_NOSORT|GLOB_MARK) as $dir) {
            $dirFiles = self::glob($dir, $pattern, $flags);
            if ($dirFiles !== false) {
                $files = array_merge($files, $dirFiles);
            }
        }

        return $files;
    }

    /**
     * List files inside the specified path.
     *
     * @param   string          $dir            The directory path.
     * @param   string|array    $extensions     The extensions filter.
     * @param   boolean         $includeDirs    Whether to include directories or not.
     *
     * @return  array
     */
    public static function files(string $dir, $extensions = "", bool $includeDirs = false)
    {
        if (is_array($extensions)) {
            $extensions = implode(',', $extensions);
        }
        $suffix = empty($extensions) ? "" : sprintf('.{%s}', $extensions);
        if (Str::endsWith($dir, '/*')) {
            $pattern = $dir . $suffix;
        } else {
            $pattern = Str::joinPaths($dir, "/*" . $suffix);
        }
        $files = glob($pattern, GLOB_BRACE);
        if (!$includeDirs) {
            $files = array_filter($files, 'is_file');
        }
        return $files;
    }

    /**
     * Lists files with the specified substring in filename.
     * 
     * @param   string  $dir
     * @param   string  $substring
     * @param   string  $extensions
     * 
     * @return  array
     */
    public static function filesWithSubstring(string $dir, string $substring, $extensions = "")
    {
        $files = self::files($dir, $extensions);
        return array_filter($files, function ($file) use ($substring) {
            return Str::contains($file, $substring);
        });
    }

    /**
     * Check whether the directory exists.
     *
     * @param   string      $dir
     * @param   boolean     $clearCache
     *
     * @return  boolean
     */
    public static function exists($dir, $clearCache = false)
    {
        if ($clearCache) {
            clearstatcache();
        }
        return is_dir($dir);
    }

    /**
     * Checks whether the string is a valid directory name.
     *
     * Reference:
     * http://stackoverflow.com/questions/12680061/check-a-string-for-a-valid-directory-name
     *
     * @param string $name  The directory name.
     *
     * @return bool
     */
    public static function validName($name)
    {
        return strpbrk($name, "\\/?%*:|\"<>") === false;
    }

    /**
     * Creates a new directory.
     *
     * @param   string  $dir
     *
     * @return  boolean
     */
    public static function make($dir, $errorIfExists = true, $suppressException = true)
    {
        if (self::exists($dir)) {
            if ($errorIfExists) {
                self::exception("Folder with the same name ('$dir') already exists.");
            } else {
                return true;
            }
        }
        
        $result = @mkdir($dir, 0777, true);
        if (!$result && !$suppressException) {
            $error = error_get_last();
            self::exception("Failed to create directory: {$error['message']}");
        }

        return $result;
    }

    /**
     * Creates a new dir if not exists.
     *
     * @param   string      $dir
     * @param   boolean     $suppressException
     *
     * @return  boolean
     */
    public static function makeIfNotExists($dir, $suppressException = true)
    {
        return self::make($dir, false, $suppressException);
    }

    /**
     * Create a temporary directory.
     *
     * @param   string  $dir
     *
     * @return  void
     */
    public static function makeTemp()
    {
        $dir = tempnam(sys_get_temp_dir(), '');
        return self::make($dir);
    }

    /**
     * Delete the directory.
     *
     * Note that $trash takes effect only if a valid recycle bin path
     * is available in laravel-config.php.
     *
     * @param   string $dir
     *
     * @return  void
     */
    public static function delete($dir, $trash = true)
    {
        if (!is_dir($dir)) {
            return false;
        }

        if ($trash) {
            $recycleBin = FileConfig::recycleBinPath();
            if (!empty($recycleBin)) {
                $dst = Str::joinPaths($recycleBin, $dir . '_' . time());
                return self::copy($dir, $dst, true);
            }
        }

        return self::rdelete($dir);
    }

    /**
     * Delete the directory recursively using PHP functions.
     *
     * @param   string $dir
     *
     * @return  void
     */
    public static function rdelete($dir)
    {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir."/".$object) == "dir") {
                    self::rdelete($dir."/".$object);
                } else {
                    @unlink($dir."/".$object);
                }
            }
        }
        reset($objects);
        return rmdir($dir);
    }

    /**
     * Force delete the directory.
     *
     * @param   string $dir
     *
     * @return  void
     */
    public static function forceDelete($dir)
    {
        $recycleBin = FileConfig::recycleBinPath();
        if (!empty($recycleBin)) {
            $dst = Str::joinPaths($recycleBin, basename($dir));
            $command = sprintf('mkdir -p %s && mv %s %s', $dst, $dir, $dst);
        } else {
            $command = sprintf('rm -rf %s', $dir);
        }
        return exec($command) !== false;
    }

    /**
     * Returns the directory size.
     *
     * @param   string $dir
     *
     * @return  int
     */
    public static function size(string $dir)
    {
        return self::size2($dir);
    }

    /**
     * Returns the directory size using recursive iterators.
     *
     * Reference:
     * https://stackoverflow.com/questions/478121/how-to-get-directory-size-in-php
     *
     * @param   string  $path
     * @param   int     $flags
     *
     * @return  int
     */
    public static function size2(string $path, int $flags = \FilesystemIterator::KEY_AS_PATHNAME)
    {
        $bytestotal = 0;
        $path = realpath($path);
        if (!empty($path) && file_exists($path)) {
            foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, $flags)) as $object) {
                if ($object->isFile()) {
                    $bytestotal += $object->getSize();
                }
            }
        }
        return $bytestotal;
    }

    /**
     * Returns the directory size using linux command (most precise but slower).
     *
     * @param   string  $path
     *
     * @return  int
     */
    public static function size3(string $path)
    {
        if (self::exists($path)) {
            return intval(exec("du -sb {$path}"));
        } elseif (File::exists($path)) {
            return File::size($path);
        }
        return 0;
    }

    /**
     * Removes broken symlinks from the specified dir.
     *
     * Reference:
     * https://stackoverflow.com/questions/30158529/finding-broken-symlinks-using-php/40647608
     *
     * @param   string $dir
     *
     * @return  array   List of removed paths.
     */
    public static function removeBrokenSymlinks($dir)
    {
        $removed = [];
        $files = scandir($dir);
        if (!empty($files)) {
            foreach ($files as $entry) {
                $path = $dir . DIRECTORY_SEPARATOR . $entry;
                if (is_link($path) && !file_exists($path)) {
                    $removed[] = $path;
                    @unlink($path);
                }
            }
        }
        return $removed;
    }

    /**
     * Recursively copies one dir to another.
     *
     * @param   string  $src
     * @param   string  $dst
     * @param   bool    $deleteOriginal
     *
     * @return  bool
     */
    public static function copy($src, $dst, $deleteOriginal = false)
    {
        $srcSize = self::size($src);
        self::rcopy($src, $dst);
        $dstSize = self::size($dst);
        if ($deleteOriginal) {
            self::rdelete($src);
        }
        return $srcSize === $dstSize;
    }

    /**
     * Recursively copies one dir to another.
     *
     * @param   string  $src
     * @param   string  $dst
     *
     * @return  void
     */
    public static function rcopy($dir, $dst)
    {
        self::makeIfNotExists($dst);
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir."/".$object) == "dir") {
                   self::rcopy($dir."/".$object, $dst."/".$object);
                } else {
                    @copy($dir."/".$object, $dst."/".$object);
                }
            }
        }
        reset($objects);
    }

    /**
     * Returns all files that exist in $dir1 but different in $dir2.
     *
     * Note that additional files in $dir2 are not compared.
     *
     * @param   string $dir1
     * @param   string $dir2
     * @param   array $extensions = []
     *
     * @return  array
     */
    public static function diffs(string $dir1, string $dir2, array $extensions = [])
    {
        $diffs = [];
        $files1 = self::scanFilesRecursive($dir1, $extensions);
        foreach ($files1 as $file1) {
            $file2 = Str::replaceFirst($dir1, $dir2, $file1);
            try {
                $identical = File::isIdentical($file1, $file2);
                if (!$identical) {
                    $diffs[$file2] = 1; // different
                }
            } catch (Exception $e) {
                $diffs[$file2] = 0; // missing
            }
        }
        return $diffs;
    }
}
