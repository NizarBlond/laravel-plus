<?php

namespace NizarBlond\LaravelPlus\Support;

use GuzzleHttp\Psr7\Response;

class Res extends Base
{
    /**
     * Maps an HTTP response status code to reason phrase.
     *
     * @param  integer    $code
     *
     * @return string
     */
    public static function getResponsePhrase($code)
    {
        $res = new Response($code);
        $text = $res->getReasonPhrase();

        // Additional responses
        if (!empty($text)) {
            return $text;
        }

        switch ($code) {
            case 499:
                // https://stackoverflow.com/questions/12973304/nginx-499-error-codes
                return 'Client Closed Connection.';
            default:
                return 'Unknown Response Reason';
        }
    }

    /**
     * Sanitizes the data and deletes sensitive information.
     *
     * @param array $data
     * @return array
     */
    public static function sanitizeData(array $data)
    {
        $sensitiveKeys = [
            'token',
            'pass',
            'password',
            'secret',
            'key',
            'api_key',
            'access_token',
            'auth_token',
        ];
    
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $data[$key] = self::sanitizeData($value);
            } elseif (is_string($value)) {
                foreach ($sensitiveKeys as $sensitiveKey) {
                    $_key = Str::replace(['-', '_', ' '], '', $key);
                    $_sensitiveKey = Str::replace(['-', '_', ' '], '', $sensitiveKey);
                    if ($_sensitiveKey === $_key) {
                        $data[$key] = '***';
                    }
                }
            }
        }
        return $data;
    }
}