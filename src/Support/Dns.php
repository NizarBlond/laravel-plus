<?php

namespace NizarBlond\LaravelPlus\Support;

use Exception;

class Dns extends Base
{
    const PRIMARY_DNS_SERVER = 'cloudflare:1';
    const SECONDARY_DNS_SERVER = 'google:1';
    const DNS_SERVERS = [
        'local' => null,
        'cloudflare:1' => '1.1.1.1',
        'cloudflare:2' => '1.0.0.1',
        'google:1' => '8.8.8.8',
        'google:2' => '8.8.4.4',
        'quad9:1' => '9.9.9.9',
        'quad9:2' => '149.112.112.112',
        'cisco:1' => '208.67.222.222',
        'cisco:2' => '208.67.220.220'
    ];

    /**
     * Checks whether the record have been propagated.
     *
     * @param string $record
     * @param string $type
     * @param array $expected
     * @param string|null $successApi
     * @param array|null $testedApis
     * @return bool
     */
    public static function testRecord(
        string $record,
        string $type,
        array $expected,
        &$successApi = null,
        &$testedApis = null
    ) {
        // Trim all dots from nameservers
        $expected = array_map(function ($_record) {
            return rtrim($_record, '.');
        }, $expected);

        // Check expected against resolved DNS
        $testedApis = [];
        foreach (self::DNS_SERVERS as $dnsServer => $serverIp) {
            $status = true;
            $testedApis[$dnsServer] = $serverIp;
            $resolvesTo = self::digRecord($record, $type, $serverIp);
            $values = $resolvesTo[0]['values'] ?? [];
            // If no records found then continue
            if (empty($values)) {
                continue;
            }
            // Test the expected against the resolved DNS data
            foreach ($values ?? [] as $target) {
                $target = rtrim($target, '.');
                if (!in_array($target, $expected)) {
                    $status = false;
                    break;
                }
            }
            // Otherwise, if status is ok then stop
            if ($status) {
                $successApi = $dnsServer;
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the DNS record was propagated with a remote DNS but not locally.
     *
     * @param string $record
     * @param string $type
     *
     * @return array
     */
    public static function isRecordPropagationDelayed(string $record, string $type, array $expected)
    {
        // Try local DNS server
        $local = self::digRecord($record, 'A', null);
        $localValues = $local[0]['values'] ?? null;
        if (Arr::isDataEqual($localValues, $expected)) {
            return false; // We got what we expect!
        }

        // Try other DNS servers
        $other = self::digRecord($record, 'A');
        $otherValues = $other[0]['values'] ?? null;
        if (Arr::isDataEqual($otherValues, $expected)) {
            return true; // In others propagated but not locally!
        }

        return false;
    }

    /**
     * Digs the DNS records.
     *
     * @param string $record
     * @param string $type
     * @param string|null $dnsServer
     *
     * @return array
     */
    public static function digRecord(string $record, string $type = 'ANY', ?string $dnsServer = '')
    {
        if ($dnsServer === '') {
            $dnsServer = self::DNS_SERVERS[self::PRIMARY_DNS_SERVER];
        }

        $cmd = sprintf(
            "/bin/dig %s %s %s | %s | %s",
            $dnsServer ? '@'.$dnsServer : '',
            $record,
            $type,
            "grep -vE '^;'", // Remove all lines starting with ;
            "grep -vE '^$'"  // Remove empty lines
        );

        try {
            $output = Proc::runCommandWithoutLog($cmd, 5, true, false);
            return self::parseDigOutput($output);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Resolves domain IPs.
     *
     * @param   array|string    $domains
     * @param   string          $recordType
     * @param   string          $expected
     *
     * @return array
     */
    public static function resolveIps($domains, string $recordType = 'A', ?string $expected = null)
    {
        $domains = is_string($domains) ? [ $domains ] : $domains;
        $retValue = [];
        $testedDnsServers = [];
        foreach ($domains as $domain) {
            $resolvesTo = null;
            $resolvedValues = [];
            $resolved = false;
            $domain = trim($domain);
            // Try getting dns record using different methods
            foreach (self::DNS_SERVERS as $dnsServer => $serverIp) {
                $result = self::digRecord($domain, $recordType, $serverIp);
                $testedDnsServers[$dnsServer] = $serverIp;
                foreach ($result ?? [] as $data) {
                    if ($data['type'] === $recordType && !empty($data['values'])) {
                        $resolvesTo = $data['values'][0];
                        $resolvedValues = $data['values'];
                        break;
                    }
                }
                if (!empty($resolvesTo)) {
                    // That's it, save the result for this domain
                    $retValue[$domain] = [
                        'domain'            => $domain,
                        'resolves_to'       => rtrim($resolvesTo, '_.'),
                        'resolved_values'   => array_map(function ($value) {
                            return rtrim($value, '_.');
                        }, $resolvedValues),
                        'dns_server'        => $dnsServer,
                        'record_type'       => $recordType,
                        'tested'            => $testedDnsServers
                    ];
                    $resolved = true;
                    // Check if the resolved IP points to the expected IP
                    if ($expected && !Url::equalAddresses($resolvesTo, $expected)) {
                        // Check if the IP belongs to a trusted reverse proxy
                        if ($recordType === 'A' && IpAddress::isTrustedReverseProxy($resolvesTo, 'v4')) {
                            $retValue[$domain]['ssl_warning'] = sprintf(
                                'The domain %s points to a reverse proxy. Please make sure that the reverse proxy points to %s.',
                                $domain,
                                $expected ?? 'n/a'
                            );
                            break;
                        }
                        // Keep trying to resolve IP via another DNS resolver...
                        continue;
                    }
                    // Check if AAAA record is valid
                    if ($recordType === 'A' && !Url::isValidAAAARecord($domain, $error)) {
                        $retValue[$domain]['ssl_warning'] = sprintf(
                            '%s. This might cause issues when attempting to generate SSL certificates for your website. Please remove your DNS AAAA record before you continue.',
                            rtrim($error, '.')
                        );
                    }
                    break;
                }
            }
            // If failed in both then return error
            if (!$resolved) {
                $retValue[$domain] = [
                    'domain'            => $domain,
                    'resolves_to'       => null,
                    'resolved_values'   => [],
                    'tested'            => $testedDnsServers
                ];
            }
        }
        return array_values($retValue);
    }

    /**
     * Digs the dns records.
     *
     * @param string|array $records
     * @return array
     */
    private static function parseDigOutput($output)
    {
        $lines = array_filter(preg_split('~[\r\n]+~', $output)); // split by new lines
        $lines = array_filter($lines); // remove empty lines
        if (empty($lines)) {
            return null;
        }
        $matches = [];
        $recordsByType = [];
        foreach ($lines as $i => $line) {
            // parse the output line
            if (!preg_match('/^(?P<name>[^\t\s]+)[\t\s]+(?P<ttl>\d+)[\t\s]+(?P<class>[A-Z]+)[\t\s]+(?P<type>[A-Z]+)[\t\s](?P<target>.*)$/', $lines[$i], $matches[$i])) {
                continue;
            }
            // remove numeric indexes
            $matches[$i] = array_filter($matches[$i], function ($key) {
                return !is_int($key);
            }, ARRAY_FILTER_USE_KEY);
            // aggregate matches by type
            $type = $matches[$i]['type'];
            if (isset($recordsByType[$type])) {
                $recordsByType[$type]['values'][] = $matches[$i]['target'];
                // $recordsByType[$type]['raw'][] = $lines[$i];
            } else {
                $recordsByType[$type] = $matches[$i];
                $recordsByType[$type]['values'] = [ $matches[$i]['target'] ];
                // $recordsByType[$type]['raw'] = [ $lines[$i] ];
            }
        }
        return array_values($recordsByType);
    }
}
