<?php

namespace NizarBlond\LaravelPlus\Support;

use Cache as LaravelCache;

class Cache extends Base
{
    /**
     * Default cache prefix for Laravel.
     *
     * @const string
     */
    const LARAVEL_CACHE_PREFIX = 'laravel:';

    /**
     * Store an item in the cache for a given number of seconds.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @param  int|null  $seconds
     * @return bool
     */
    public static function put($key, $value, $seconds = null)
    {
        if (!$seconds) {
            return self::forever($key, $value);
        }
        return LaravelCache::put($key, $value, $seconds);
    }

    /**
     * Store an item in the cache forever.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return bool
     */
    public static function forever($key, $value)
    {
        return LaravelCache::forever($key, $value);
    }

    /**
     * Retrieve an item from the cache by key.
     *
     * @param  string|array  $key
     * @return mixed
     */
    public static function get($key)
    {
        return LaravelCache::get($key);
    }

    /**
     * Forgets an item from cache.
     *
     * @param  string|array  $key
     * @return bool
     */
    public static function forget($key)
    {
        return LaravelCache::forget($key);
    }

    /**
     * Returns cached items by the specified prefix.
     *
     * @param  string  $prefix
     * @param  string  $driver
     * @return array
     */
    public static function getByPrefix($prefix, $driver = null)
    {
        $driver = $driver ?? env('CACHE_DRIVER');
        $retValue = [];

        if ($driver === 'redis') {
            // Get redis client
            $conn = LaravelCache::getRedis();
            // Find all keys
            $keys = $conn->keys(self::LARAVEL_CACHE_PREFIX . "$prefix*");
            // Remove laravel prefix
            $filteredKeys = array_map(function ($key) {
                return preg_replace("/^(".self::LARAVEL_CACHE_PREFIX.")(.+)/", '$2', $key);
            }, $keys);
            // Map keys to values
            $retValue = [];
            foreach ($filteredKeys ?? [] as $key) {
                $retValue[$key] = self::get($key);
            }
        } elseif ($driver === 'file') {
            // https://stackoverflow.com/questions/31791178/how-to-get-list-of-all-cached-items-by-key-in-laravel-5
        }

        return $retValue;
    }

    /**
     * Returns the TTL of the specified key.
     *
     * @param  string  $key
     * @param  string  $driver
     * @return int|null
     */
    public static function getKeyTtl($key, $driver = null)
    {
        $driver = $driver ?? env('CACHE_DRIVER');
        if ($driver === 'redis') {
            // Get redis client
            $conn = LaravelCache::getRedis();
            return $conn->ttl(self::LARAVEL_CACHE_PREFIX . "$key");
        }
        return null;
    }

    /**
     * Removes cached items by the specified prefix.
     *
     * @param  string  $prefix
     * @param  string  $driver
     * @return void
     */
    public static function removePrefix($prefix, $driver = null)
    {
        $keys = self::getByPrefix($prefix, $driver);
        foreach ($keys as $key => $value) {
            self::forget($key);
        }
    }
}
