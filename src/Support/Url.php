<?php

namespace NizarBlond\LaravelPlus\Support;

use NizarBlond\LaravelPlus\Config\LaravelPlus as LaravelPlusConfig;
use Pdp\Rules;
use Pdp\Domain;
use Spatie\Url\Url as SpatieUrl;
use Exception;

class Url extends Base
{
    /**
     * Checks whether a relative URL.
     *
     * @param string $url
     * @return bool
     */
    public static function isRelative(string $url)
    {
        if (parse_url($url, PHP_URL_SCHEME) || parse_url($url, PHP_URL_HOST)) {
            return false;
        }

        if (parse_url($url, PHP_URL_PATH)) {
            return true;
        }

        return empty(Url::getRegistrableDomain($url));
    }

    /**
     * Creates a normalised URL string.
     *
     * @param string $url
     * @param string $path
     * @param string $scheme
     * @param string $fragment
     * @return string
     */
    public static function createFromString($url, $path = '', $scheme = null, $fragment = null)
    {
        $url = SpatieUrl::create()->fromString($url);
        $path = Str::joinPaths($url->getPath(), $path);
        $scheme = is_null($scheme) ? $url->getScheme() : $scheme;
        $fragment = is_null($fragment) ? $url->getFragment() : $fragment;

        $urlStr = $url
            ->withScheme(empty($scheme) ? 'http' : $scheme)
            ->withPath($path)
            ->withFragment(empty($fragment) ? '' : $fragment)
            ->__toString();

        // BUG in spatie/url if URL provided without scheme it results in
        // the built URL being <scheme>:///
        return str_replace(':///', '://', $urlStr);
    }

    /**
     * Creates a normalised URL string from parts.
     *
     * @param string $url
     * @param string $host
     * @param string $path
     * @return string
     */
    public static function createFromParts($scheme, $host, $path)
    {
        return SpatieUrl::create()
            ->withScheme($scheme)
            ->withHost($host)
            ->withPath($path)
            ->__toString();
    }

    /**
     * Retrieves the normalized domain name from a URL.
     *
     * @param string $url
     * @return string
     */
    public static function normalizeHost(string $url)
    {
        $url = preg_replace('/^https?:\/\//', '', $url); // Remove http:// or https://
        $url = preg_replace('/\?.*/', '', $url); // Remove query string.
        $url = preg_replace('/\/.*/', '', $url); // Remove path.
        return $url;
    }

    /**
     * Gets the registrable domain of the specified url.
     *
     * @param string $url
     * @return string
     */
     public static function getRegistrableDomain(string $url)
     {
         static $cache = [];
         if (isset($cache[$url])) {
             return $cache[$url];
         }
         $parser = self::getDomainParser($url);
         $domain = $parser->registrableDomain()->toString();
         $cache[$url] = $domain;
         return $domain;
     }

    /**
     * Groups domains by registrable domain.
     *
     * @param array $domains
     * @return array
     */
    public static function groupByRegistrableDomain(array $domains)
    {
        $domains = array_unique($domains);
        $domainsGroup = [];
        foreach ($domains as $domain) {
            $registrableDomain = self::getRegistrableDomain($domain);
            $domainsGroup[$registrableDomain][] = $domain;
        }

        return $domainsGroup;
    }

    /**
     * Checks whether the host is a valid regex.
     *
     * @param string $host
     * @return bool
     */
    public static function validHostString(string $host)
    {
        // Reference: https://stackoverflow.com/questions/3026957/how-to-validate-a-domain-name-using-regex-php
        return preg_match(
            '/^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/',
            strtolower($host)
        );
    }

    /**
     * Checks whether the host is a valid URL.
     *
     * @param string $host
     * @return bool
     */
    public static function validHost(string $host)
    {
        return self::getDnsRecords($host) !== false;
    }

    /**
     * Checks whether the provided addresses are equal.
     *
     * @param string $addr1
     * @param string $addr2
     * @return bool
     */
    public static function equalAddresses(string $addr1, string $addr2)
    {
        return trim($addr1, '_.') === trim($addr2, '_.');
    }

    /**
     * Checks whether the second domain is a subdomain of the first.
     *
     * @param string $subdomain
     * @param string $domain
     * @return bool
     */
    public static function isSubdomain(string $subdomain, string $domain)
    {
        if (empty($subdomain) || empty($domain)) {
            self::exception('The domain or subdomain cannot be empty.');
        }

        $domain = rtrim($domain, '.');
        $subdomain = rtrim($subdomain, '.');

        if (Str::startsWith($subdomain, '.') || Str::startsWith($domain, '.')) {
            self::exception('The domain or subdomain cannot start with a dot.');
        }
        if (!Str::contains($domain, '.') || !Str::contains($subdomain, '.')) {
            self::exception('The domain or subdomain must include at least one dot.');
        }

        return self::equalAddresses($domain, $subdomain) || Str::endsWith($subdomain, '.' . $domain);
    }

    /**
     * Returns the host by address.
     *
     * @param string $ip
     * @return string|false
     */
    public static function getHostByAddress(string $ip)
    {
        return gethostbyaddr($ip);
    }

    /**
     * Returns the DNS records of the given DNS type(s).
     *
     * @param string $host
     * @param mixed $type
     * @param array|null $filterType
     * @return array|false
     */
    public static function getDnsRecords(string $host, $type = DNS_ANY, ?array $filterType = [ "A", "CNAME"])
    {
        $records = @dns_get_record($host, $type);
        if (empty($records)) {
            return false;
        }

        if (!empty($filterType)) {
            return array_filter($records, function ($record) use ($filterType) {
                return in_array($record['type'], $filterType);
            });
        }

        return $records;
    }

    /**
     * Returns the DNS record's IP.
     *
     * @param string $host
     * @return string|false
     */
    public static function getDnsRecordIp(string $host)
    {
        $records = self::getDnsRecords($host, DNS_A);
        if (! $records) {
            return false;
        }

        $typeARecords = array_filter($records, function ($key) {
            return $key['type'] === 'A';
        });

        if (empty($typeARecords)) {
            return false;
        }

        $firstARecord = reset($typeARecords);
        return !empty($firstARecord['ip']) ? $firstARecord['ip'] : false;
    }

    /**
     * Returns the DNS record's IPv6.
     *
     * @param string $host
     * @return string|false
     */
    public static function getDnsRecordIpV6(string $host)
    {
        $records = self::getDnsRecords($host, DNS_AAAA, [ 'AAAA' ]);
        if (! $records) {
            return false;
        }

        $typeARecords = array_filter($records, function ($key) {
            return $key['type'] === 'AAAA';
        });

        if (empty($typeARecords)) {
            return false;
        }

        $firstARecord = reset($typeARecords);
        return !empty($firstARecord['ipv6']) ? $firstARecord['ipv6'] : false;
    }

    /**
     * Maps the DNS record type to its ID.
     *
     * Reference:
     * https://en.wikipedia.org/wiki/List_of_DNS_record_types
     *
     * @param mixed $type
     * @return mixed
     */
    public static function getRRTypeId($type)
    {
        switch ($type) {
            case DNS_ANY:
            case 'ANY':
            case '*':
                return 255;
            case DNS_A:
            case 'A':
                return 1;
            case DNS_AAAA:
            case 'AAAA':
                return 28;
            case DNS_CNAME:
            case 'CNAME':
                return 5;
            case DNS_MX:
            case 'MX':
                return 12;
            case DNS_NS:
            case 'NS':
                return 2;
            case DNS_PTR:
            case 'PTR':
                return 12;
            case DNS_TXT:
            case 'TXT':
                return 16;
            default:
                return $type;
        }
    }

    /**
     * Resolves domain IPs using Google's JSON API for DNS over HTTPS (DoH).
     *
     * Reference:
     * https://developers.google.com/speed/public-dns/docs/doh/json
     *
     * @param   string  $domain
     * @param   string  $recordType
     * @param   bool    $returnAll
     *
     * @return  string|null
     */
    public static function getDnsRecordViaGoogleApi($domain, $recordType = DNS_ANY, $returnAll = false)
    {
        $type = self::getRRTypeId($recordType);
        $client = new \GuzzleHttp\Client;
        $googleUri = sprintf('https://dns.google/resolve?name=%s&type=%s', $domain, $type);
        $res = $client->request('GET', $googleUri, ['http_errors' => false]);
        if (!in_array($res->getStatusCode(), [200, 201, 202])) {
            return null;
        }
        $resBody = $res->getBody();
        if (!empty($resBody)) {
            $resAsStr = $resBody->getContents();
            $resAsArr = Str::jsonDecode($resAsStr);
            // If return all is provided then return all answers
            if ($returnAll) {
                return $resAsArr['Answer'] ?? [];
            }
            // Otherwise, return the first answer's data.
            foreach ($resAsArr['Answer'] ?? [] as $answer) {
                if ($type === 255 || $answer['type'] === $type) {
                    return $answer['data'];
                }
            }
        }
        return null;
    }

    /**
     * Parses the query from the URL.
     *
     * @param string $url
     * @return array
     */
    public static function parseQuery(string $url)
    {
        $query = parse_url($url, PHP_URL_QUERY);
        if ($query === false || is_null($query)) {
            self::exception('Failed to parse the query from the URL.');
        }
        return \GuzzleHttp\Psr7\Query::parse($query);
    }

    /**
     * Parses the host from the URL.
     *
     * @param string $url
     * @return array
     */
    public static function parseHost(string $url)
    {
        $host = self::removeScheme($url);
        $host = preg_replace('/\/.*/', '', $host);
        $host = preg_replace('/\?.*/', '', $host);
        return $host;
    }

    /**
     * Removes the scheme from the URL.
     *
     * @param string $url
     * @return string
     */
    public static function removeScheme(string $url)
    {
        return preg_replace('/^\s*(http)?s?:?\/\//', '', $url);
    }

    /**
     * Checks whether the setup of the AAAA record exist and is valid.
     *
     * @param string $domain
     * @param string|null $reason
     * @param string|null $ipv4
     * @return bool
     */
    public static function isValidAAAARecord(string $domain, ?string &$reason, ?string $ipv4 = null)
    {
        $reason = null;

        $ipv6 = self::getDnsRecordIpV6($domain);
        if (empty($ipv6)) {
            $reason = 'The domain AAAA record (IPv6) is empty.';
            return true;
        }

        $ipv4 = $ipv4 ?? self::getDnsRecordIp($domain);
        if (empty($ipv4)) {
            $reason = 'The domain A record (IPv4) is empty.';
            return true;
        }

        // Check if they point to the same host
        $ipv4Host = self::getHostByAddress($ipv4);
        $ipv6Host = self::getHostByAddress($ipv6);
        if ($ipv4Host === $ipv6Host) {
            $reason = 'The domain AAAA record (IPv6) is valid.';
            return true;
        }

        $reason = 'Set the domain AAAA record (IPv6) to the same host as the A record (IPv4) or remove it.';
        return false;
    }

    /**
     * Tests whether the SSL certificate is valid.
     *
     * @param string $url
     * @return bool
     * @throws \Exception
     */
    public static function isValidSSL(string $url)
    {
        $host = preg_replace('#^https?://#', '', $url);

        try {
            get_headers('https://' . $host);
        } catch (Exception $e) {
            if (Str::contains($e->getMessage(), 'did not match expected CN')) {
                return false;
            }
            throw $e;
        }
        return true;
    }

    /**
     * Returns the domain parser.
     * 
     * @param string $url
     * @return \Pdp\ResolvedDomainName
     */
    public static function getDomainParser(string $url)
    {
        static $publicSuffixList = null;

        if (is_null($publicSuffixList)) {
            $publicSiffixData = LaravelPlusConfig::rootPath() . '/data/public_suffix_list.dat';
            if (File::missing($publicSiffixData)) {
                self::exception('Public suffix list data file not found.');
            }
            $publicSuffixList = Rules::fromPath($publicSiffixData);
        }

        $host = self::normalizeHost($url);
        $domain = Domain::fromIDNA2008($host);
        return $publicSuffixList->resolve($domain);
    }
}
