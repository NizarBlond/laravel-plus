<?php

namespace NizarBlond\LaravelPlus\Support;

use Symfony\Component\Process\Process as SymfonyProcess;
use Symfony\Component\Process\Exception\RuntimeException as SymfonyRuntimeException;
use NizarBlond\LaravelPlus\Services\ActivityLogger;
use NizarBlond\LaravelPlus\Config\Proc as ProcConfig;
use NizarBlond\LaravelPlus\Constants\RecordPriority;

class Proc extends Base
{
    /**
     * The activity type to be used in log.
     *
     * @var string
     */
    const ACTIVITY_TYPE = 'proc';

    /**
     * Runs a shell script.
     *
     * @param   string    $file
     * @param   array     $args
     * @param   int       $timeout
     * @param   bool      $logFailure
     * @param   bool      $logSuccess
     * @param   string    $errorOutput
     *
     * @return  string
     *
     * @throws  \Exception
     */
    public static function runScript(
        string $file,
        array $args = [],
        ?int $timeout = null,
        bool $throwException = true,
        bool $logFailure = true,
        bool $logSuccess = true,
        ?string &$errorOutput = null
    ) {
        if (!File::exists($file)) {
            self::exception("Invalid script file '$file'");
        }

        $process = new SymfonyProcess(array_merge([ $file ], $args));
        return self::run(
            $process,
            $timeout,
            basename($file),
            $args,
            $throwException,
            $logFailure,
            $logSuccess,
            $errorOutput
        );
    }

    /**
     * Runs command without logging record on success.
     *
     * @param   string|array    $command
     * @param   int             $timeout
     * @param   bool            $throwException
     * @param   bool            $logFailure
     * @param   string          $errorOutput
     *
     * @return  string
     *
     * @throws  \Exception
     */
    public static function runCommandWithoutLog(
        $command,
        ?int $timeout = null,
        bool $throwException = true,
        bool $logFailure = true,
        ?string &$errorOutput = null
    ) {
        return self::runCommand($command, $timeout, $throwException, $logFailure, null, false, $errorOutput);
    }

    /**
     * Runs a shell command.
     *
     * The command must be the command to run and its arguments listed as separate entries.
     *
     * @param   string|array    $command
     * @param   int             $timeout
     * @param   bool            $throwException
     * @param   bool            $logFailure
     * @param   string          $workingDir
     * @param   bool            $logSuccess
     * @param   string          $errorOutput
     *
     * @return  string
     *
     * @throws  \Exception
     */
    public static function runCommand(
        $command,
        ?int $timeout = null,
        bool $throwException = true,
        bool $logFailure = true,
        ?string $workingDir = null,
        bool $logSuccess = true,
        ?string &$errorOutput = null
    ) {
        if (is_array($command)) {
            $process = new SymfonyProcess($command);
        } elseif (is_string($command)) {
            $process = SymfonyProcess::fromShellCommandline($command);
        } else {
            self::exception("Invalid command type.");
        }

        if (!empty($workingDir)) {
            $process->setWorkingDirectory($workingDir);
        }

        return self::run(
            $process,
            $timeout,
            'shell_cmd',
            [ 'command' => $command ],
            $throwException,
            $logFailure,
            $logSuccess,
            $errorOutput
        );
    }

    /**
     * Runs a shell command.
     *
     * @param   array   $command
     * @param   int     $timeout
     * @param   bool    $throwException
     * @param   bool    $logFailure
     *
     * @return  string
     *
     * @throws  \Exception
     */
    public static function runCommands(
        array $commands,
        ?int $timeout = null,
        bool $throwException = true,
        bool $logFailure = true,
        bool $logSuccess = true
    ) {
        $output = null;
        foreach ($commands as $command) {
            $output = self::runCommand($command, $timeout, $throwException, $logFailure, null, $logSuccess);
            if (!$throwException && $output === false) {
                return false;
            }
        }

        return $output;
    }

    /**
     * Returns the current user running the process.
     *
     * @return string
     */
    public static function whoAmI()
    {
        return exec('whoami');
    }

    /**
     * Returns the PID of the current process.
     *
     * @return int
     */
    public static function currentPID()
    {
        return getmypid();
    }

    /**
     * Returns the current user running the process.
     *
     * @param boolean $fromCache
     *
     * @return string
     */
    public static function currentUser(bool $fromCache = true)
    {
        static $current = null;

        if ($fromCache && !is_null($current)) {
            return $current;
        }

        // Using POSIX function
        if (function_exists('posix_getpwuid')) {
            $username = posix_getpwuid(posix_geteuid())['name'];
        }

        // Otherwise, try to getting user from whoami command
        if (empty($username)) {
            $username = self::whoAmI();
        }

        $current = $username;
        return $username;
    }

    /**
     * Returns whether the current user is root.
     *
     * @return void
     */
    public static function isRootUser()
    {
        return self::currentUser() === 'root';
    }

    /**
     * Runs the process.
     *
     * @param   \Symfony\Component\Process\Process  $process
     * @param   int                                 $timeout
     * @param   string                              $logName
     * @param   array                               $logData
     * @param   bool                                $throwException
     * @param   bool                                $logFailure
     * @param   bool                                $logSuccess
     * @param   string                              $errorOutput
     *
     * @return  string
     *
     * @throws  \Exception
     */
    private static function run(
        SymfonyProcess $process,
        ?int $timeout = null,
        ?string $logName = null,
        array $logData = [],
        bool $throwException = true,
        bool $logFailure = true,
        bool $logSuccess = true,
        ?string &$errorOutput = null
    ) {
        // Set timeout
        $process->setTimeout(
            !empty($timeout) ? $timeout : ProcConfig::defaultTimeout()
        );

        // Start stopwatch
        $startTime = time();

        // Log output in realtime
        $rtOutput = [
            'err' => [],
            'out' => []
        ];

        // Run process
        $errorOutput = null;
        $failureException = null;
        try {
            $process->run(function ($type, $buffer) use ($rtOutput) {
                if (SymfonyProcess::ERR === $type) {
                    $rtOutput['err'][] = $buffer;
                } else {
                    $rtOutput['out'][] = $buffer;
                }
            });
        } catch (SymfonyRuntimeException $e) {
            $failureException = $e;
        }

        // Get process run duration
        $duration = time() - $startTime; // seconds
    
        // Check process result status
        if (!$process->isSuccessful() || !empty($failureException)) {
            // If error is an exception then log exception message as
            // the  error message; otherwise, print whole error buffer
            $error = !empty($failureException)
                        ? sprintf(
                            "%s (Error Output=%s)",
                            $failureException->getMessage(),
                            json_encode($rtOutput['err'])
                        )
                        : $process->getErrorOutput();

            // Save the error output reference
            $errorOutput = $process->getErrorOutput();

            // If error is empty at least print exit code text
            if (empty($error)) {
                $error = $process->getExitCodeText();
            }

            if ($logFailure) {
                self::recordFailedActivity(
                    $startTime,
                    $logName,
                    $logData,
                    $error,
                    $duration,
                    empty($process->getOutput()) ? $rtOutput['out'] : $process->getOutput()
                );
            }

            if ($throwException) {
                self::exception($error);
            } else {
                return false;
            }
        }

        if ($logSuccess) {
            self::recordActivity(
                $startTime,
                $logName,
                $logData,
                $process->getOutput(),
                $duration
            );
        }

        // Even if the process is successful, we still need to save the error output
        // in case of commands that return success exit code but still have error output.
        $errorOutput = $process->getErrorOutput();

        return $process->getOutput();
    }

    /**
     * Records script activity.
     *
     * @param   integer   $startedAt  The start time timestamp.
     * @param   string    $scriptId   The script ID.
     * @param   string    $args       The script args.
     * @param   string    $output     The script output.
     * @param   string    $duration   The script run duration.
     *
     * @return  \NizarBlond\LaravelPlus\Models\ActivityLog
     */
    private static function recordActivity(
        $startedAt,
        $scriptId,
        $args,
        $output,
        $duration
    ) {
        return ActivityLogger::record(
            $startedAt, // scheduled at
            $startedAt,
            $scriptId,
            $args,
            $output,
            self::ACTIVITY_TYPE,
            RecordPriority::HIGH,
            [],
            $duration
        );
    }

    /**
     * Records script failed activity.
     *
     * @param   integer   $startedAt  The start time timestamp.
     * @param   string    $scriptId   The script ID.
     * @param   string    $args       The script args.
     * @param   string    $error      The script error.
     * @param   string    $duration   The script run duration.
     * @param   string    $output     The script output.
     *
     * @return  \NizarBlond\LaravelPlus\Models\ActivityLog
     */
    private static function recordFailedActivity(
        $startedAt,
        $scriptId,
        $args,
        $error,
        $duration,
        $output
    ) {
        return ActivityLogger::recordFailure(
            $startedAt, // scheduled at
            $startedAt,
            $scriptId,
            $args,
            $error,
            self::ACTIVITY_TYPE,
            RecordPriority::HIGH,
            [],
            $duration,
            $output
        );
    }
}
