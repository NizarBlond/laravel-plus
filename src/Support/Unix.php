<?php

namespace NizarBlond\LaravelPlus\Support;

class Unix extends Base
{
    /**
     * Retrieves the system's temporary directory.
     *
     * @return  string
     */
    public static function getTempDir()
    {
        return sys_get_temp_dir();
    }

    /**
     * Returns the system load average in the last 1, 5 and 15 minutes.
     *
     * @return array
     */
    public static function getSystemLoadAverage()
    {
        $load = sys_getloadavg();
        
        return [
            'cpus' => self::getCpusNumber(),
            'load' => [
                'last_1_min'    => $load[0],
                'last_5_mins'   => $load[1],
                'last_15_mins'  => $load[2],
            ]
        ];
    }

    /**
     * Returns CPUs number.
     *
     * Reference:
     * https://stackoverflow.com/questions/36970270/how-to-calculate-number-of-processor-cores-in-php-script-linux
     *
     * @return integer
     */
    public static function getCpusNumber()
    {
        $command = "cat /proc/cpuinfo | grep processor | wc -l";
        return  (int) shell_exec($command);
    }

    /**
     * Returns memory usage status.
     *
     * Reference:
     * https://stackoverflow.com/questions/22949295/how-do-you-get-server-cpu-usage-and-ram-usage-with-php
     *
     * @return array
     */
    public static function getMemoryUsage()
    {
        $mem_line = [];
        $swap_line = [];

        $free = shell_exec('free -b');
        $free = trim($free);
        $free_arr = explode("\n", $free);

        foreach ($free_arr as $line) {
            $line_arr = explode(" ", $line);
            if ($line_arr[0] === 'Mem:') {
                $mem_line = array_values(array_filter($line_arr));
            }
            if ($line_arr[0] === 'Swap:') {
                $swap_line = array_values(array_filter($line_arr));
            }
        }

        return [
            'total_memory'      => $mem_line[1],
            'used_memory'       => $mem_line[2],
            'free_memory'       => $mem_line[3],
            'shared_memory'     => $mem_line[4],
            'buff_cache_memory' => $mem_line[5],
            'available_memory'  => $mem_line[6],
            'total_swap'        => !empty($swap_line[1]) ? $swap_line[1] : 0,
            'used_swap'         => !empty($swap_line[2]) ? $swap_line[2] : 0,
            'free_swap'         => !empty($swap_line[3]) ? $swap_line[3] : 0,
        ];
    }

    /**
     * Returns disk space usage status.
     *
     * @return array
     */
    public static function getDiskspaceUsage()
    {
        $ds = disk_total_space('/');
        $df = disk_free_space('/');

        return [
            'total' => $ds,
            'used'  => $ds - $df,
            'free'  => $df
        ];
    }

    /**
     * Parses ps-aux output to an array.
     *
     * Reference:
     * https://man7.org/linux/man-pages/man1/ps.1.html
     * https://github.com/LucaKling/ps-aux-Analyzer/blob/master/analyzer.php
     *
     * @return array
     */
    public static function listProcesses()
    {
        // Constructing pattern
        $pattern = '/'; // Start
        $pattern .= '([\S]+)'; // USER - 1
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // PID - 2
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // %CPU - 3
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // %MEM - 4
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // STAT - 5
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // START - 6
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // ELAPSED - 7
        $pattern .= '[\s]+';
        $pattern .= '([\S\s]+)'; // COMMAND - 8
        $pattern .= '/'; // End

        // Running
        $shell_output = shell_exec('ps axo user:25,pid,pcpu,pmem,stat,start,etimes,command:100');
        $shell_output = explode("\n", $shell_output);
        unset($shell_output[0], $shell_output[count($shell_output)]); // Unset table header and last (empty) line
        $matches = null;
        foreach ($shell_output as $pid => $value) {
            preg_match($pattern, $value, $matches[]);
        }

        // Creating array with values
        $ps = [];
        foreach ($matches ?? [] as $value) {
            $pid = $value[2];
            if (isset($ps[$pid])) {
                $_pid = sprintf("%s_%s", $pid, Str::random(5));
                $pid = $_pid;
            }
            $ps['%CPU'][$pid] = $value[3];
            $ps['%MEM'][$pid] = $value[4];
            $ps['USER'][$pid] = $value[1];
            $ps['STAT'][$pid] = $value[5];
            $ps['START'][$pid] = $value[6];
            $ps['ELAPSED'][$pid] = $value[7];
            $ps['COMMAND'][$pid] = $value[8];
        }

        return $ps;
    }

    /**
     * Lists systemctl service units.
     *
     * @return array
     */
    public static function listServices()
    {
        // Constructing pattern
        $pattern = '/'; // Start
        $pattern .= '([\S]+)'; // UNIT - 1
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // LOAD - 2
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // ACTIVE - 3
        $pattern .= '[\s]+';
        $pattern .= '([\S]+)'; // SUB - 3
        $pattern .= '[\s]+';
        $pattern .= '([\S\s]+)'; // DESCRIPTION - 11
        $pattern .= '/'; // End

        // List services
        $shell_output = shell_exec('systemctl list-units --type=service');
        $shell_output = explode("\n", $shell_output);
        unset($shell_output[0], $shell_output[count($shell_output)]); // Unset table header and last (empty) line
        $matches = null;
        foreach ($shell_output as $pid => $value) {
            preg_match($pattern, $value, $matches[]);
        }

        // Creating array with values
        $ss = [];
        foreach ($matches ?? [] as $value) {
            if (empty($value) || !Str::contains($value[0], '.service')) {
                continue;
            }
            $serviceName = explode(".", $value[1])[0];
            $ss[$serviceName] = [
                'UNIT' => $value[1],
                'LOAD' => $value[2],
                'ACTIVE' => $value[3],
                'SUB' => $value[4],
                'DESCRIPTION' => $value[5],
            ];
        }

        return $ss;
    }

    /**
     * Returns whether the service is running.
     *
     * @param string $serviceName
     * @return bool
     */
    public static function isServiceUp(string $serviceName)
    {
        $output = shell_exec("systemctl is-active $serviceName");
        return Str::replaceNewlines($output, '') === 'active';
    }

    /**
     * Restarts the service.
     *
     * @param string $serviceName
     * @param array|null $errors
     * @param bool $retry
     * @param int $sleepBeforeRetry
     * @return bool
     */
    public static function restartService(string $serviceName, ?array &$errors = null, bool $retry = true, int $sleepBeforeRetry = 2)
    {
        $action = 'restart';
        if (!self::isServiceUp($serviceName)) {
            $action = 'start';
        }

        shell_exec("systemctl $action $serviceName");
        if (self::isServiceUp($serviceName)) {
            $errors = null;
            return true;
        }

        if ($retry) {
            sleep($sleepBeforeRetry);
            return self::restartService($serviceName, $errors, false, 0);
        }

        $errors = self::getServiceErrors($serviceName, 3);
        return false;
    }

    /**
     * Returns the errors of the service.
     *
     * @param string $serviceName
     * @param int $maxAgeInSeconds
     * @return array
     */
    public static function getServiceErrors(string $serviceName, int $maxAgeInSeconds = 60)
    {
        exec("systemctl status $serviceName", $output);
        $lines = Arr::filterEmpty($output);
        $errors = [];
        foreach ($lines as $line) {
            // If the line does not start with a date, skip it.
            if (!preg_match('/^(?<date>\w{3} \d{2} \d{2}:\d{2}:\d{2}) (?<host>.*?) (?<service>.*?): (?<message>.*)$/', $line, $matches)) {
                continue;
            }
            // If the date is not within the last N seconds, skip it.
            $date = Time::create($matches['date']);
            if ($date->lt(Time::now()->subSeconds($maxAgeInSeconds))) {
                continue;
            }
            // Get errors from the service itself, bypassing systemd errors.
            if (Str::contains($matches['service'], 'systemd[')) {
                continue;
            }
            // Delete any date in the message.
            $matches['message'] = preg_replace('/^\[.*?]/', '', $matches['message']);
            // Add the line to the array.
            $errors[] = $matches['message'];
        }
        return $errors;
    }
}
