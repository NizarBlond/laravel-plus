<?php

namespace NizarBlond\LaravelPlus\Support;

use Carbon\Carbon;
use Exception;

class Time extends Base
{
    /**
     * Time constants.
     */
    const WEEK_IN_SECONDS = 604800;
    const DAY_IN_SECONDS = 86400;
    const HOUR_IN_SECONDS = 3600;
    const MINUTE_IN_SECONDS = 60;
    const WEEK_IN_MINUTES = 10080;
    const DAY_IN_MINUTES = 1440;
    const HOUR_IN_MINUTES = 60;

    /**
     * Compares two given time variables.
     *
     * @param   mixed   $a
     * @param   mixed   $b
     * @return  bool
     */
    public static function equals($a, $b)
    {
        if ($a instanceof Carbon || $b instanceof Carbon) {
            try {
                $_a = Carbon::parse($a);
                $_b = Carbon::parse($b);
                return $_a->eq($_b);
            } catch (Exception $e) {
                return false; // invalid date
            }
        }
        return $a == $b;
    }

    /**
     * Create a Carbon object by adding the specified time in words.
     *
     * @param   string   $add
     * @return  Carbon
     */
    public static function createFutureDate($add)
    {
        if ($add instanceof Carbon) {
            return $add;
        } elseif (is_int($add)) {
            $add .= 's';
        }
        $seconds = self::convertTextToSeconds($add);
        return Carbon::now()->addSeconds($seconds);
    }

    /**
     * Create a Carbon object by subtracting the specified time in words.
     *
     * @param   string   $subtract
     * @return  Carbon
     */
    public static function createPastDate($subtract)
    {
        if ($subtract instanceof Carbon) {
            return $subtract;
        } elseif (is_int($subtract)) {
            $subtract .= 's';
        }
        $seconds = self::convertTextToSeconds($subtract);
        return Carbon::now()->subSeconds($seconds);
    }

    /**
     * Returns a Carbon object with time set to now.
     *
     * @return  Carbon
     */
    public static function now()
    {
        return Carbon::now();
    }

    /**
     * Returns a Carbon object with time set to today.
     *
     * @return  Carbon
     */
    public static function today()
    {
        return Carbon::today();
    }

    /**
     * Returns a Carbon object with time set to now by default.
     *
     * @param   mixed   $value
     * @return  Carbon
     */
    public static function create($value = null)
    {
        if (!$value) {
            return Carbon::now();
        } elseif (is_int($value) || preg_match('/^\d+$/', $value)) {
            return Carbon::createFromTimestamp($value);
        }
        return Carbon::parse($value);
    }

    /**
     * Creates a Carbon object in the specified timezone.
     * 
     * @param   string  $timezone
     * @param   mixed   $value
     * @param   string  $format
     * @return  Carbon
     */
    public static function createInTimezone(string $timezone, $value = null, string $format = 'Y-m-d H:i:s')
    {
        return Carbon::createFromFormat($format, $value, $timezone);
    }

    /**
     * Creates a Carbon object from the specified format.
     * 
     * @param   string  $format
     * @param   mixed   $value
     * @return  Carbon
     */
    public static function createFromFormat(string $format, $value)
    {
        return Carbon::createFromFormat($format, $value);
    }

    /**
     * Converts text time input to seconds.
     *
     * @param   string $text
     * @return  int
     */
    public static function convertTextToSeconds(string $text)
    {
        $intval = intval($text);
        if ($intval < 0) {
            self::exception('The text value must positive.');
        }

        $now = Carbon::now();
        $timestamp = $now->timestamp;

        if (Str::endsWith($text, 'y')) { // years
            return $now->clone()->addYears($intval)->timestamp - $timestamp;
        } elseif (Str::endsWith($text, 'M')) { // months
            return $now->clone()->addMonths($intval)->timestamp - $timestamp;
        } elseif (Str::endsWith($text, 'w')) { // weeks
            return $now->clone()->addWeeks($intval)->timestamp - $timestamp;
        } elseif (Str::endsWith($text, 'd')) { // days
            return $now->clone()->addDays($intval)->timestamp - $timestamp;
        } elseif (Str::endsWith($text, 'h')) { // hours
            return $now->clone()->addHours($intval)->timestamp - $timestamp;
        } elseif (Str::endsWith($text, 'm')) { // minutes
            return $now->clone()->addMinutes($intval)->timestamp - $timestamp;
        } elseif (Str::endsWith($text, 's') || preg_match('/^\d+$/', $text)) { // seconds
            return $intval;
        }

        self::exception('Failed to convert text to seconds.');
    }

    /**
     * Converts values from one to another time unit.
     *
     * @param int $value
     * @param string $from
     * @param string $to
     * @param int $decimalPlaces
     * @return  float
     */
    public static function convertUnit($value, $from, $to, $decimalPlaces = 2)
    {
        $units = [
            's' => 1,
            'm' => 60,
            'h' => 3600,
            'd' => 86400,
            'w' => 604800,
            'M' => 2628000,
            'y' => 31536000,
        ];

        if (!isset($units[$from]) || !isset($units[$to])) {
            self::exception('Invalid time unit.');
        }

        $seconds = $value * $units[$from];
        $result = $seconds / $units[$to];
        return round($result, $decimalPlaces);
    }
}
