<?php

namespace NizarBlond\LaravelPlus\Support;

use Illuminate\Support\Str as IlluminateStr;
use NizarBlond\LaravelPlus\Config\LaravelPlus as LPConfig;
use NizarBlond\LaravelPlus\Traits\ClassHelpers;
use Riimu\Kit\PathJoin\Path;
use DOMDocument;
use Exception;

class Str extends IlluminateStr
{
    use ClassHelpers;
    
    /**
     * Determine if a given string contains a given substring.
     *
     * @param   string          $haystack
     * @param   string|array    $needles
     * @param   bool            $checkCase
     *
     * @return  bool
     */
    public static function contains($haystack, $needles, $checkCase = true)
    {
        $haystack = $haystack ?? '';
        return  ($checkCase
                    ? strpos($haystack, $needles)
                    : stripos($haystack, $needles))
                !== false;
    }

    /**
     * Determine if a given string contains any of the given substrings.
     *
     * @param  string   $haystack
     * @param  array    $needles
     * @param  string   $found
     * @return bool
     */
    public static function containsAny(string $haystack, array $needlesArr, &$found = null)
    {
        $found = null;
        foreach ($needlesArr as $needles) {
            if (self::contains($haystack, $needles)) {
                $found = $needles;
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether the specified string is a valid regular expression.
     *
     * @param string $pattern
     * @return bool
     */
    public static function isValidRegex(string $pattern)
    {
        // An empty string is always a match if the regex is valid
        return @preg_match($pattern, '') !== false;
    }

    /**
     * Checks whether one of the patterns matches the value.
     *
     * @param array|string $patterns
     * @param string $value
     * @param array $patternMatch
     * @return bool
     */
    public static function pregMatch($patterns, string $value, &$patternMatch = null)
    {
        $patterns = is_string($patterns) ? [ $patterns ] : $patterns;

        foreach ($patterns as $pattern) {
            // Add default delimiter and regex options if missing
            if (!in_array(mb_substr($pattern, 0, 1), [ '#','@','/','~' ])) {
                $pattern = '#' . $pattern . '#i';
            }
            // Test regex against value
            if (preg_match($pattern, $value)) {
                $patternMatch = $pattern;
                return true;
            }
        }
        return false;
    }

    /**
     * Generates a random string.
     *
     * @param   int     $length
     * @param   string  $prefix
     *
     * @return  string
     */
    public static function randomString($length = 16, $prefix = "")
    {
        return $prefix . self::random($length);
    }

    /**
     * Generates a alpha string (letters-only).
     *
     * @param   int     $length
     *
     * @return  string
     */
    public static function randomAlpha($length = 8)
    {
        $chars = "abcdefghijklmnopqrstuvwxyz";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    /**
     * Generates a random password.
     *
     * Reference:
     * https://hugh.blog/2012/04/23/simple-way-to-generate-a-random-password-in-php/
     *
     * @param   int     $length
     *
     * @return  string
     */
    public static function randomPassword($length = 32)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    /**
     * Joins the provided file systems paths together and normalizes the result.
     *
     * @return string
     */
    public static function joinPaths()
    {
        try {
            return Path::join(func_get_args());
        } catch (Exception $e) {
            if (Str::contains($e->getMessage(), 'Invalid path character ":"')) {
                return self::joinUnixPaths(func_get_args());
            }
            throw $e;
        }
    }

    /**
     * Standarizes versions and compares.
     *
     * @param   string      $v1
     * @param   string      $v2
     * @param   boolean     $operator
     *
     * @return  int|bool
     */
    public static function versionCompare($v1, $v2, $operator = null)
    {
        $v1Parts = explode(".", $v1);
        $v2Parts = explode(".", $v2);

        for ($i=0; $i<max(count($v1Parts), count($v2Parts)); $i++) {
            if (!isset($v1Parts[$i])) {
                $v1Parts[$i] = 0;
            }
            if (!isset($v2Parts[$i])) {
                $v2Parts[$i] = 0;
            }
        }

        $normV1 = implode(".", $v1Parts);
        $normV2 = implode(".", $v2Parts);
        if ($normV1 === $normV2) {
            // If both are the same then compare the original strings
            return !empty($operator) ? version_compare($v1, $v2, $operator) : version_compare($v1, $v2);
        }

        // Otherwise, just compare the normalized versions
        return !empty($operator) ? version_compare($normV1, $normV2, $operator) : version_compare($normV1, $normV2);
    }

    /**
     * Returns the last N chars of the given string.
     *
     * @param   string  $value
     * @param   int     $lastNCharsSize
     * @param   boolean $coverFirstChars
     *
     * @return  string
     */
    public static function getLastNChars(
        $value,
        $lastNCharsSize = 4,
        $coverFirstChars = false
    ) {
        $size = strlen($value);
        if ($coverFirstChars) {
            $coverSize = $lastNCharsSize < $size ? $size - $lastNCharsSize : $size;
            for ($i = 0; $i < $coverSize; $i++) {
                $value[$i] = '*';
            }
            return $value;
        }

        return substr($value, $lastNCharsSize * -1);
    }


    /**
     * Generate random token.
     *
     * Reference:
     * http://stackoverflow.com/questions/18910814/best-practice-to-generate-random-token-for-forgot-password
     * https://secure.php.net/random_bytes (comments)
     *
     * @param int $length The token length. Optional.
     *
     * @return string
     */
    public static function randomToken($length = 64)
    {
        if (intval($length) <= 8) {
            $length = 78;
        }

        if (function_exists('random_bytes')) {
            return bin2hex(random_bytes($length));
        }

        if (function_exists('mcrypt_create_iv')) {
            return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
        }

        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }

        return bin2hex(random_bytes($length));
    }

    /**
     * Generate random salt.
     *
     * Reference:
     * https://secure.php.net/random_bytes (comments)
     *
     * @param int $length The salt length. Optional.
     *
     * @return string
     */
    public static function randomSalt($length = 78)
    {
        return substr(strtr(base64_encode(hex2bin(self::randomToken(32))), '+', '.'), 0, 44);
    }

    /**
     * Check if the specified string is serialized via serialize() function.
     *
     * @param   string  $str
     *
     * @return  boolean
     */
    public static function isSerialized($str)
    {
        $unserialize = @unserialize($str);
        return $unserialize !== false && $unserialize !== null;
    }

    /**
     * If a string provided then it checks whether it is a valid json.
     * If an array or object array given, it checks whether they are
     * convertible to JSON.
     *
     * @param   mixed  $value
     *
     * @return  boolean
     */
    public static function isJsonable($value)
    {
        if (is_string($value)) {
            return self::isJson($value);
        }

        json_encode($value);
        return JSON_ERROR_NONE === json_last_error();
    }

    /**
     * Check if the given string is valid json
     *
     * @param   string  $string
     *
     * @return  boolean
     */
    public static function isJson($string)
    {
        if (!is_string($string)) {
            return false;
        }

        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }

    /**
     * Encodes a value into a JSON string.
     *
     * @param   mixed   $value
     * @param   boolean $throwException
     *
     * @return  string
     *
     * @throws  \Exception
     */
    public static function jsonEncode($value, $throwException = false)
    {
        $result = json_encode($value);

        if (JSON_ERROR_NONE === json_last_error()) {
            return $result;
        }

        return $throwException
            ? self::exception("JSON encode failed: " . json_last_error_msg())
            : $result;
    }
    
    /**
     * Encodes a value into a JSON string with pretty print.
     *
     * @param   mixed   $value
     * @param   boolean $unescape
     *
     * @return  string
     */
    public static function jsonPrettify($value, bool $unescape = true)
    {
        $flags = JSON_PRETTY_PRINT;
        if ($unescape) {
            $flags |= JSON_UNESCAPED_UNICODE;
            $flags |= JSON_UNESCAPED_SLASHES;
        }
        return json_encode($value, $flags);
    }

    /**
     * Decodes a JSON string.
     *
     * @param   string  $str
     * @param   boolean $assoc
     * @param   boolean $throwException
     *
     * @return  mixed
     *
     * @throws  \Exception
     */
    public static function jsonDecode($str, $assoc = true, $throwException = false)
    {
        if (is_null($str)) {
            return null;
        }

        $json = json_decode($str, $assoc, JSON_UNESCAPED_UNICODE);

        if (JSON_ERROR_NONE === json_last_error()) {
            return $json;
        }

        return $throwException
            ? self::exception("JSON decode failed: " . json_last_error_msg())
            : $json;
    }

    /**
     * Decodes a JSON string while retrying with UTF8 conversion if needed.
     * 
     * @param   string  $str
     * @param   boolean $assoc
     * @param   boolean $throwException
     */
    public static function jsonDecodeUtf8($str, $assoc = true, $throwException = false)
    {
        $json = self::jsonDecode($str, $assoc, false);
        if (JSON_ERROR_UTF8 === json_last_error()) {
            $json = self::jsonDecode(self::convertToUtf8($str), $assoc, $throwException);
        }
        return $json;
    }

    /**
     * Return GUID, This needed in Unix platform there is no com_create_guid function
     * this function need .net framework.
     *
     * @return  string
     */
    public static function guid()
    {
        if (function_exists('com_create_guid')) {
            return trim(com_create_guid(), '{}');
        }
        return sprintf(
            '%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(16384, 20479),
            mt_rand(32768, 49151),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535)
        );
    }

    /**
     * Replaces new lines.
     *
     * @param   string  $text
     * @param   string  $replaceWith
     * @param   bool    $trim
     * @param   bool    $removeWhitespaces
     *
     * @return  string
     */
    public static function replaceNewlines($text, $replaceWith = ' ', $trim = true, $removeWhitespaces = true)
    {
        $text = preg_replace('~[\r\n]+~', $replaceWith, $text);

        $text = $trim ? trim($text) : $text;

        $text = $removeWhitespaces ? Str::removeWhitespaces($text) : $text;

        return $text;
    }

    /**
     * Removes empty lines from the given text.
     *
     * @param   string  $text
     * @param   bool    $trim
     *
     * @return  string
     */
    public static function removeEmptyLines($text, $trim = false)
    {
        $lines = explode(PHP_EOL, $text);
        $result = [];
        foreach ($lines as $line) {
            if (!empty($line)) {
                $result[] = $trim ? trim($line) : $line;
            }
        }
        return implode(PHP_EOL, $result);
    }

    /**
     * Check if the given string is valid html
     *
     * @param   string  $string
     *
     * @return  boolean
     */
    public static function isHtml($string, &$error = null)
    {
        $error  = null;
        $isHtml = false;
        try {
            $doc = new DOMDocument();
            $isHtml = $doc->loadHTML($string);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $isHtml = false;
        }
        return $isHtml;
    }

    /**
     * Creates a slug from given text.
     *
     * @param   string  $text
     * @param   string  $replaceWith
     *
     * @return  string
     */
    public static function slugify($text, $replaceWith = "")
    {
        // replace non letter or digits
        $text = preg_replace('~[^-\pL\d]+~u', $replaceWith, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
    
        // remove duplicates
        $text = preg_replace('~-+~', '-', $text);
        
        // trim
        $text = trim($text, $replaceWith);

        // lowercase
        $text = strtolower($text);
    
        if (empty($text)) {
            return null;
        }
    
        return $text;
    }

    /**
     * Removes any non-UTF 8 characters.
     *
     * Useful for errors raised by json_encode:
     * Malformed UTF-8 characters, possibly incorrectly encoded.
     *
     * @param array|string $data
     *
     * @return array|string
     */
    public static function convertToUtf8($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = self::convertToUtf8($value);
            }
        } elseif (is_string($data)) {
            return mb_convert_encoding($data, 'UTF-8', 'UTF-8');
        }
        return $data;
    }

    /**
     * Sorts array by string length.
     *
     * @param   array       $arr
     * @param   boolean     $byShortest
     *
     * @return  array
     */
    public static function sortByLength($arr, $byShortest = true)
    {
        $result = usort($arr, function ($s1, $s2) use ($byShortest) {
            if ($byShortest) {
                return strlen($s1) <=> strlen($s2);
            } else {
                return strlen($s2) <=> strlen($s1);
            }
        });

        if (!$result) {
            self::exception("Sort array failed.");
        }

        return $arr;
    }

    /**
     * Guesses variable type from string.
     *
     * @param   string  $string
     *
     * @return  string
     */
    public static function guessTypeFromString($string)
    {
        //  (c) José Moreira - Microdual (www.microdual.com)
        return gettype(self::getCorrectVariable($string));
    }

    /**
     * Returns the name initials.
     *
     * @param   string  $string
     * @param   bool    $toUpper
     *
     * @return  string
     */
    public static function getNameInitials($string, $allUppers = true, $toUpper = true)
    {
        $words = explode(" ", $string);
        $initials = '';
        foreach ($words ?? [] as $word) {
            for ($i=0; $i < strlen($word); $i++) {
                if ($i === 0 || ($allUppers && ctype_upper($word[$i]))) {
                    $initials .= $word[$i];
                }
            }
        }
        return $toUpper ? strtoupper($initials) : $initials;
    }

    /**
     * Guesses variable type from string and returns it in the
     * guessed type.
     *
     * @param   string  $string
     *
     * @return  string
     */
    public static function getCorrectVariable($string)
    {
        //  (c) José Moreira - Microdual (www.microdual.com)
        //      With the help of Svisstack (http://stackoverflow.com/users/283564/svisstack)

        /* FUNCTION FLOW */
        // *1. Remove unused spaces
        // *2. Check if it is empty, if yes, return blank string
        // *3. Check if it is numeric
        // *4. If numeric, this may be a integer or double, must compare this values.
        // *5. If string, try parse to bool.
        // *6. If not, this is string.

        $string=trim($string);
        if (empty($string)) {
            return "";
        }
        if (!preg_match("/[^0-9.]+/", $string)) {
            if (preg_match("/[.]+/", $string)) {
                return (double)$string;
            } else {
                return (int)$string;
            }
        }
        if ($string=="true") {
            return true;
        }
        if ($string=="false") {
            return false;
        }
        return (string)$string;
    }

    /**
     * Removes whitespaces from the specified string.
     *
     * @param   string      $string
     * @param   boolean     $trim
     *
     * @return  string
     */
    public static function removeWhitespaces($string, $trim = false)
    {
        $result = preg_replace('/\s+/', ' ', $string);
        return $trim ? trim($result) : $result;
    }

    /**
     * Converts values from one to another digital unit.
     *
     * @param   integer $value
     * @param   string  $to
     * @param   string  $from
     * @param   integer $decimalPlaces
     * @param   integer $bytesToKb
     *
     * @return  integer
     *
     */
    public static function convertDigitalUnit($value, $from, $to, $decimalPlaces = 2, $bytesToKb = null)
    {
        $bytesToKb = $bytesToKb ?? LPConfig::bytesToKilobyte();
        $separator = '';
        $decimalPoint = '.';
        $invalid = null;

        $from = strtoupper($from);
        $to = strtoupper($to);

        if ($from === $to) {
            return $value;
        }

        if ($from === 'B') {
            $formulas = [
                'KB' => number_format($value / pow($bytesToKb, 1), $decimalPlaces, $decimalPoint, $separator),
                'MB' => number_format($value / pow($bytesToKb, 2), $decimalPlaces, $decimalPoint, $separator),
                'GB' => number_format($value / pow($bytesToKb, 3), $decimalPlaces, $decimalPoint, $separator)
            ];
            return $formulas[$to] ?? $invalid;
        }

        if ($from === 'KB') {
            $formulas = [
                'B' => number_format($value * pow($bytesToKb, 1), $decimalPlaces, $decimalPoint, $separator),
                'MB' => number_format($value / pow($bytesToKb, 1), $decimalPlaces, $decimalPoint, $separator),
                'GB' => number_format($value / pow($bytesToKb, 2), $decimalPlaces, $decimalPoint, $separator)
            ];
            return $formulas[$to] ?? $invalid;
        }

        if ($from === 'MB') {
            $formulas = [
                'B' => number_format($value * pow($bytesToKb, 2), $decimalPlaces, $decimalPoint, $separator),
                'KB' => number_format($value * pow($bytesToKb, 1), $decimalPlaces, $decimalPoint, $separator),
                'GB' => number_format($value / pow($bytesToKb, 1), $decimalPlaces, $decimalPoint, $separator)
            ];
            return $formulas[$to] ?? $invalid;
        }

        if ($from === 'GB') {
            $formulas = [
                'B' => number_format($value * pow($bytesToKb, 3), $decimalPlaces, $decimalPoint, $separator),
                'KB' => number_format($value * pow($bytesToKb, 2), $decimalPlaces, $decimalPoint, $separator),
                'MB' => number_format($value * pow($bytesToKb, 1), $decimalPlaces, $decimalPoint, $separator)
            ];
            return $formulas[$to] ?? $invalid;
        }

        return $invalid;
    }

    /**
     * Converts bytes to a readable string.
     *
     * Reference:
     * http://php.net/manual/en/function.disk-free-space.php
     *
     * @param   integer     $bytes
     *
     * @return  string
     */
    public static function bytesToReadable($bytes)
    {
        $si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
        $base = 1024;
        $class = min((int)log($bytes, $base), count($si_prefix) - 1);
        return sprintf('%1.2f', $bytes / pow($base, $class)) . ' ' . $si_prefix[$class];
    }

    /**
     * Converts seconds to a readable string.
     *
     * @param   integer     $bytes
     *
     * @return  string
     */
    public static function secondsToReadable($seconds)
    {
        $units = array_filter([
            's' => $seconds % 60,
            'm' => floor(($seconds % 3600) / 60),
            'h' => floor(($seconds % 86400) / 3600),
            'd' => floor(($seconds % 2592000) /86400),
            'M' => floor($seconds / 2592000),
        ]);
        $format = '';
        foreach (array_reverse($units) as $unit => $value) {
            $format .= sprintf('%s%s ', $value, $unit);
        }
        return empty($format) ? '0s' : rtrim($format);
    }

    /**
     * Compares the content line by line and returns all lines in $content1 that
     * are different or do not exist in $content2.
     *
     * @param string $fil1
     * @param string $file2
     * @param array $replaceRegex
     *
     * @return string
     */
    public static function getDiffs(string $content1, string $content2, array $replaceRegex = [])
    {
        foreach ($replaceRegex as $pattern) {
            $content1 = preg_replace($pattern, '', $content1);
            $content2 = preg_replace($pattern, '', $content2);
        }

        $diffs = [];
        $lines1 = explode(PHP_EOL, $content1);
        $lines2 = explode(PHP_EOL, $content2);
        foreach ($lines1 as $i => $line1) {
            $line2 = $lines2[$i] ?? null;
            if (is_null($line2) || $line1 !== $line2) {
                $diffs[] = $line1;
            }
        }

        return $diffs;
    }

    /**
     * An internal implementation of Path::join that is simpler and supports ":".
     *
     * @param array $paths
     * @return string
     */
    private static function joinUnixPaths(array $paths)
    {
        $args = array_map('strval', is_array($paths) ? $paths : func_get_args());

        $paths = [];
        foreach ($args as $arg) {
            $paths = array_merge($paths, (array) $arg);
        }

        foreach ($paths as &$path) {
            $path = trim($path, DIRECTORY_SEPARATOR);
        }

        if (Str::startsWith($args[0], DIRECTORY_SEPARATOR)) {
            $paths[0] = DIRECTORY_SEPARATOR . $paths[0];
        }

        return join(DIRECTORY_SEPARATOR, $paths);
    }
}
