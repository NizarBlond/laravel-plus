<?php

namespace NizarBlond\LaravelPlus\Support;

use Exception;

class Git extends Base
{
    /**
     * Creates key files to be used for communication with the Git server.
     *
     * @param string $private
     * @return array
     */
    public static function createKeyFiles(string $private)
    {
        $files = [
            'private' => File::createTemp($private),
        ];
        foreach ($files as $file) {
            chmod($file, 0400);
        }
        return $files;
    }

    /**
     * Returns the Git service from the URL.
     *
     * @param string $url
     * @return string
     */
    public static function getServiceFromUrl(string $url)
    {
        if (Str::contains($url, 'https://bitbucket.org')) {
            return 'bitbucket';
        } elseif (Str::contains($url, 'https://github.com')) {
            return 'github';
        }
        return self::exception('Invalid or not supported Git service.');
    }

    /**
     * Pulls a repository.
     *
     * @param   string  $gitDir
     * @param   string  $branch
     * @param   array   $keysDetails
     * @param   string  $service
     * @param   bool    $checkout
     * @return  string
     */
    public static function pull(
        string $gitDir,
        string $branch,
        array $keysDetails,
        string $service,
        bool $checkout = false
    ) {
        $output = '';

        if ($checkout) {
            $command = sprintf("git -C '%s' checkout %s", $gitDir, $branch);
            $output .= self::runCommand($keysDetails, $command, $service);
        }

        $command = sprintf("git -C '%s' pull", $gitDir);
        $output .= self::runCommand($keysDetails, $command, $service);

        return $output;
    }

    /**
     * Clones a repository.
     *
     * Note that for some reason HTTPS url does not work for clone.
     *
     * @param   string  $url
     * @param   string  $branch
     * @param   array   $keysDetails
     * @param   string  $gitDir
     * @param   array   $args
     * @return  string
     */
    public static function clone(
        string $url,
        string $branch,
        array $keysDetails,
        string $gitDir,
        array $args = []
    ) {
        $output = '';
        $repositoryDetails = self::parseRepositoryUrl($url);
        $sshUrl = $repositoryDetails['ssh_path'];

        if (empty($gitDir)) {
            self::exception('Invalid Git directory.');
        }

        if (Dir::exists($gitDir)) {
            self::logWarning('The Git working directory already exists: ' . $gitDir);
        }

        $args['--single-branch'] = null;
        $args['--branch'] = $branch;

        $config = '';
        foreach ($args as $key => $value) {
            $config .= (is_null($value) ? $key : sprintf('%s %s', $key, $value)) . ' ';
        }

        $command = sprintf("git clone %s %s %s", $config, $sshUrl, $gitDir);

        $service = self::getServiceFromUrl($url);
        $output .= self::runCommand($keysDetails, $command, $service);

        return $output;
    }

    /**
     * Checks if the branch is cloneable without actually cloning it.
     * 
     * @param   string  $url
     * @param   string  $branch
     * @param   array   $keysDetails
     * @param   string  $error
     * @return  bool
     */
    public static function isCloneable(string $url, string $branch, array $keysDetails, ?string &$error = null)
    {
        $error = null;
        $repositoryDetails = self::parseRepositoryUrl($url);
        $sshUrl = $repositoryDetails['ssh_path'];

        $args = [
            '--single-branch' => null,
            '--branch' => $branch,
            '--depth' => 1,
        ];

        $config = '';
        foreach ($args as $key => $value) {
            $config .= (is_null($value) ? $key : sprintf('%s %s', $key, $value)) . ' ';
        }

        $service = self::getServiceFromUrl($url);
        $command = sprintf("git ls-remote --heads %s %s", $sshUrl, $branch);

        try {
            $output = self::runCommand($keysDetails, $command, $service);
        } catch (Exception $e) {
            $error = $e->getMessage();
            return false;
        }

        return Str::contains($output, $branch);
    }

    /**
     * Parses the repository URL.
     *
     * @param  string  $url
     * @return bool
     */
    public static function parseRepositoryUrl(string $url)
    {
        $service = self::getServiceFromUrl($url);

        switch ($service) {
            case 'bitbucket':
                $pattern = '#https://bitbucket\.org/(?P<account>[^/]+)/(?P<name>[^/]+)/?#';
                $sshFormat = 'git@bitbucket.org:%s/%s.git';
                break;
            case 'github':
                $pattern = '#https://github\.com/(?P<account>[^/]+)/(?P<name>[^/]+)/?#';
                $sshFormat = 'git@github.com:%s/%s.git';
                break;
            default:
                self::exception('Git service provider not supported.');
        }

        if (!preg_match($pattern, $url, $matches)) {
            self::exception('Invalid Git repository URL.');
        }

        $account = $matches['account'] ?? self::exception('Failed to read account.');
        $name = $matches['name'] ?? self::exception('Failed to read repository name.');

        return [
            'account' => $account,
            'name' => $name,
            'ssh_path' => sprintf($sshFormat, $account, $name),
        ];
    }

    /**
     * Ensures an RSA key finger print is added to the known_hosts of the linux user.
     *
     * References:
     * https://support.atlassian.com/bitbucket-cloud/docs/configure-ssh-and-two-step-verification/
     *
     * @param   string  $sshPublicKey
     * @param   string  $service
     * @return  array
     */
    private static function addRSAKeyFingerPrint(string $service)
    {
        // Get the SSH folder
        $user = Proc::currentUser();
        $sshDir = sprintf(
            '/%s/.ssh',
            $user === 'root' ? 'root' : '/home/' . $user
        );
        if (!Dir::exists($sshDir)) {
            self::exception('SSH folder not found. Please verify the user is not a nologin user.');
        }

        // Add finger print "ssh-keyscan -t rsa bitbucket.org"
        switch ($service) {
            case 'bitbucket':
                $host = 'bitbucket.org';
                break;
            case 'github':
                $host = 'github.com';
                break;
            default:
                self::exception('Git service provider not supported.');
        }

        // Check whether ~/.ssh/known_hosts file has service signature
        $knownhostsFile = Str::joinPaths($sshDir, 'known_hosts');
        if (!File::exists($knownhostsFile)) {
            File::touch($knownhostsFile);
        } else {
            $knownhosts = File::read($knownhostsFile);
            if (Str::contains($knownhosts, $host)) {
                return true;
            }
        }

        // Otherwise, add service signature
        Proc::runCommand(sprintf('/bin/ssh-keyscan -t rsa %s >> %s', $host, $knownhostsFile));
    }

    /**
     * Runs a git command.
     *
     * References:
     * https://stackoverflow.com/questions/4565700/how-to-specify-the-private-ssh-key-to-use-when-executing-shell-command-on-git
     * https://ralphjsmit.com/git-custom-ssh-key
     *
     * @param   array   $keysDetails
     * @param   string  $command
     * @param   string  $service
     * @return  string
     */
    private static function runCommand(array $keysDetails, string $command, string $service)
    {
        self::addRSAKeyFingerPrint($service);

        $sshPrivateKey = $keysDetails['private'] ?? null;
        if (!File::exists($sshPrivateKey)) {
            self::exception('Invalid SSH private key path.');
        }

        $flags = implode(' ', [
            '-o StrictHostKeyChecking=no', // Disable host key checking
            '-o UserKnownHostsFile=/dev/null', // Disable known hosts
            "-i $sshPrivateKey" // Use the private key
        ]);
        $command = "GIT_SSH_COMMAND='/bin/ssh $flags' $command";

        try {
            $output = Proc::runCommand($command);
        } catch (Exception $e) {
            $error = $e->getMessage();
            self::logError(sprintf('Git command failed: command="%s", error="%s"', $command, $error));

            if (Str::contains($error, 'Permission denied (publickey)')) {
                self::exception('Permission denied. Are you sure you added the SSH key?');
            } elseif (Str::contains($error, 'Host key verification failed')) {
                self::exception('Failed to verify SSH key. Are you sure you added the SSH key?');
            } elseif (Str::contains($error, 'fatal: Could not read from remote repository.')) {
                self::exception('Could not read from remote repository.');
            } elseif (Str::contains($error, 'Could not find remote branch')) {
                self::exception('Could not find remote branch.');
            } elseif (Str::contains($error, '--add safe.directory')) { 
                self::exception('Detected detected dubious ownership of the .git directory.');
            } elseif (Str::contains($error, 'fatal: not a git repository')) {
                self::exception('Could not find a Git repository in the specified directory.');
            } else {
                self::exception('Failed to connect to repository.');
            }
        }

        return $output;
    }
}
