<?php

namespace NizarBlond\LaravelPlus\Support;

class Math extends Base
{
    /**
     * Returns the specified number in float formatted string.
     *
     * @param  mixed    $number
     * @param  int      $decimalPlaces
     *
     * @return string
     */
    public static function toFloat($number, $decimalPlaces = 2)
    {
        return number_format((float) $number, $decimalPlaces, '.', '');
    }
}
