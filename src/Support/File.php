<?php

namespace NizarBlond\LaravelPlus\Support;

use NizarBlond\LaravelPlus\Config\File as FileConfig;
use Exception;

class File extends Base
{
    /**
     * Size constants.
     */
    const KB_IN_BYTES = 1024;
    const MB_IN_BYTES = 1048576;
    const GB_IN_BYTES = 1073741824;

    /**
     * Sets the file owner.
     * 
     * @param   string      $file
     * @param   string|int  $user
     * @param   string|int  $group
     * @return  boolean
     */
    public static function chown(string $file, $user, $group = null)
    {
        if (!self::exists($file)) {
            return false;
        }

        $result = chown($file, $user);
        if (! $result) {
            self::exception("Failed to change owner of '$file' to '$user'.");
        }

        if ($group) {
            $result = chgrp($file, $group);
            if (! $result) {
                self::exception("Failed to change group of '$file' to '$group'.");
            }
        }

        return $result;
    }

    /**
     * Returns the file owner username.
     *
     * @param   string  $file
     *
     * @return  string
     */
    public static function getOwnerUser($file)
    {
        if (function_exists('posix_getpwuid')) {
            $info = posix_getpwuid(fileowner($file));
            return !empty($info['name']) ? $info['name'] : null;
        }
        
        self::exception("You must install php-posix in order to use this function.");
    }

    /**
     * Returns the file owner username.
     *
     * @param   string  $file
     *
     * @return  string
     */
    public static function getOwnerGroup($file)
    {
        if (function_exists('posix_getgrgid')) {
            $info = posix_getgrgid(filegroup($file));
            return !empty($info['name']) ? $info['name'] : null;
        }
        
        self::exception("You must install php-posix in order to use this function.");
    }

    /**
     * Returns the file permissions.
     *
     * @param   string  $file
     *
     * @return  string
     */
    public static function getPermissions($file)
    {
        return decoct(fileperms($file) & 0777);
    }

    /**
     * Tells whether the filename is a regular file.
     *
     * @param   string  $file
     *
     * @return  boolean
     */
    public static function isFile($file)
    {
        return is_file($file);
    }

    /**
     * Tells whether the filename is a symbolic link.
     *
     * @param   string  $file
     *
     * @return  boolean
     */
    public static function isLink($file)
    {
        return is_link($file);
    }

    /**
     * Check whether the specified file exists.
     *
     * @param   string  $file
     * @param   boolean $fromCache
     *
     * @return  boolean
     */
    public static function exists($file, $fromCache = true)
    {
        if (!$fromCache) {
            clearstatcache(true, $file);
        }
        return file_exists($file);
    }

    /**
     * Check whether the specified file is missing.
     *
     * @param   string  $file
     * @param   boolean $fromCache
     *
     * @return  boolean
     */
    public static function missing($file, $fromCache = true)
    {
        return !self::exists($file, $fromCache);
    }

    /**
     * Check whether the provided files are the same copy.
     *
     * @param   string  $fn1
     * @param   string  $fn2
     * @param   bool    $cmpChunks
     *
     * @return  boolean
     */
    public static function isCopy($fn1, $fn2, $cmpChunks = false)
    {
        try {
            return  filemtime($fn1) === filemtime($fn2) &&
                    self::isIdentical($fn1, $fn2, $cmpChunks);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Check whether the provided files are identical.
     *
     * @param   string  $fn1
     * @param   string  $fn2
     * @param   bool    $cmpChunks
     *
     * @return  boolean
     */
    public static function isIdentical($fn1, $fn2, $cmpChunks = false)
    {
        if (!self::exists($fn1)) {
            self::exception("File does not exist: $fn1.");
        }

        if (!self::exists($fn2)) {
            self::exception("File does not exist: $fn2.");
        }

        if (filetype($fn1) !== filetype($fn2)) {
            return false;
        }

        if (filesize($fn1) !== filesize($fn2)) {
            return false;
        }

        return self::isDataEqual($fn1, $fn2, $cmpChunks);
    }

    /**
     * Compare the file contents.
     *
     * @param   string  $fn1
     * @param   string  $fn2
     * @param   bool    $chunks
     *
     * @return  boolean
     */
    public static function isDataEqual($fn1, $fn2, $chunks = false)
    {
        if ($chunks) {
          $ah = fopen($fn1, 'rb');
          $bh = fopen($fn2, 'rb');
          $result = true;
          while(!feof($ah)) {
            if(fread($ah, 8192) != fread($bh, 8192)) {
              $result = false;
              break;
            }
          }
          fclose($ah);
          fclose($bh);
        } else {
            $result = sha1_file($fn1) === sha1_file($fn2);
        }

        return $result;
    }

    /**
     * Generates filename with date by suffixing date to name.
     *
     * @param   string  $name
     * @param   string  $separator
     * @param   string  $format
     *
     * @return  string
     */
    public static function createNameWithDate($name, $format = 'Y-m-d_h-i-s', $separator = '-')
    {
        $pathParts = pathinfo($name);
        return sprintf(
            '%s%s%s%s',
            $pathParts['filename'],
            $separator,
            date($format),
            empty($pathParts['extension']) ? '' : '.' . $pathParts['extension']
        );
    }

    /**
     * Check whether file contains a string.
     *
     * @param  string  $file
     * @param  string  $string
     * @return void
     */
    public static function contains(string $file, string $string, int $limit = 1000)
    {
        if (!self::exists($file)) {
            return false;
        }
        $offset = 0;
        while (true) {
            $lines = self::slice($file, $offset, $limit);
            foreach ($lines as $line) {
                if (Str::contains($line, $string)) {
                    return true;
                }
            }
            if (count($lines) < $limit) {
                return false;
            }
            $offset += $limit;
        }
        return false;
    }

    /**
     * Read the entire file into a string or array.
     *
     * @param   string  $file
     * @param   boolean $intoArr
     * @param   boolean $failIfNotFound
     *
     * @return  string|array
     */
    public static function read($file, $intoArr = false, $failIfNotFound = false)
    {
        $content = $intoArr ? @file($file) : @file_get_contents($file);
        if (! $content) {
            return $failIfNotFound
                    ? self::exception("Failed to read content from $file.")
                    : null;
        }

        return $content;
    }

    /**
     * Read the entire file into an array.
     *
     * @param   string  $file
     * @param   boolean $failIfNotFound
     *
     * @return  array
     */
    public static function toArray($file, $failIfNotFound = false)
    {
        return self::read($file, true, $failIfNotFound);
    }

    /**
     * Parse file with JSON content.
     *
     * @param   string  $file
     *
     * @return  false|array
     */
    public static function parseJson(string $file)
    {
        $content = self::read($file);
        if ($content) {
            return Str::jsonDecode($content);
        }
        return false;
    }

    /**
     * Checks if remote file URL is valid.
     *
     * Rerefence:
     * https://stackoverflow.com/questions/1363925/check-whether-image-exists-on-remote-url
     *
     * @param   string  $url
     *
     * @return  boolean
     */
    public static function isRemoteFileValid($url, &$error = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $error  = curl_error($ch);
        curl_close($ch);
        if ($result !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Reads the first lines of a file.
     * 
     * @param string $file
     * @param int $lines
     * @return array
     */
    public static function head(string $file, int $lines = 10)
    {
        return self::slice($file, 0, $lines);
    }

    /**
     * Reads the last lines of a file.
     * 
     * @param string $file
     * @param int $lines
     * @return array
     */
    public static function tail(string $file, int $lines = 10)
    {
        return self::slice($file, 0, $lines, true);
    }

    /**
     * Read the entire file into a string.
     *
     * @param   string  $file
     *
     * @return  array
     */
    public static function readAsArray($file)
    {
        return self::read($file, true);
    }

    /**
     * Write a string to a file (if exists; it will be overwritten).
     *
     * @param   string          $file
     * @param   mixed           $data
     * @param   integer         $flags
     *
     * @return  integer|boolean             The number of bytes
     */
    public static function write($file, $data, $flags = 0)
    {
        return file_put_contents($file, $data, $flags);
    }

    /**
     * Appends a line to a file.
     *
     * @param   string  $file
     *
     * @return  integer|boolean             The number of bytes
     */
    public static function append($file, $line)
    {
        return file_put_contents($file, $line, FILE_APPEND | LOCK_EX);
    }

    /**
     * Copies file from source to destination.
     *
     * IMPORTANT:
     * If the destination exists it will be overwritten.
     *
     * @param   string  $src
     * @param   string  $dst
     * @param   boolean $preserveTimestamps
     *
     * @return  boolean
     */
    public static function copy(string $src, string $dst, bool $preserveTimestamps = false)
    {
        if (!Dir::exists(dirname($dst))) {
            Dir::makeIfNotExists(dirname($dst));
        }

        $dt = filemtime($src);

        $result = copy($src, $dst);
        if (! $result) {
            self::exception(sprintf("Failed to copy file from '%s' to '%s'.", $src, $dst));
        }

        if ($preserveTimestamps && $dt) {
            touch($dst, $dt);
        }

        return $result;
    }

    /**
     * Copies file from source to destination while preserving the last modified time.
     *
     * @param   string  $src
     * @param   string  $dst
     *
     * @return  boolean
     */
    public static function copydt(string $src, string $dst)
    {
        return self::copy($src, $dst, true);
    }

    /**
     * Creates a symbolic link.
     *
     * @param   string  $target
     * @param   string  $link
     *
     * @return  boolean
     */
    public static function symlink($target, $link)
    {
        $result = symlink($target, $link);
        if (! $result) {
            self::exception("Failed to create symbolic link from '$target' to '$link'.");
        }

        return $result;
    }

    /**
     * Returns file last modified time.
     *
     * @param   string  $file
     * @param   boolean $fromCache
     *
     * @return  integer|null
     */
    public static function lastModified($file, $fromCache = true)
    {
        if (empty($file) || !self::exists($file, $fromCache)) {
            return null;
        }
        return filemtime($file);
    }

    /**
     * Returns file last modified time in Carbon format.
     *
     * @param   string  $file
     * @param   boolean $fromCache
     *
     * @return  Carbon|null
     */
    public static function mtime($file, $fromCache = true)
    {
        $timestamp = self::lastModified($file, $fromCache);
        return $timestamp ? Time::create($timestamp) : null;
    }
    
    /**
     * Renames (moves) the specified file.
     *
     * @param   string  $old
     * @param   string  $new
     *
     * @return  boolean
     */
    public static function rename($old, $new)
    {
        if (!self::exists($old)) {
            return false;
        }
        $result = @rename($old, $new);
        if (! $result) {
            self::exception("Failed to move '$old' to '$new'.");
        }
        return $result;
    }

    /**
     * Tells whether the specified URL is downloadable.
     *
     * @param   string $url
     * @param   array  $headers
     * @param   mixed  $context
     *
     * @return  boolean
     */
    public static function isDownloadable(string $url, ?array &$responseHeaders = null, $context = null)
    {
        $context = $context ?? self::createBrowserContext();
        $responseHeaders = get_headers($url, false, $context);
        if ($responseHeaders) {
            foreach ($responseHeaders as $header) { // Loop to handle redirections
                if (Str::contains($header, '200 OK')) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Downloads file from URL with a fallback to encoding the URL path.
     *
     * @param   string  $url
     * @param   boolean $checkHeaders
     * @param   array   $responseHeaders
     * @param   array   $context
     *
     * @return  string|false
     */
    public static function download(string $url, bool $checkHeaders = false, ?array &$responseHeaders = null, $context = null)
    {
        // Attempt to download the file
        $content = self::downloadRaw($url, $checkHeaders, $responseHeaders, $context);
        if ($content) {
            return $content;
        }
        // Fallback to encoding the URL path
        $encodedURL = self::encodeUrlPath($url);
        if ($url !== $encodedURL) {
            return self::downloadRaw($encodedURL, $checkHeaders, $responseHeaders, $context);
        }
        return false;
    }

    /**
     * Downloads file from URL.
     *
     * @param   string  $url
     * @param   boolean $checkHeaders
     * @param   array   $responseHeaders
     * @param   array   $context
     *
     * @return  string|false
     */
    public static function downloadRaw(string $url, bool $checkHeaders = false, ?array &$responseHeaders = null, $context = null)
    {
        if ($checkHeaders && !self::isDownloadable($url, $responseHeaders)) {
            return false;
        }
        $context = $context ?? self::createBrowserContext();
        return @file_get_contents($url, false, $context);
    }

    /**
     * Creates a browser context.
     *
     * @return  resource
     */
    public static function createBrowserContext()
    {
        return stream_context_create([
            'http' => [
                'method' => 'GET',
                'header' => "Accept-language: en\r\n" .
                            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3\r\n"
            ]
        ]);
    }

    /**
     * Attempts to set the access and modification times of the file named
     * in the filename parameter to the value given in time. Note that the
     * access time is always modified, regardless of the number of parameters.
     *
     * @param   string  $file
     * @param   int     $mode
     * @param   int     $time
     * @param   int     $atime
     *
     * @return  boolean
     */
    public static function touch(string $file, ?int $mode = null, ?int $time = null, ?int $atime = null)
    {
        $time = $time ?? time();
        $atime = $atime ?? time();
        $result = touch($file, $time, $atime);
        if ($mode) {
            self::chmod($file, $mode);
        }
        return $result;
    }

    /**
     * Attempts to change the mode of the specified file to that given in permissions.
     *
     * @param   string  $file
     * @param   int     $permissions
     *
     * @return  boolean
     */
    public static function chmod(string $file, int $permissions)
    {
        return chmod($file, $permissions);
    }

    /**
     * Encodes only the path part of a URL.
     * 
     * Example:
     * https://example.com/my path?name=John Doe => https://example.com/my%20path?name=John Doe
     *
     * @param   string  $url
     *
     * @return  string
     */
    public static function encodeUrlPath($url)
    {
        $parts = parse_url($url);
        $parts['scheme'] = !empty($parts['scheme']) ? $parts['scheme'] : 'http';
        
        if (! isset($parts['host'])) {
            self::exception("Invalid URL host (url='$url').");
        }

        $pathParts = array_map('rawurldecode', explode('/', $parts['path'] ?? ''));
        $path = $parts['scheme'] . '://' . $parts['host'] . implode('/', array_map('rawurlencode', $pathParts));

        if (!empty($parts['query'])) {
            $path .= '?' . $parts['query'];
        }

        return $path;
    }

    /**
     * Deletes a file or a symbolic link.
     *
     * Note that $trash takes effect only if a valid recycle bin path
     * is available in laravel-config.php. It only applies to regular
     * files and not symlinks.
     *
     * @param   string  $file
     * @param   bool    $trash
     *
     * @return  bool
     */
    public static function delete($file, $trash = true)
    {
        if (is_link($file)) {
            return unlink($file);
        }

        if (!is_file($file)) {
            return false;
        }

        if ($trash) {
            $recycleBin = FileConfig::recycleBinPath();
            if (!empty($recycleBin)) {
                $dst = Str::joinPaths($recycleBin, dirname($file), basename($file) . '_' . time());
                Dir::makeIfNotExists(dirname($dst));
                return File::rename($file, $dst);
            }
        }

        return unlink($file);
    }

    /**
     * Returns the file size in bytes.
     *
     * @param   string  $file
     * @param   string  $unit
     *
     * @return  integer|float
     */
    public static function size(string $file, string $unit = 'B', string $thousandsSeparator = '')
    {
        $size = @filesize($file);
        if ($size === false) {
            return 0;
        }
        $units = array_flip([ 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB' ]);
        $power = $units[$unit];
        if ($power > 1) {
            return number_format($size / pow(self::KB_IN_BYTES, $power), 2, '.', $thousandsSeparator);
        }
        return round($size / pow(self::KB_IN_BYTES, $power));
    }

    /**
     * Extracts a *.gz file.
     * Reference: https://stackoverflow.com/questions/3293121/how-can-i-unzip-a-gz-file-with-php
     *
     * @param   string     $path
     * @param   string     $outputDir
     *
     * @return  string
     */
    public static function extractGzip($path, $outputDir)
    {
        if (! File::exists($path)) {
            self::exception("Failed to read file data: '$path' does not exist.");
        }

        // Raising this value may increase performance
        $bufferSize = 4096; // read 4kb at a time
        $outputFileName = basename(str_replace('.gz', '', $path));
        $outputFilePath = sprintf("%s/%s", $outputDir, $outputFileName);

        // Open our files (in binary mode)
        $gzFile = gzopen($path, 'rb');
        $output = fopen($outputFilePath, 'wb');

        // Keep repeating until the end of the input file
        while (!gzeof($gzFile)) {
            // Read buffer-size bytes
            // Both fwrite and gzread and binary-safe
            fwrite($output, gzread($gzFile, $bufferSize));
        }

        // Files are done, close files
        fclose($output);
        gzclose($gzFile);

        return $outputFilePath;
    }

    /**
     * Checks whether a file is writable.
     *
     * @param string $file
     *
     * @return bool
     */
    public static function writable($file)
    {
        if (self::exists($file)) {
            return \is_writable($file);
        }
        $dir = dirname($file);
        return self::exists($dir) && \is_writable($dir);
    }

    /**
     * Checks whether a file is readable.
     *
     * @param string $file
     *
     * @return bool
     */
    public static function readable($file)
    {
        return \is_readable($file);
    }

    /**
     * Returns the number of lines in file.
     *
     * @param string $path
     *
     * @return int|false
     */
    public static function linesNumber(string $path)
    {
        if (!self::exists($path) || !self::readable($path)) {
            return false;
        }
        $file = new \SplFileObject($path, 'r');
        $file->seek(PHP_INT_MAX);
        return $file->key();
    }

    /**
     * Reads a specific line from file.
     *
     * @param string $path
     * @param int $lineNumber
     *
     * @return string|false
     */
    public static function readLine(string $path, int $lineNumber)
    {
        if (!self::exists($path) || !self::readable($path)) {
            return false;
        }
        $file = new \SplFileObject($path, 'r');
        $file->seek($lineNumber);
        return $file->current();
    }

    /**
     * Slice lines from file.
     *
     * @param string $file
     * @param string $offset
     * @param string $limit
     * @param bool $reverse
     * @return array|null
     */
    public static function slice(string $path, int $offset, int $limit, bool $reverse = false)
    {
        if (!self::exists($path) || !self::readable($path)) {
            return false;
        }
        $file = new \SplFileObject($path, 'r');
        if ($reverse) {
            $file->seek(PHP_INT_MAX);
            $fromLine = $file->key() - ($offset + $limit);
            if ($fromLine < 0) {
                $limit += $fromLine;
                if ($limit <= 0) {
                    return [];
                }
                $fromLine = 0;
            }
        } else {
            $fromLine = $offset;
        }
        $lines = new \LimitIterator($file, $fromLine, $limit);
        return iterator_to_array($lines);
    }

    /**
     * Retrieves lines from a file in reverse order and filters them based on the provided callback function.
     *
     * The callback function should take in one argument, which is a single line from the file, and return
     * a boolean value indicating whether the line should be included or excluded from the final result. It
     * can also return NULL to stop reading the file entirely.
     *
     * Reference:
     * https://stackoverflow.com/questions/3234580/read-a-file-backwards-line-by-line-using-fseek
     *
     * @param string $path
     * @param callable $callback
     * @param string $stoppedAt
     *
     * @return array
     */
    public static function filterInReverseOrder(string $path, callable $callback = null, ?string &$stoppedAt = null)
    {
        // Read file in reverse order 
        $lines = [];
        $fp = fopen($path, 'r');
        $current = '';
        $pos = -1;
        while (-1 !== fseek($fp, $pos, SEEK_END)) {
            $char = fgetc($fp);
            if (PHP_EOL == $char) {
                // We created a line, examine it.
                if ($callback) {
                    $result = $callback($current);
                    if ($result) {
                        $lines[] = $current;
                    } elseif (is_null($result)) {
                        $stoppedAt = $current;
                        return $lines; // If the callback returns null, stop.
                    }
                } else {
                    $lines[] = $current;
                }
                $current = '';
            } else {
                $current = $char . $current;
            }
            $pos--;
        }

        // We're left with the last line, examine it.
        if ($callback) {
            if ($callback($current)) {
                $lines[] = $current;
            }
        } else {
            $lines[] = $current;
        }

        // No stop signs, filtered all file.
        return $lines;
    }

    /**
     * Creates a temporary key file.
     *
     * @param string $content
     * @param string $prefix
     *
     * @return string
     */
    public static function createTemp(string $content, string $prefix = '')
    {
        $path = Str::joinPaths(
            Unix::getTempDir(),
            Str::randomString(8, $prefix)
        );
        File::write($path, $content);
        return $path;
    }

    /**
     * Returns the file extension.
     *
     * @param string $path
     * @return string
     */
    public static function ext(string $path)
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }

    /**
     * Tells whether the files have equal logic based on file type and content
     * after deleting comments.
     *
     * @param string $file1
     * @param string $file2
     * @param array &$diffs
     * @return bool
     */
    public static function isLogicEqual(string $file1, string $file2, ?array &$diffs = null)
    {
        $diffs = null;

        // Verify both files exist
        $conf1 = self::read($file1);
        $conf2 = self::read($file2);
        if ((is_null($conf1) && !is_null($conf2)) || (is_null($conf2) && !is_null($conf1))) {
            return false;
        }

        // Verfiy file extension is equal
        $type1 = self::ext($file1);
        $type2 = self::ext($file2);
        if ($type1 !== $type2) {
            return false;
        }

        return self::compareLogic($type1, $conf1, $conf2, $diffs);
    }

    /**
     * Compares logic of content after removing comments.
     *
     * @param string $type
     * @param string $conf1
     * @param string $conf2
     * @param array &$diffs
     * @return bool
     */
    public static function compareLogic(string $type, string $conf1, string $conf2, ?array &$diffs = null)
    {
        $diffs = null;

        // Delete comments based on file type
        if ($type === 'php') {
            $conf1 = self::removePhpComments($conf1);
            $conf2 = self::removePhpComments($conf2);
        } elseif (in_array($type, [ 'conf', 'sh' ])) {
            $conf1 = self::removeConfFileComments($conf1);
            $conf2 = self::removeConfFileComments($conf2);
        }

        // Compare content after comments are deleted
        $lines1 = explode(PHP_EOL, $conf1);
        $lines2 = explode(PHP_EOL, $conf2);
        $diffs = Arr::fullDiff($lines1, $lines2);
        return empty($diffs);
    }

    /**
     * Remove comments from conf file contnet.
     *
     * @param string $content
     * @return string
     */
    public static function removeConfFileComments(string $content)
    {
        $newStr = '';
        $lines = explode(PHP_EOL, $content);
        foreach ($lines as $line) {
            if (preg_match('/\s*#/', $line)) {
                continue;
            }
            $newStr .= $line . PHP_EOL;
        }
        return preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", PHP_EOL, $newStr);
    }

    /**
     * Remove PHP comments from code.
     *
     * https://stackoverflow.com/questions/503871/best-way-to-automatically-remove-comments-from-php-code
     *
     * @param string $content
     * @return string
     */
    public static function removePhpComments(string $content)
    {
        $newStr  = '';
        $commentTokens = array(T_COMMENT);
        if (defined('T_DOC_COMMENT')) {
            $commentTokens[] = T_DOC_COMMENT; // PHP 5
        }
        if (defined('T_ML_COMMENT')) {
            $commentTokens[] = T_ML_COMMENT;  // PHP 4
        }
        $tokens = token_get_all($content);
        foreach ($tokens as $token) {
            if (is_array($token)) {
                if (in_array($token[0], $commentTokens)) {
                    continue;
                }
                $token = $token[1];
            }
            $newStr .= $token . ($token === ';' ? PHP_EOL : '');
        }
        // Remove empty line
        return preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", PHP_EOL, $newStr);
    }
}
