<?php

namespace NizarBlond\LaravelPlus\Models;

use NizarBlond\LaravelPlus\Abstracts\ModelBase;
use NizarBlond\LaravelPlus\Support\Time;
use NizarBlond\LaravelPlus\Support\Arr;

class JobLog extends ModelBase
{
   /**
    * The table name.
    *
    * @var string
    */
    protected $table = 'job_logs';

    /**
     * The casting array.
     *
     * @var array
     */
    protected $casts = [
        'input' => 'array',
        'timeline' => 'array',
    ];

    /**
     * Adds a timeline entry.
     *
     * @param string $message
     * @param array $data
     * @param string $type
     * @var array
     */
    public function addTimeline(string $message, array $data = [], string $type = 'info')
    {
        $timeline = $this->timeline ?? [];
        $timeline[] = Arr::filterEmpty([
            'type' => $type,
            'message' => $message,
            'data' => $data,
            'time' => Time::now()->toDateTimeString(),
        ]);
        $this->timeline = $timeline;
    }
}

