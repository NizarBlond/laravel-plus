<?php

namespace NizarBlond\LaravelPlus\Models;

use NizarBlond\LaravelPlus\Abstracts\ModelBase;

class Job extends ModelBase
{
   /**
    * The table name.
    *
    * @var string
    */
    protected $table = 'jobs';
}
