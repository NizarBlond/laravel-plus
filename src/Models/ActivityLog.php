<?php

namespace NizarBlond\LaravelPlus\Models;

use NizarBlond\LaravelPlus\Abstracts\ModelBase;
use NizarBlond\LaravelPlus\Config\ActivityLogger as ALConfig;
use NizarBlond\LaravelPlus\Constants\ErrorLevel;
use NizarBlond\LaravelPlus\Support\Str;
use Carbon\Carbon;

class ActivityLog extends ModelBase
{
   /**
    * Whether timestamps exist.
    *
    * @var bool
    */
    public $timestamps = false;

   /**
    * The table name.
    *
    * @var string
    */
    protected $table;

    /**
     * The casting array.
     *
     * @var array
     */
     protected $casts = [
        'started_at' => 'datetime',
        'scheduled_at' => 'datetime'
     ];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->table = ALConfig::tableName();

        parent::__construct($attributes);
    }

    /**
     * Boot function for using with User Events.
     *
     * @todo Move events to an Observer.
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            if (!is_null($model->priority) && ($model->priority > 5 || $model->priority < 0)) {
                $model->exception("Invalid record priority value '{$model->priority}'.");
            }
            if (!is_null($model->error_level) && ($model->error_level > 5 || $model->error_level < 0)) {
                $model->exception("Invalid error level value '{$model->error_level}'.");
            }
        });

        static::saved(function ($model) {
            if (ALConfig::hasTableRowsLimit() && !($model->id % ALConfig::getTableRowCheckPivot())) {
                $maxUsed = ALConfig::getTableMaxUsedRows();
                $minFree = ALConfig::getTableMinFreeRows();
                $upperLimit = $maxUsed - $minFree;
                if ($upperLimit <= 0) {
                    $model->logWarning("The min free rows value must be greater than max used rows.");
                    return;
                }
                $size = ActivityLog::count();
                if ($size <= $upperLimit) {
                    return;
                }
                $toDelete = $size - $upperLimit;
                $model->logWarning(
                    sprintf(
                        "Deleting the oldest and/or less important %s records of %s...",
                        $toDelete,
                        ALConfig::tableName()
                    )
                );
                ActivityLog::orderBy('id', 'asc') // older first
                           ->orderBy('priority', 'desc') // lower first
                           ->orderBy('error_level', 'desc') // lower first
                           ->take($toDelete)
                           ->delete();
            }
        });
    }

    /**
     * Scope a query to get failed logs only.
     *
     * @param int $priority
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByPriority($query, $priority)
    {
        return $query->where('priority', $priority);
    }

    /**
     * Scope a query to get failed logs only.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetFailed($query)
    {
        return $query->where('error_level', '!=', ErrorLevel::NO_ERROR);
    }

    /**
     * Scope a query to get successful logs only.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetSuccessful($query)
    {
        return $query->where('error_level', ErrorLevel::NO_ERROR);
    }

    /**
     * Scope a query to get logs by guid.
     *
     * @param string $guid
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByGuid($query, $guid)
    {
        return $query->where('guid', $guid);
    }

    /**
     * Scope a query to get logs by type.
     *
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByType($query, $type)
    {
        return $query->where('type', $type);
    }
    
    /**
     * Scope a query to get logs by name.
     *
     * @param string $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByName($query, $name)
    {
        return $query->where('name', $name);
    }
    
    /**
     * Scope a query to get logs by name.
     *
     * @param string $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereNameLike($query, $name)
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * Scope a query to get logs by input.
     *
     * @param string $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereInputLike($query, $input)
    {
        return $query->where('input', 'like', '%' . $input . '%');
    }

    /**
     * Scope a query to get logs by output.
     *
     * @param string $output
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereOutputLike($query, $output)
    {
        return $query->where('output', 'like', '%' . $output . '%');
    }

    /**
     * Scope a query to get logs by host id.
     *
     * @param string $hostId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByHostId($query, $hostId)
    {
        return $query->where('host_id', $hostId);
    }
    
    /**
     * Scope a query to get logs by host name.
     *
     * @param string $hostName
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByHostName($query, $hostName)
    {
        return $query->where('host_name', $hostName);
    }
    
    /**
     * Scope a query to get logs by days ago.
     *
     * @param string $days
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetFromDaysAgo($query, $days)
    {
        return $query->where(
            'scheduled_at',
            '>',
            Carbon::now()->subDays($days)
        );
    }

    /**
     * Returns error level as string.
     *
     * @return string
     */
    public function getErrorLevelString()
    {
        return ErrorLevel::toString($this->error_level);
    }

    /**
     * Encodes input to JSON in case of array given.
     *
     * @param   string|array  $value
     *
     * @return  void
     */
    public function setInputAttribute($value)
    {
        $value = $this->sanitizeArray($value);
        $valueAsJson = $this->convertToJson($value);
        $this->attributes['input'] = $this->maybeTextValue($valueAsJson);
    }
    
    /**
     * Encodes output to JSON in case of array given.
     *
     * @param   string|array  $value
     *
     * @return  void
     */
    public function setOutputAttribute($value)
    {
        $value = $this->sanitizeArray($value);
        $valueAsJson = $this->convertToJson($value);
        $this->attributes['output'] = $this->maybeTextValue($valueAsJson);
    }
    
    /**
     * Encodes input to JSON in case of array given.
     *
     * @param   string|array  $value
     *
     * @return  void
     */
    public function setLogsAttribute($value)
    {
        $value = $this->sanitizeArray($value);
        $valueAsJson = $this->convertToJson($value);
        $this->attributes['logs'] = $this->maybeTextValue($valueAsJson);
    }
    
    /**
     * Decodes input from JSON in case of array given.
     *
     * @param   mixed  $value
     *
     * @return  mixed
     */
    public function getInputAttribute($value)
    {
        return $this->convertFromJson($value);
    }

    /**
     * Decodes output from JSON in case of array given.
     *
     * @param   mixed  $value
     *
     * @return  mixed
     */
    public function getOutputAttribute($value)
    {
        return $this->convertFromJson($value);
    }

    /**
     * Decodes output from JSON in case of array given.
     *
     * @param   mixed  $value
     *
     * @return  mixed
     */
    public function getLogsAttribute($value)
    {
        return $this->convertFromJson($value);
    }

    /**
     * Encodes metadata to JSON in case of array given.
     *
     * @param   string|array  $value
     *
     * @return  void
     */
    public function setMetadataAttribute($value)
    {
        $this->attributes['metadata'] = $this->convertToJson($value);
    }

    /**
     * Returns whether the log is a success log.
     *
     * @return boolean
     */
    public function isSuccess()
    {
        return ErrorLevel::NO_ERROR === $this->error_level;
    }

    /**
     * Checks whether the specified string value should be limited.
     *
     * @param   mixed $value
     *
     * @return  mixed
     */
    private function maybeTextValue($value)
    {
        if (!ALConfig::hasTableTextColumnSizeLimit()) {
            return $value;
        }

        if (!is_string($value)) {
            return $value;
        }

        $limit = ALConfig::getTableMaxTextColumnSize();
        $length = Str::length($value);
        if ($length >= $limit) {
            $value = Str::substr($value, 0, $limit) . '...';
            $this->logWarning("A text value has been shortened from $length to $limit.");
        }

        return $value;
    }
}
