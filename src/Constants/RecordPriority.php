<?php

namespace NizarBlond\LaravelPlus\Constants;

class RecordPriority
{
    const NO_PRIORITY = null;

    const VERY_HIGH = 1;

    const HIGH = 2;
    
    const NORMAL = 3;
    
    const LOW = 4;

    const VERY_LOW = 5;

    public static function toString($level)
    {
        switch ($level) {
            case self::NO_PRIORITY:
                return 'None';
            case self::VERY_HIGH:
                return 'Very High';
            case self::HIGH:
                return 'High';
            case self::NORMAL:
                return 'Normal';
            case self::LOW:
                return 'Low';
            case self::VERY_LOW:
                return 'Very Low';
            default:
                return 'N/A';
        }
    }
}
