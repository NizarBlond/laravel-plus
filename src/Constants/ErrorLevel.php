<?php

namespace NizarBlond\LaravelPlus\Constants;

class ErrorLevel
{
    const NO_ERROR = null;

    const CRITICAL = 1;
    
    const HIGH = 2;
    
    const NORMAL = 3;
    
    const LOW = 4;

    const WARNING = 5;

    public static function toString($level)
    {
        switch ($level) {
            case self::NO_ERROR:
                return 'None';
            case self::CRITICAL:
                return 'Critical';
            case self::HIGH:
                return 'High';
            case self::NORMAL:
                return 'Normal';
            case self::LOW:
                return 'Low';
            case self::WARNING:
                return 'Warinng';
            default:
                return 'N/A';
        }
    }
}
