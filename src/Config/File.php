<?php

namespace NizarBlond\LaravelPlus\Config;

class File
{
    public static function recycleBinPath()
    {
        return self::config('recycle_bin_path', true);
    }

    private static function config($path, $allowEmpty = false)
    {
        return LaravelPlus::config("file.$path", $allowEmpty);
    }
}
