<?php

namespace NizarBlond\LaravelPlus\Config;

class Que
{
    public static function default()
    {
        return self::config('default', true);
    }

    private static function config($path, $allowEmpty = false)
    {
        return LaravelPlus::config("queue.$path", $allowEmpty);
    }
}
