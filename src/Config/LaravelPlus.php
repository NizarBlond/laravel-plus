<?php

namespace NizarBlond\LaravelPlus\Config;

use NizarBlond\LaravelPlus\Traits\ClassHelpers;

class LaravelPlus
{
    use ClassHelpers;

    public static function rootPath()
    {
        return __DIR__ . '/../../';
    }

    public static function hostId()
    {
        return self::config('host.id');
    }

    public static function cors()
    {
        return self::config('cors', false);
    }

    public static function hostName()
    {
        return self::config('host.name', true);
    }

    public static function bytesToKilobyte()
    {
        return self::config('string.bytes_to_kilobyte', true) ?? 1024;
    }

    public static function dbMonitorEnabled()
    {
        // Not part of config because it is loaded before the config file.
        return env('LP_DB_MONITOR_ENABLED', false);
    }

    public static function dbMonitorDateFormat()
    {
        // Not part of config because it is loaded before the config file.
        return env('LP_DB_MONITOR_DATEFORMAT', 'h-i');
    }

    public static function config($path, $allowEmpty = false)
    {
        if (empty(config('laravel-plus'))) {
            self::exception("laravel-plus config was not loaded.");
        }

        $path = "laravel-plus.$path";
        $config = config($path);
        if (!$allowEmpty && is_null($config)) {
            self::exception("Invalid config path '$path'.");
        }

        return $config;
    }
}
