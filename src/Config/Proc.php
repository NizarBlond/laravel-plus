<?php

namespace NizarBlond\LaravelPlus\Config;

class Proc
{
    public static function defaultTimeout()
    {
        return self::config('default_timeout');
    }

    private static function config($path, $allowEmpty = false)
    {
        return LaravelPlus::config("proc.$path", $allowEmpty);
    }
}
