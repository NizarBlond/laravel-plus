<?php

namespace NizarBlond\LaravelPlus\Config;

class Log
{
    public static function getMinLoggerLevel()
    {
        return self::config('min_logger_level');
    }

    public static function getMinHistoryLevel()
    {
        return self::config('min_history_level');
    }

    public static function printLevelToStdout($level)
    {
        return in_array($level, self::config('stdout'));
    }

    public static function logProcessId()
    {
        return self::config('log_process_id');
    }

    public static function isDebugEnabled()
    {
        return self::config('debug_mode');
    }

    private static function config($path, $allowEmpty = false)
    {
        return LaravelPlus::config("log.$path", $allowEmpty);
    }
}
