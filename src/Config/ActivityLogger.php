<?php

namespace NizarBlond\LaravelPlus\Config;

class ActivityLogger
{
    public static function tableName()
    {
        return self::config('table.name');
    }

    public static function hasTableRowsLimit()
    {
        return self::getTableMaxUsedRows() > 0;
    }

    public static function getTableMaxUsedRows()
    {
        return self::config('table.max_used_rows');
    }

    public static function hasTableTextColumnSizeLimit()
    {
        return self::getTableMaxTextColumnSize() > 0;
    }

    public static function getTableMaxTextColumnSize()
    {
        return self::config('table.max_text_column_size');
    }

    public static function getTableErrorMsgNoRepeat()
    {
        return self::config('table.error_msg_no_repeat');
    }

    public static function getTableMinFreeRows()
    {
        return self::config('table.min_free_rows');
    }

    public static function getTableRowCheckPivot()
    {
        return self::config('table.check_rows_pivot');
    }

    public static function keysToRemove()
    {
        return self::config('keys_to_remove', true);
    }

    public static function keysToHide()
    {
        return self::config('keys_to_hide', true);
    }

    public static function isEnabledForModule($module)
    {
        return in_array($module, self::config('modules'));
    }

    private static function config($path, $allowEmpty = false)
    {
        return LaravelPlus::config("activity_logger.$path", $allowEmpty);
    }
}
