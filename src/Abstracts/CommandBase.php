<?php

namespace NizarBlond\LaravelPlus\Abstracts;

use Illuminate\Console\Command;
use NizarBlond\LaravelPlus\Traits\InstanceHelpers;
use NizarBlond\LaravelPlus\Support\Dir;
use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Support\Unix;
use NizarBlond\LaravelPlus\Support\Proc;
use NizarBlond\LaravelPlus\Services\ActivityLogger;
use Illuminate\Support\Facades\Artisan;
use NizarBlond\LaravelPlus\Constants\ErrorLevel;
use NizarBlond\LaravelPlus\Constants\RecordPriority;
use Exception;
use Carbon\Carbon;

abstract class CommandBase extends Command
{
    use InstanceHelpers;

    /**
     * The job's configuration array
     *
     * @var array
     */
    protected $config = array();
    
    /**
     * The job start time in unix timestamp
     *
     * @var integer
     */
    protected $startedAt;
    
    /**
     * The job schedule time in unix timestamp
     *
     * @var integer
     */
    protected $scheduledAt;
    
    /**
     * The command name.
     *
     * @var string
     */
    protected $name;

    /**
     * Indicates if the command success should be logged.
     *
     * @var bool
     */
    public $logSuccess = true;
    
    /**
     * Indicates if the command failure should be logged.
     *
     * @var bool
     */
    public $logFailure = true;
    
    /**
     * The default activity error level.
     *
     * @var \NizarBlond\LaravelPlus\Constants\ErrorLevel
     */
    protected $errorLevel = ErrorLevel::NORMAL;

    /**
     * The default record priority.
     *
     * @var \NizarBlond\LaravelPlus\Constants\RecordPriority
     */
    protected $priority = RecordPriority::NORMAL;

    /**
     * Prints time as part of the output line.
     *
     * @var bool
     */
    protected $printTime = false;

    /**
     * Do not actually perform the action if enabled.
     *
     * @var bool
     */
    protected $dryRun = false;

    /**
     * Tells whether to allow concurrent commands.
     *
     * @return false
     */
    protected $allowConcurrency = true;

    /**
     * The log activity type.
     *
     * @var string
     */
    const ACTIVITY_TYPE = 'command';
    
    /**
     * The command temporary folder.
     *
     * @var string
     */
    protected $tmpDir;

    /**
     * The command's guid.
     *
     * @var string
     */
    protected $guid;
    
    /**
     * Constructor.
     *
     * @param   array     $config
     * @param   array     $extraLog
     *
     * @return  void
     */
    public function __construct($config = [])
    {
        parent::__construct();
        
        $this->name         = $this->getClassName(2);
        $this->config       = array_merge($this->config, $config);
        $this->scheduledAt  = time();
        $this->guid         = Str::guid();

        // Log commands to file system
        $this->fsRuntimeLog = Str::joinPaths(storage_path(), 'logs', 'commands.log');
    }

    /**
     * Prints the proper operation verb based on dry-run option.
     *
     * @param bool $pluralize
     * @return string
     */
    protected function actionVerb(bool $pluralize = false)
    {
        return $this->dryRun ? 'will be' : ($pluralize ? 'were' : 'was');
    }

    /**
     * Retrives an option safely without failing if the option does not exist.
     *
     * @param   string  $key
     * @param   mixed   $default
     *
     * @return  mixed
     */
    protected function safeOption($key, $default = false)
    {
        if (!$this->hasOption($key)) {
            return $default;
        }
        return $this->option($key);
    }

    /**
     * Parses list argument.
     *
     * @param   string  $key
     * @param   string  $delimiter
     *
     * @return  array
     */
    protected function parseListArgument(string $key, string $delimiter = ',')
    {
        $val = $this->argument($key);
        if (empty($val)) {
            return [];
        }
        return explode($delimiter, $val);
    }

    /**
     * Parses list option.
     *
     * @param   string  $key
     * @param   string  $delimiter
     * @param   mixed   $default
     *
     * @return  array
     */
    protected function parseListOption(string $key, string $delimiter = ',', $default = [])
    {
        $val = $this->safeOption($key);
        if (!$val) {
            return $default;
        }
        return explode($delimiter, $val);
    }

    /**
     * Returns options as array.
     *
     * @return  array
     */
    protected function getOptionsArray()
    {
        $inputs = [];
        $options = $this->options();
        foreach ($options as $name => $option) {
            foreach ($_SERVER['argv'] ?? [] as $arg) {
                if (Str::contains($arg, '--'.$name)) {
                    $inputs[$name] = $option;
                    break;
                }
            }
        }
        return $inputs;
    }

    /**
     * Runs the command from code.
     * See: https://laravel.com/docs/5.2/artisan#calling-commands-via-code
     *
     * @param   array   $args
     *
     * @return  bool
     */
    public function runFromCode($args = [], &$output = null)
    {
        $result = Artisan::call($this->getName(), !empty($args) ? $args : []);
        $output = Artisan::output();
        return $result;
    }

    /**
     * Write a string as standard output.
     *
     * @param  string  $string
     * @param  string  $style
     * @param  int|string|null  $verbosity
     * @return void
     */
    public function line($string, $style = null, $verbosity = null)
    {
        $this->addLogData($string, [], $verbosity ?? 'info');
        if ($style !== 'comment' && $this->printTime && !Str::startsWith($string, '*')) {
            $string = sprintf('[%s] %s', Carbon::now()->toDateTimeString(), $string);
        }
        return parent::line($string, $style, $verbosity);
    }

    /**
     * Asynchronously runs the command from code.
     *
     * @param   array   $args
     *
     * @return  bool
     */
    public function runFromCodeAsync($args = [])
    {
        // TODO:
    }

    /**
     * Execute the job.
     *
     * Note that the shell status is determined by the output of this function.
     * By default, void is considered 0 (success) and exception 1 (failure).
     * Do not return "true", otherwise the shell running it will consider it
     * a failed function despite being successful.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->fsRuntimeLog)) {
            $this->writeFSRuntimeLog(true);
        }

        $this->startedAt = time();

        $logMessage = sprintf(
            "Starting '%s' command at %s",
            $this->name,
            gmdate('Y-m-d H:i:s', $this->startedAt)
        );
        self::log($logMessage);

        try {
            $this->validate();
            $this->initDir();
            $this->init();
            $output = $this->process();
            $this->onComplete(null);
            $this->onSuccess($output);
            $this->cleanTemp();
        } catch (Exception $e) {
            $this->onComplete($e);
            $this->onFailure($e);
            $this->cleanTemp();
            throw $e;
        }
    }

    /**
     * Initializes the command right before execution.
     *
     * @return void
     */
    protected function init()
    {
        $this->dryRun = $this->safeOption('dry-run');
        $this->debugMode = $this->safeOption('debug-high') ? 'verbose' : $this->safeOption('debug');
    }

    /**
     * Validates the command before execution.
     *
     * @return void
     */
    protected function validate()
    {
        if (!$this->allowConcurrency) {
            $signature = strtok($this->signature, " \n\t");
            $currentPID = Proc::currentPID();
            $parallels = 0;
            $processes = Unix::listProcesses();
            $commands = $processes['COMMAND'] ?? [];
            foreach ($commands as $pid => $command) {
                if ($currentPID !== $pid && Str::contains($command, $signature)) {
                    $parallels++;
                }
            }
            if ($parallels) {
                $this->exception(sprintf('Concurrent "%s" processes is not allowed.', $signature));
            }
        }
    }

    /**
     * Sets the default error level.
     *
     * @param   NizarBlond\LaravelPlus\Constants\ErrorLevel
     *
     * @return  void
     */
    protected function setErrorLevel($level)
    {
        if (! is_int($level)) {
            $this->exception("Invalid error level.");
        }

        $this->errorLevel = $level;
    }

    /**
     * The system's temporary directory.
     *
     * @return string
     */
    protected function getTempDir()
    {
        return sys_get_temp_dir();
    }

    /**
     * Handle the command completion (success/error) event.
     *
     * @param   Exception|null
     *
     * @return  void
     */
    protected function onComplete($error = null)
    {
        if (!empty($this->fsRuntimeLog)) {
            $this->writeFSRuntimeLog(false, $error);
        }
    }

    /**
     * Handle the job success event internally
     *
     * @return void
     */
    protected function onSuccess($output = null)
    {
        // Get execution time in seconds
        $finishedAt = time();
        $duration = $finishedAt - $this->startedAt;
        
        // Log command output
        $logMessage = sprintf(
            "Command '%s' successfully finished at %s after %ss",
            $this->name,
            gmdate('Y-m-d H:i:s', $finishedAt),
            $duration
        );
        self::info($logMessage);

        // Log successful job activity
        $this->recordActivity($duration, $output);
    }

    /**
     * Handle the job failure event internally
     *
     * @param Exception $e
     * @return void
     */
    protected function onFailure($e)
    {
        // Get execution time in seconds
        $finishedAt = time();
        $duration = $finishedAt - $this->startedAt;
        
        // Log command output
        $logMessage = sprintf(
            "Command '%s' failed at %s after %ss: %s",
            $this->name,
            gmdate('Y-m-d H:i:s', $finishedAt),
            $duration,
            is_string($e) ? $e : $e->getMessage()
        );
        self::error($logMessage);

        // Log failed job activity
        $this->recordFailedActivity($e, $duration);

        // Print error
        $this->logError($e);
    }

    /**
     * Executes the job.
     *
     * @return void
     */
    protected function process()
    {
        //
    }

    /**
     * Records activity.
     *
     * @param   integer     $duration   The duration in seconds. Optional.
     * @param   mixed       $output     The output. Optional.
     *
     * @return  ActivityLog
     */
    protected function recordActivity($duration = null, $output = null)
    {
        if (!$this->logSuccess) {
            return;
        }

        return ActivityLogger::record(
            $this->scheduledAt,
            $this->startedAt,
            $this->name,
            [
                'arguments' => $this->arguments(),
                'options' => $this->options(),
                'config' => $this->config,
            ],
            $output,
            self::ACTIVITY_TYPE,
            $this->priority,
            $this->getLogData(),
            $duration
        );
    }

    /**
     * Records a warning to the database.
     *
     * @param   Exception     $e    The activity exception.
     *
     * @return  ActivityLog         The created activity log instance.
     */
    protected function recordActivityWarning($e)
    {
        return $this->recordFailedActivity(
            $e,
            null,
            ErrorLevel::WARNING
        );
    }
    
    /**
     * Records a failed activity to the database.
     *
     * @param   Exception     $error        The activity exception.
     * @param   string        $duration     The duration in seconds. Optional.
     * @param   int           $level        The error level. Optional.
     *
     * @return  ActivityLog                 The created activity log instance.
     */
    protected function recordFailedActivity(
        $e,
        $duration = null,
        $level = null
    ) {
        if (!$this->logFailure) {
            return;
        }

        return ActivityLogger::recordFailure(
            $this->scheduledAt,
            $this->startedAt,
            $this->name,
            [
                'arguments' => $this->arguments(),
                'options' => $this->options(),
                'config' => $this->config,
            ],
            $e,
            self::ACTIVITY_TYPE,
            $this->priority,
            $this->getLogData(),
            $duration,
            null, // output
            null, // destination
            null, // guid
            !empty($level) ? $level : $this->errorLevel
        );
    }

    /**
     * Create a temporary folder for the current command.
     *
     * @return void
     */
    private function initDir()
    {
        $this->tmpDir = Str::joinPaths(
            self::getTempDir(),
            sprintf(
                "%s_%s_%s",
                Str::slugify($this->getClassName(2)),
                gmdate('y-m-d_h-i-s'),
                Str::random(5)
            )
        );
        
        Dir::make($this->tmpDir);
    }

    /**
     * Cleans the command's temporary folder
     *
     * @return void
     */
    private function cleanTemp()
    {
        if (!$this->tmpDir) {
            $this->logWarning(sprintf(
                "Temporary directory not found for '%s' command.",
                $this->name
            ));
            return;
        }
        Dir::delete($this->tmpDir, false);
    }
}
