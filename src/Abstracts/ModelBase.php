<?php

namespace NizarBlond\LaravelPlus\Abstracts;

use Illuminate\Database\Eloquent\Model;
use NizarBlond\LaravelPlus\Traits\InstanceHelpers;
use NizarBlond\LaravelPlus\Traits\FractalHelpers;
use NizarBlond\LaravelPlus\Support\Arr;
use NizarBlond\LaravelPlus\Support\Time;
use NizarBlond\LaravelPlus\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

abstract class ModelBase extends Model
{
    use InstanceHelpers, FractalHelpers;

    /**
     * Whether to allow non-strict changes for some casts such as of 'array' type.
     *
     * @var bool
     */
    protected $allowStrictChangesOnly = false;

    /**
     * Tells whether to cache fractal model data.
     *
     * @var bool
     */
    protected $fractalModelCache = false;

    /**
     * Tells whether to cache relationship data.
     *
     * @var bool
     */
    protected $relationshipCache = false;

    /**
     * Indicates if the model was updated during the current request lifecycle.
     *
     * Note that this is added in order to bypass $this->save() that on update
     * returns true even if the database data was not updated.
     *
     * @var bool
     */
    public $wasRecentlyUpdated = false;

    /**
     * Boot function for using with User Events.
     *
     * @todo Move events to an Observer.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        /**
         * updating is a "halting" event which means when Illuminate\Events\Dispatcher::dispatch()
         * is called the $halt is true because it was fired from Model::performUpdate() with the
         * default $halt value. Quoted from Dispatch::dispatch():
         * "If a response is returned from the listener and event halting is enabled we will just
         * return this response, and not call the rest of the event listeners. Otherwise we will
         * add the response on the response list."
         *
         * In other words, if you override this function in a child class then only one callback
         * will be listened to; the one that was called first.
         */
        static::updating(function ($model) {
            return ModelBase::isStrictlyDirty($model);
        });

        static::deleted(function ($model) {
            $model->removeFractalCache('deleted');
        });

        static::updated(function ($model) {
            $model->removeFractalCache('updated');
        });

        static::created(function ($model) {
            // We use it for dependent models on others
            $model->removeFractalCache('created');
        });
    }

    /**
     * Perform a model update operation.
     *
     * We use this function in order as a workaround to get an indication whether
     * the database data was updated.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return bool
     */
    protected function performUpdate(Builder $query)
    {
        // Check if it was intercepted by 'updated' event.
        $saved = parent::performUpdate($query);
        if (!$saved) {
            return $saved;
        }

        // Check if there was any dirt becuase that's what invokes the syncChanges()
        if (!$this->wasRecentlyUpdated) {
            $dirty = $this->getDirty();
            $this->wasRecentlyUpdated = count($dirty) > 0;
        }

        return $saved; // do not interfere with original value
    }

    /**
     * Returns differences between the current and original attributes.
     *
     * @return array
     */
    public function getDirtyDiffs()
    {
        $diffs = [];
        foreach ($this->getDirty() as $key => $value) {
            $diffs[$key] = [
                'current' => $value,
                'original' => $this->getOriginal($key)
            ];
        }
        return $diffs;
    }

    /**
     * Tells if the model was saved to database during the current request lifecycle.
     *
     * @return  bool
     */
    public function wasRecentlySaved()
    {
        return $this->wasRecentlyUpdated || $this->wasRecentlyCreated;
    }

    /**
     * Checks whether to refresh model data.
     *
     * @param   string $event
     *
     * @return  void
     */
    public function removeFractalCache($event)
    {
        if (!$this->fractalModelCache) {
            return;
        }

        $performRemove = false;

        if ($event === 'updated') {
            // Remove resolved relationships
            $current = Arr::only($this->toArray(), array_keys($this->getOriginal()));
            // Do a deep comparison between two arrays and examine the differences
            if (!Arr::isDataEqual($current, $this->getOriginal(), $diffs, false)) {
                $ignoredFields = [ 'created_at', 'checked_at', 'updated_at' ];
                foreach ($diffs as $key => $value) {
                    if (!Str::containsAny($key, $ignoredFields)) {
                        $performRemove = true;
                        break;
                    }
                }
            }
        } elseif ($event === 'deleted' || $event === 'created') {
            $performRemove = true;
        } else {
            $this->exception('Event %s is not supported.', $event);
        }

        if ($performRemove) {
            $this->removeModelFractalDataFromCache();
        }
    }

    /**
     * Converts a given object/array to JSON string.
     *
     * @param   object|array  $value
     *
     * @return  string
     */
    protected function convertToJson($value)
    {
        return is_array($value) || is_object($value)
                ? Str::jsonEncode($value)
                : $value;
    }

    /**
     * Converts a given string to object/array.
     *
     * @param   string  $value
     *
     * @return  array
     */
    protected function convertFromJson($value, $assoc = true)
    {
        return is_string($value) && Str::isJson($value)
                ? Str::jsonDecode($value, $assoc)
                : $value;
    }

    /**
     * Converts a datetime/date to date string
     *
     * @param   string|Carbon  $value
     *
     * @return  string
     */
    protected function convertToDateString($value)
    {
        return is_string($value) ? $value : Carbon::parse($value)->toDateString();
    }

    /**
     * Returns a copy of the model as collection.
     *
     * @return  \Illuminate\Database\Eloquent\Collection
     */
    public function toCollection()
    {
        return static::where('id', '=', $this->id)->get();
    }

    /**
     * Delete the model from the database if older than the
     * specified minutes.
     *
     * @param   int $mins
     *
     * @return  bool|null
     *
     * @throws  \Exception
     */
    public function deleteIfOlderThan($mins)
    {
        $createdAt = $this->getAttribute('created_at');
        if (empty($createdAt)) {
            $this->exception("Invalid created_at attribute.");
        }

        if ($createdAt->addMinutes($mins) > Carbon::now()) {
            $this->log(sprintf(
                "Not deleting '%s' model (id=%s, created_at=%s): not older than %s mins.",
                get_class($this),
                $this->id,
                $this->created_at,
                $mins
            ));
            return false;
        }

        return $this->delete();
    }

    /**
     * Scope a query to get entries updated it mins ago.
     *
     * @param   int  $mins
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByUpdatedMinsAgo($query, int $mins)
    {
        if (!$this->usesTimestamps()) {
            $this->exception("Scope is only valid for models that use timestamps.");
        }
        return $query->where('updated_at', '>=', \Carbon\Carbon::now()->subMinutes($mins));
    }

    /**
     * Scope a query to get entries updated it secs ago.
     *
     * @param   int  $secs
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByUpdatedSecsAgo($query, int $secs)
    {
        if (!$this->usesTimestamps()) {
            $this->exception("Scope is only valid for models that use timestamps.");
        }
        return $query->where('updated_at', '>=', \Carbon\Carbon::now()->subSeconds($secs));
    }

    /**
     * Scope a query to get only selected slug name.
     *
     * @param   string  $slug
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetBySlug($query, string $slug)
    {
        return $query->where('slug', $slug);
    }
    
    /**
     * Scope a query to get only selected slug names.
     *
     * @param   string  $slugs
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetBySlugs($query, array $slugs)
    {
        return $query->whereIn('slug', $slugs);
    }

    /**
     * Scope a query to get regions by id.
     *
     * @param   string|integer  $id
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetById($query, $id)
    {
        return $query->where('id', $id);
    }

    /**
     * Scope a query to get regions by id.
     *
     * @param   array  $ids
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByIds($query, array $ids)
    {
        return $query->whereIn('id', $ids);
    }

    /**
     * Scope a query to get only selected name.
     *
     * @param   string  $name
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByName($query, string $name)
    {
        return $query->where('name', $name);
    }

    /**
     * Scope a query to get only selected names.
     *
     * @param   array  $names
     *
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByNames($query, array $names)
    {
        return $query->whereIn('name', $names);
    }

    /**
     * Checks whether the module has the specified attribute.
     *
     * @param   string  $attr
     *
     * @return  bool
     */
    public function hasAttribute($attr)
    {
        return array_key_exists($attr, $this->attributes);
    }

    /**
     * Checks whether the module has the specified metadata.
     *
     * @param   string  $key
     *
     * @return  bool
     */
    public function hasMeta($key)
    {
        return array_key_exists($key, $this->metadata ?? []);
    }

    /**
     * Returns the value of the provided meta key.
     *
     * @param   string  $key
     * @param   mixed   $default
     *
     * @return  mixed
     */
    public function getMeta($key, $default = null)
    {
        if (!$this->hasMeta($key)) {
            return $default;
        }
        return $this->metadata[$key];
    }

    /**
     * Sets the value of the provided meta key.
     *
     * @param   string  $key
     * @param   mixed   $value
     *
     * @return  array
     */
    public function setMeta($key, $value)
    {
        $this->metadata = array_merge($this->metadata ?? [], [ $key => $value ]);
        return $this->metadata;
    }

    /**
     * Removes the provided meta key.
     *
     * @param   string    $key
     *
     * @return  array
     */
    public function removeMeta($key)
    {
        if ($this->hasMeta($key)) {
            // Workaround for Indirect modification of overloaded property
            // App\Models\[XXX]::$metadata has no effect
            $copy = $this->metadata;
            unset($copy[$key]);
            $this->metadata = $copy;
        }
        return $this->metadata;
    }

    /**
     * Compares whether the specified model is the same.
     *
     * @param   ModelBase  $model
     *
     * @return  bool
     */
    public function equalTo(ModelBase $model)
    {
        return self::isEqual($this, $model);
    }

    /**
     * Compares whether the two models are equal.
     *
     * @param   ModelBase  $model1
     * @param   ModelBase  $model2
     *
     * @return  bool
     */
    public static function isEqual(ModelBase $model1, ModelBase $model2)
    {
        return $model1->getClassName() == $model2->getClassName() && $model1->id == $model2->id;
    }

    /**
     * Checks whether to refresh model data.
     *
     * @param   ModelBase  $model
     * @param   bool       $strictCompare
     *
     * @return  bool
     */
    public static function isStrictlyDirty(ModelBase $model, bool $strictCompare = false)
    {
        if (!$model->allowStrictChangesOnly) {
            return true;
        }

        // Get the dirty values discovered by the default comparison that Laravel does
        // using Model::originalIsEquivalent() method. For primitive types (array is
        // included), the comparison is simply using "===" which is not always the
        // desired outcome for applications that consider array a complex type.
        $dirty = $model->getDirty();
        $casts = $model->getCasts();

        $changes = [];
        foreach ($dirty as $key => $value) {
            // Get the original fetched from database
            $original = $model->getOriginal($key);
            // For casts of type array do a deep comparison of data without actually
            // caring about the references or keys order.
            if (!empty($casts[$key]) && $casts[$key] === 'array') {
                $current = $model->fromJson($value);
                if (Arr::isDataEqual($current, $original, $diffs, $strictCompare)) {
                    $changes[] = $key;
                }
            }
            // For other types just apply non-strict data comparison while also taking
            // into consideration time variables to cover Carbon vs string cases.
            elseif ($value == $original || Time::equals($value, $original)) {
                $changes[] = $key;
            }
        }

        // If all dirty changes are non-strict then stop the update
        return count($dirty) !== count($changes);
    }

    /**
     * Finds or creates a relationship attribute from metadata in order to
     * save database calls.
     *
     * @param string $relationship
     * @param string $key
     * @return string
     */
    protected function findOrCreateRelationshipAttributeInMeta(string $relationship, string $key)
    {
        // Check whether the value is already cached
        if ($this->relationshipCache) {
            $metaKey = sprintf('_%s_%s', Str::replace('.', '_', $relationship), $key);
            if ($this->hasMeta($metaKey)) {
                return $this->getMeta($metaKey);
            }
        }

        // Support deep relationships
        $relationships = explode('.', $relationship);
        $relationship_0 = $relationships[0];
        $current = $this->$relationship_0;
        for ($i=1; $i < count($relationships); $i++) {
            if ($current) {
                $relationship_i = $relationships[$i];
                $current = $current->$relationship_i;
            } 
        }
        $value = $current->$key ?? null;

        // Update metadata on a fresh copy
        if ($this->relationshipCache) {
            $this->saveMetaQuietly($metaKey, $value);
        }

        return $value;
    }

    /**
     * Saves metadata quietly on a fresh copy to prevent saving other
     * values by accident without firing any events.
     *
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    protected function saveMetaQuietly(string $key, $value)
    {
        $model = static::getById($this->id)->first();
        if (!$model) {
            return false;
        }
        $model->setMeta($key, $value);
        return $model->saveQuietly(); // Do not fire events
    }

    /**
     * Returns complex meteadata.
     *
     * @param string $metakey
     * @return array
     */
    protected function getComplexMeta(string $metakey)
    {
        $data = $this->metadata[$metakey] ?? null;
        if (!empty($data)) {
            return $this->convertFromJson($data);
        }
        return [];
    }
}
