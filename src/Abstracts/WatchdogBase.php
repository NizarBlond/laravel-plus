<?php

namespace NizarBlond\LaravelPlus\Abstracts;

use NizarBlond\LaravelPlus\Support\Proc;
use NizarBlond\LaravelPlus\Support\File;
use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Support\Dir;
use NizarBlond\LaravelPlus\Constants\ErrorLevel;
use Carbon\Carbon;

class WatchdogBase extends CommandBase
{
    /**
     * Indicates if the command success should be logged.
     *
     * @var bool
     */
    public $logSuccess = false;

    /**
     * Deletes the specified object.
     *
     * @param string $path
     * @param string $reason
     * @param bool $trash
     * @return bool
     */
    protected function delete(string $path, string $reason = 'n/a', bool $trash = true)
    {
        $type = Dir::exists($path, true) ? 'dir' : 'file';
        if ($this->dryRun) {
            $this->warn(sprintf('(%s) %s should be deleted: %s.', $path, $type, $reason));
            return true;
        }

        $type === 'dir' ? Dir::delete($path, $trash) : File::delete($path, $trash);
        if (File::missing($path)) {
            $this->issueWarning(sprintf('(%s) %s was deleted: %s.', $path, $type, $reason));
            return true;
        }

        $this->issueWarning(sprintf('Failed to delete %s.', $path));
        return false;
    }

    /**
     * Permantly deletes the specified object.
     * 
     * @param string $path
     * @param string $reason
     * @return bool
     */
    protected function permanentlyDelete(string $path, string $reason = 'n/a')
    {
        return $this->delete($path, $reason, false);
    }

    /**
     * Issues a warning and reports it to developers.
     *
     * @param string $msg
     * @return void
     */
    protected function issueWarning(string $msg)
    {
        $this->logWarning($msg);
        $this->warn($msg);

        if ($this->dryRun) {
            return;
        }

        $this->recordActionActivity($msg);
    }

    /**
     * Issues an error and reports it to developers.
     *
     * @param string $msg
     * @param bool $dontReport
     * @return void
     */
    protected function issueError($msg, $dontReport = false)
    {
        $this->logError($msg);
        $this->error($msg);
        if (!$dontReport) {
            $this->recordActionActivity($msg);
        }
    }

    /**
     * Issues an error and reports it to developers.
     *
     * @param string $path
     * @param string $owner
     * @param string|null $group
     * @return void
     */
    protected function setDirOwner(string $path, string $owner, ?string $group = null)
    {
        $group = $group ?? $owner;
        return Proc::runCommand("chown -R '$owner:$group' $path", 15, false);
    }

    /**
     * Records warning activity.
     *
     * @param mixed $e
     * @return void
     */
    protected function recordActionActivity($e)
    {
        $this->recordFailedActivity($e, null, ErrorLevel::WARNING);
    }
    
    /**
     * Reads php-fpm option from config.
     *
     * @param string $conf
     * @param string $key
     * @param string $type
     * @return string|null
     */
    protected function readPhpFpmConfValue(string $conf, string $key, string $type = 'php_admin_value')
    {
        $pattern = sprintf('/%s\[%s\]\s*=\s*(.*)$/m', $type, $key);
        if (preg_match($pattern, $conf, $match)) {
            return $match[1];
        }
        return null;
    }

    /**
     * Reads nginx option from config.
     *
     * @param string $conf
     * @param string $key
     * @return string|null
     */
    protected function readNginxConfValue(string $conf, string $key)
    {
        $pattern = sprintf('/%s\s+(.*)$/m', $key);
        if (preg_match($pattern, $conf, $match)) {
            return rtrim(trim($match[1]), ';');
        }
        return null;
    }

    /**
     * Converts wildcard expression to pattern.
     *
     * @param string $wildcard
     * @return string
     */
    protected function wildcardToRegex(string $wildcard)
    {
        $value = $wildcard;
        $value = str_replace('/', '\/', $value);
        $value = str_replace('.', '\.', $value);
        $value = str_replace('*', '.*', $value);
        return '/'.$value.'/';
    }

    /**
     * Kills the provided process.
     *
     * @param int $pid
     * @return bool
     */
    protected function killProcess(string $pid, string $reason = '')
    {
        if ($this->dryRun) {
            $this->warn(sprintf('(%s) process should be killed: reason=%s.', $pid, $reason ?? 'n/a'));
            return;
        }
        $killed = exec("kill -9 $pid") !== false;
        if ($killed) {
            $this->issueWarning(sprintf('(%s) process was killed: reason=%s.', $pid, $reason ?? 'n/a'));
        } else {
            $this->issueWarning(sprintf('(%s) failed to kill process.', $pid));
        }
        return $killed;
    }

    /**
     * Adds a cron job.
     *
     * @param string $cron
     * @return bool
     */
    protected function addCronJob(string $cron)
    {
        if ($this->dryRun) {
            $this->warn(sprintf('(ews) cron job "%s" should be added.', $cron));
            return;
        }
        $cmd = sprintf('(crontab -l 2>/dev/null; echo "%s") | crontab -', $cron);
        shell_exec($cmd);
        if (!in_array($cron, $this->listCronJobs())) {
            $this->exception(sprintf('Failed to add cron job "%s".', $cron));
        }
    }

    /**
     * Removes a cron job.
     *
     * @param string $cron
     * @return bool
     */
    protected function removeCronJob(string $cron)
    {
        if ($this->dryRun) {
            $this->warn(sprintf('(ews) cron job "%s" should be removed.', $cron));
            return;
        }
        $crons = $this->listCronJobs();
        shell_exec('crontab -r');
        foreach ($crons as $i => $_cron) {
            if ($_cron !== $cron) {
                $this->addCronJob($_cron);
            }
        }
    }

    /**
     * Lists all cron jobs.
     *
     * @param string|null $user
     * @return array
     */
    protected function listCronJobs(?string $user = null)
    {
        $cmd = 'crontab -l';
        if (!empty($user)) {
            $cmd .= ' -u ' . $user;
        }
        $output = shell_exec($cmd);
        if (empty($output) || Str::contains($output, 'no crontab for')) {
            return [];
        }
        return array_filter(explode(PHP_EOL, $output));
    }

    /**
     * Removes a cron job by substring.
     *
     * @param string $user
     * @param string $substring
     * @return bool
     */
    protected function removeCronJobBySubstring(string $user, string $substring)
    {
        // Get current crontab
        $existingCrontab = shell_exec("crontab -u $user -l 2>/dev/null");
    
        // Check if the cron job exists
        if (!Str::contains($existingCrontab, $substring)) {
            return false;
        }

        // If it's a dry run, return true.
        if ($this->dryRun) {
            $this->debug(sprintf('(%s) dry run: removing cron job by substring: %s', $user, $substring), [
                'existingCrontab' => $existingCrontab,
                'substring' => $substring,
            ]);
            return true;
        }

        // Remove only the matching line(s) and keep the rest
        $updatedCrontab = preg_replace("/^.*" . preg_quote($substring, "/") . ".*$/m", "", $existingCrontab);

        // Remove empty lines
        $updatedCrontab = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $updatedCrontab);
    
        // Save the new crontab (only if it's not empty)
        if (!empty(trim($updatedCrontab))) {
            file_put_contents("/tmp/crontab_$user", $updatedCrontab); // Save to a temporary file
            exec("crontab -u $user /tmp/crontab_$user"); // Install the new crontab
            unlink("/tmp/crontab_$user"); // Cleanup
        } else {
            // If the last job is removed, clear the crontab
            exec("crontab -u $user -r");
        }
    
        return true;
    }

    /**
     * Returns all files in the specified path by prefix (depth=0).
     *
     * @param string $path
     * @param bool $basename
     * @return array
     */
    protected function getFilesByPrefix(string $prefix, bool $basename = false)
    {
        $files = glob($prefix . '*', 0);
        return $basename ? array_map("basename", $files) : $files;
    }

    /**
     * Returns all files in the specified path.
     *
     * @param string $path
     * @param int $maxDepth
     * @return array
     */
    protected function getFilesFromSubdir(string $path, int $maxDepth = 3)
    {
        $path = rtrim($path, '/*');
        if (!Dir::exists($path)) {
            return [];
        }
        $filesWithDots = Dir::scanRecursive($path, false, $maxDepth);
        foreach ($filesWithDots as $i => $file) {
            $basename = basename($file);
            if ($basename === '..') {
                unset($filesWithDots[$i]);
            } elseif ($basename === '.') {
                $filesWithDots[$i] = rtrim($file, '/.');
            }
        }
        return array_unique($filesWithDots);
    }

    /**
     * Runs an app script.
     *
     * @param string $script
     * @param array $args
     * @return void
     */
    protected function runCommands(array $commands)
    {
        if ($this->dryRun) {
            $this->debug(sprintf('should run commands: commands=%s.', json_encode($commands)));
            return;
        }
        Proc::runCommands($commands);
    }

    /**
     * Copy file.
     *
     * @param string $src
     * @param string $dst
     * @return void
     */
    protected function copyFile(string $src, string $dst)
    {
        if ($this->dryRun) {
            $this->warn(sprintf('should copy file "%s" to "%s"', $src, $dst));
            return;
        }
        $this->issueWarning(sprintf('copied file "%s" to "%s".', $src, $dst));
        File::copy($src, $dst);
    }

    /**
     * Delete file.
     *
     * @param string $src
     * @param string $dst
     * @return void
     */
    protected function renameFile(string $src, string $dst)
    {
        if ($this->dryRun) {
            $this->warn(sprintf('should rename file "%s" to "%s"', $src, $dst));
            return;
        }
        $this->issueWarning(sprintf('renamed file "%s" to "%s".', $src, $dst));
        File::rename($src, $dst);
    }
}