<?php

namespace NizarBlond\LaravelPlus\Abstracts;

use NizarBlond\LaravelPlus\Traits\InstanceHelpers;
use NizarBlond\LaravelPlus\Services\ActivityLogger;
use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Constants\ErrorLevel;
use NizarBlond\LaravelPlus\Constants\RecordPriority;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TransferException;
use Exception;

abstract class HttpClientBase
{
    use InstanceHelpers;

    /**
     * The http client.
     *
     * @var GuzzleHttp\Client
     */
    private $client = null;

    /**
     * The http client name.
     *
     * @var string
     */
    private $name;
    
    /**
     * The request timeout in seconds.
     *
     * @var string
     */
    private $requestTimeout = 5;

    /**
     * Whether to return false instead of throwing exception.
     *
     * @var boolean
     */
    private $suppressException = false;

    /**
     * Whether to log failure.
     *
     * @var boolean
     */
    private $suppressFailureLog = false;

    /**
     * Whether to log success.
     *
     * @var boolean
     */
    private $suppressSuccessLog = false;

    /**
     * The last exception.
     *
     * @var \Exception
     */
    protected $thrownException = null;

    /**
     * The last exception.
     *
     * @var string
     */
    protected $thrownExceptionMessage = null;

    /**
     * The latest exception.
     *
     * @var \Psr\Http\Message\ResponseInterface
     */
    protected $latestResponse = null;

    /**
     * The default activity error level.
     *
     * @var \NizarBlond\LaravelPlus\Constants\ErrorLevel
     */
    protected $errorLevel = ErrorLevel::NORMAL;

    /**
     * The default record priority.
     *
     * @var \NizarBlond\LaravelPlus\Constants\RecordPriority
     */
    protected $priority = RecordPriority::NORMAL;

    /**
     * Indicates if the job success should be logged.
     *
     * @var bool
     */
    public $logSuccess = true;
    
    /**
     * Indicates if the job failure should be logged.
     *
     * @var bool
     */
    public $logFailure = true;

    /**
     * The log activity type.
     *
     * @var string
     */
    const ACTIVITY_TYPE = 'http_client';

    /**
     * Constructor.
     */
    protected function __construct($token = null, $debug = false)
    {
        $this->name = $this->getClassName(2);
        $this->client = $this->makeHttpClient($token);
        $this->debugMode = $debug;
    }

    /**
     * Returns the latest response returned by client.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getLatestResponse()
    {
        return $this->latestResponse;
    }

    /**
     * Suppresses logging on successful operations.
     *
     * @return void
     */
    public function suppressLogOnSuccess()
    {
        $this->suppressSuccessLog = true;
    }
    
    /**
     * Returns whether to suppress exception.
     *
     * @return  boolean
     */
    public function isSuppressExceptionEnabled()
    {
        return $this->suppressException;
    }

    /**
     * Unsuppress logging on successful operations.
     *
     * @return void
     */
    public function unsuppressLogOnSuccess()
    {
        $this->suppressSuccessLog = false;
    }

    /**
     * Suppresses exceptions and returns boolean instead.
     *
     * @param   boolean     $suppressLog
     *
     * @return  boolean
     */
    public function suppressException($suppressLog = true)
    {
        $this->suppressException    = true;
        $this->suppressFailureLog   = $suppressLog;
    }

    /**
     * Removes exception suppression.
     *
     * @param   boolean     $unsuppressLog
     *
     * @return  boolean
     */
    public function unsuppressException($unsuppressLog = true)
    {
        $this->suppressException    = false;
        $this->suppressFailureLog   = !$unsuppressLog;
    }

    /**
     * Returns the thrown exception.
     *
     * @return \Exception|null
     */
    public function getException()
    {
        return $this->thrownException;
    }

    /**
     * Parses last error message.
     *
     * @return string
     */
    public function parseExceptionMessage()
    {
        $e = $this->thrownException;
        if (empty($e)) {
            return; // Empty exception: nothing to parse!
        }

        if ($e instanceof RequestException) {
            $response = $e->getResponse();
            if (empty($response)) {
                $this->thrownExceptionMessage = $e->getMessage();
                return;
            }

            $responseBody = $response->getBody();
            if (empty($responseBody)) {
                $this->thrownExceptionMessage = $e->getMessage();
                return;
            }

            // Reset pointer to the begging of string
            // Reference: https://github.com/8p/EightPointsGuzzleBundle/issues/48
            $responseBody->rewind();
            $this->thrownExceptionMessage = $responseBody->getContents();
        } else {
            $this->thrownExceptionMessage = $e->getMessage();
        }
    }

    /**
     * Returns the error (last exception) summary.
     *
     * @return array
     */
    public function getErrorSummary()
    {
        $e = $this->thrownException;
        
        if (! ($e instanceof RequestException)) {
            return [
                'error' => ($e instanceof Exception) ? $e->getMessage() : $e
            ];
        }

        $response = $e->getResponse();
        $responseBody = Str::jsonDecode($this->thrownExceptionMessage);

        if (is_array($responseBody)) {
            $summary['body']    = $responseBody;
            $summary['error']   = $responseBody['error'] ?? '';
        } else {
            $summary['body']    = $responseBody;
            $summary['error']   = $responseBody ?? '';
        }

        $summary['message']     = $e->getMessage();
        $summary['status_code'] = $response ? $response->getStatusCode() : '';
        $summary['reason']      = $response ? $response->getReasonPhrase() : '';

        return $summary;
    }

    /**
     * Returns a pretty error response message.
     *
     * @return string
     */
    public function getErrorResponseMessage()
    {
        $summary = $this->getErrorSummary();
        if (!empty($summary['error'])) {
            return $summary['error'];
        }
        return 'Internal server error.';
    }

    /**
     * Returns last error output.
     *
     * @return string
     */
    public function getLastErrorOutput()
    {
        $this->parseExceptionMessage();
        return $this->thrownExceptionMessage ?? '-';
    }

    /**
     * Returns error reason.
     *
     * @return string
     */
    public function getErrorReason()
    {
        $summary = $this->getErrorSummary();
        return $summary['reason'] ?? null;
    }

    /**
     * Returns error message.
     *
     * @return string
     */
    public function getErrorMessage()
    {
        $summary = $this->getErrorSummary();
        return $summary['message'] ?? null;
    }

    /**
     * Returns error status code.
     *
     * @return string
     */
    public function getErrorStatusCode()
    {
        $summary = $this->getErrorSummary();
        return $summary['status_code'] ?? null;
    }

    /**
     * Sets the request timeout.
     *
     * @param   integer
     *
     * @return  void
     */
    public function setTimeout($timeout)
    {
        if (! is_int($timeout) || $timeout <= 0) {
            $this->exception("Invalid timeout.");
        }

        $this->requestTimeout = $timeout;
    }

    /**
     * Returns the request timeout.
     *
     * @return  integer
     */
    public function getTimeout()
    {
        return $this->requestTimeout;
    }

    /**
     * Sets the default error level.
     *
     * @param   integer
     *
     * @return  void
     */
    public function setErrorLevel($level)
    {
        if (! is_int($level)) {
            $this->exception("Invalid error level.");
        }

        $this->errorLevel = $level;
    }

    /**
     * Returns the default error level.
     *
     * @return  integer
     */
    public function getErrorLevel()
    {
        return $this->errorLevel;
    }

    /**
     * Issues an http request.
     *
     * @param   string  $uri
     * @param   string  $method
     * @param   array   $params
     *
     * @return  mixed
     */
    protected function makeRequest($uri, $method, $params = [])
    {
        if (! in_array($method, [ 'post', 'get', 'delete', 'put', 'patch' ])) {
            $this->exception("Method '$method' is not supported.");
        }

        $startTime  = time();

        try {
            try {
                // Request data
                $params['timeout'] = $params['timeout'] ?? $this->requestTimeout;

                // Send request
                $this->debug(sprintf('[%s %s] Sending request...', strtoupper($method), $uri), $params);
                $response = $this->client->request($method, $uri, $params);

                // Save response
                $this->latestResponse = $response;

                // Try to get body contents
                $body = ! empty($response) ? $response->getBody() : null;
                $content = ! empty($body) ? $body->getContents() : null;

                // Try to parse response in JSON
                if (! empty($content) && Str::isJson($content)) {
                    $content = Str::jsonDecode($content);
                }

                $this->debug(sprintf('[%s %s] Request sent.', strtoupper($method), $uri), [
                    'status_code' => $response->getStatusCode(),
                    'reason' => $response->getReasonPhrase(),
                    'response' => $content
                ]);
            } catch (ClientException $e) { // 4xx codes
                throw $e;
            } catch (ServerException $e) { // 5xx codes
                throw $e;
            }
        } catch (TransferException $e) {
            // Save the thrown exception
            $this->thrownException = $e;

            // Parse exception and get message
            $message = $this->getLastErrorOutput();

            // Write to logger
            $this->logError(sprintf('[%s %s] HTTP client error: %s', strtoupper($method), $uri, Str::limit($message)));

            // Check whether to log failure to database
            if (!$this->suppressFailureLog) {
                $this->recordFailedRequest("($method) $uri", $params, $message, time() - $startTime);
            }

            // Just in case the exception is suppressed
            if ($this->suppressException) {
                return false;
            }

            throw $e;
        }

        if (!$this->suppressSuccessLog) {
            $this->recordRequest("($method) $uri", $params, $content, time() - $startTime);
        }

        return $content;
    }

    /**
     * Make get API request
     *
     * @param   string      $uri
     * @param   array       $params
     *
     * @return  array
     *
     * @throws \Exception
     */
    protected function makeGetApiRequest($uri, $params = [])
    {
        $data = [
            'query' => $params
        ];
        
        return $this->makeRequest($uri, 'get', $data);
    }
    
    /**
     * Make get DELETE request
     *
     * @param   string      $uri
     * @param   array       $params
     *
     * @return  array
     *
     * @throws \Exception
     */
    protected function makeDeleteApiRequest($uri, $params = [])
    {
        return $this->makeRequest($uri, 'delete', [
            'json' => $params,
            'timeout' => $params['timeout'] ?? null
        ]);
    }

    /**
     * Make post API request
     *
     * @param   string      $uri
     * @param   array       $params
     * @param   string      $type
     *
     * @return  array
     *
     * @throws \Exception
     */
    protected function makePostApiRequest($uri, $params = [], $type = 'json', $method = 'post')
    {
        if (! in_array($type, [ 'json', 'form_params' ])) {
            $this->exception("Invalid request type '$type'");
        }

        $data = [];
        if (! empty($type)) {
            $data[$type] = $params;
        }
        $data['timeout'] = $params['timeout'] ?? null;

        return $this->makeRequest($uri, $method, $data);
    }

    /**
     * Make put API request
     * 
     * @param   string      $uri
     * @param   array       $params
     * @param   string      $type
     * 
     * @return  array
     */
    protected function makePutApiRequest($uri, $params = [], $type = 'json')
    {
        return $this->makePostApiRequest($uri, $params, $type, 'put');
    }

    /**
     * Make patch API request
     * 
     * @param   string      $uri
     * @param   array       $params
     * @param   string      $type
     * 
     * @return  array
     */
    protected function makePatchApiRequest($uri, $params = [], $type = 'json')
    {
        return $this->makePostApiRequest($uri, $params, $type, 'patch');
    }

    /**
     * Records an app client activity.
     *
     * @param string    $url        The URL.
     * @param array     $params     The request params.
     * @param array     $response   The response.
     * @param string    $duration   The duration in seconds. Optional.
     *
     * @return ActivityLog          The created activity log instance.
     */
    private function recordRequest($url, $params, $response = null, $duration = null)
    {
        if (!$this->logSuccess) {
            return;
        }
        
        $now = time();

        return ActivityLogger::record(
            $now, // scheduled at
            $now, // started at
            $this->name,
            $params,
            $response,
            self::ACTIVITY_TYPE,
            $this->priority,
            [], // logs
            $duration,
            $url
        );
    }

    /**
     * Records a failed app client activity to the database.
     *
     * @param string    $url        The URL.
     * @param array     $params     The request params.
     * @param string    $error      The activity error.
     * @param string    $duration   The duration in seconds. Optional.
     *
     * @return ActivityLog          The created activity log instance.
     */
    private function recordFailedRequest($url, $params, $error, $duration = null)
    {
        if (!$this->logFailure) {
            return;
        }

        $now = time();
        
        return ActivityLogger::recordFailure(
            $now, // scheduled at
            $now, // started at
            $this->name,
            $params,
            $error,
            self::ACTIVITY_TYPE,
            $this->priority,
            [], // logs
            $duration,
            null, // output
            $url, // destination
            null, // guid
            $this->errorLevel
        );
    }

    /**
     * Create and configure an http client in order to send API requests.
     *
     * @param   string $token   The header authentication token. Optional.
     *
     * @return  GuzzleHttp\Client
     */
    private function makeHttpClient($token)
    {
        if (empty($token)) {
            return new Client();
        }

        $client = new Client([
            'defaults' => [
                "headers" => [
                    'Authorization' => $token
                ]
            ]
        ]);

        return $client;
    }
}
