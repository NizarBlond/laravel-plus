<?php

namespace NizarBlond\LaravelPlus\Abstracts;

use NizarBlond\LaravelPlus\Abstracts\JobBase;
use NizarBlond\LaravelPlus\Traits\FractalHelpers;
use NizarBlond\LaravelPlus\Traits\InstanceHelpers as LaravelPlusHelpers;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use NizarBlond\LaravelPlus\Support\Req;
use NizarBlond\LaravelPlus\Support\Que;
use NizarBlond\LaravelPlus\Support\Str;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Auth;
use Exception;

abstract class ControllerBase extends Controller
{
    use FractalHelpers, LaravelPlusHelpers;

    /**
     * The response factory object.
     *
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    protected $responseFactory = null;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (class_exists(\League\Fractal\Manager::class)) {
            $this->initFractalManager();
            $this->registerDefaultModelTransformers();
        }
        $this->responseFactory = response();
    }

    /**
     * Validates request using Laravel validator and responds with
     * a custom error message.
     *
     * @param  \Illuminate\Http\Request     $request
     * @param  array                        $rules
     *
     * @return \Illuminate\Http\Response
     */
    protected function validateRequest($request, $rules)
    {
        if (! Req::validRequest($request, $rules, $validator)) {
            throw new ValidationException($validator);
        }
    }

    /**
     * Responds with item while handling the transformation internally.
     *
     * @param  array $items
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithItems($items)
    {
        $data = $this->createCollectionFractalData($items);
        return $this->respondWithJson($data);
    }

    /**
     * Responds with item while handling the transformation internally.
     *
     * @param  mixed    $item
     * @param  boolean  $transform
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithItem($item, $transform = true)
    {
        if (! $transform) {
            return $this->respondWithJson($item);
        }

        if ($item instanceof EloquentCollection) {
            return $this->respondWithItems($item);
        }

        $resource = $this->dataToFractalResource($item);
        if (! empty($resource)) {
            $data = $this->createFractalData($resource);
            return $this->respondWithJson($data);
        }

        return $this->respondWithError("Failed to transform item.");
    }

    /**
     * Responds with item while handling the transformation internally.
     *
     * @param  array $items
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithDeepItems($items)
    {
        $transformed = [];
        foreach ($items as $key => $itemData) {
            if (is_array($itemData) || $itemData instanceof EloquentCollection) {
                $transformed[$key] = $this->createCollectionFractalData($itemData);
            } else {
                $resource = $this->dataToFractalResource($itemData);
                $transformed[$key] = !empty($resource) ? $this->createFractalData($resource) : $itemData;
            }
        }
        return $this->respondWithJson($transformed);
    }

    /**
     * Responds with file download.
     *
     * @param  string   $filename
     * @param  string   $content
     * @param  array    $headers
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithFileDownload($filename, $content, $headers = [])
    {
        if (empty($headers)) {
            $headers = [
                'Content-Type' => 'application/octet-stream',
                'Content-Disposition' => 'attachment; filename='.$filename
            ];
        }
        return $this->responseFactory->make($content, 200, $headers);
    }

    /**
     * Responds with file streaming.
     *
     * @param  string   $filepath
     * @param  string   $filename
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithFileStreaming($filepath, $fliename)
    {
        // https://medium.com/@barryvdh/streaming-large-csv-files-with-laravel-chunked-queries-4158e484a5a2#.z9aey685l
        $response = new StreamedResponse(
            function () use ($filepath, $fliename) {
                // Open output stream
                if ($file = fopen($filepath, 'rb')) {
                    while (!feof($file) and (connection_status() == 0)) {
                        print(fread($file, 1024 * 8));
                        flush();
                    }
                    fclose($file);
                }
            },
            200,
            [
                'Content-Type' => 'application/octet-stream',
                'Content-Disposition' => 'attachment; filename="' . $fliename . '"',
            ]
        );

        return $response;
    }

    /**
     * Responds with json.
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithOk()
    {
        return $this->respondWithJson([]);
    }

    /**
     * Responds with json.
     *
     * @param  mixed $object
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithJson($object = [])
    {
        try {
            return $this->responseFactory->json($object);
        } catch (Exception $e) {
            if (Str::contains($e->getMessage(), 'Malformed UTF-8 characters')) {
                $objectAsUtf8 = Str::convertToUtf8($object);
                return $this->responseFactory->json($objectAsUtf8);
            }
            throw $e;
        }
    }

    /**
     * Responds with error.
     *
     * @param  mixed $object
     * @param  int $status
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithError($message = '', $status = 400)
    {
        return $this->responseFactory->json([ 'error' => $message ], $status);
    }

    /**
     * Responds with unauthorized (401) error.
     *
     * @return \Illuminate\Http\Response
     */
    protected function respondWithUnauthorized()
    {
        return $this->respondWithError('Unauthorized', 401);
    }

    /**
     * Returns the authentication guard.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

    /**
     * Returns the current logged in user.
     *
     * @return \App\Models\User
     */
    public function user()
    {
        return $this->guard()->user();
    }

    /**
     * Runs a sync job and handles error if any.
     *
     * @param JobBase $job
     *
     * @return \Illuminate\Http\Response
     */
    protected function runJob(JobBase $job)
    {
        return $job->runNow();
    }

    /**
     * Enqueues a job.
     *
     * @param JobBase $job
     *
     * @return \Illuminate\Http\Response
     */
    protected function enqueueJob(JobBase $job)
    {
        return Que::dispatch($job);
    }
}
