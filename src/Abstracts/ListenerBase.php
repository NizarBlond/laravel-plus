<?php

namespace NizarBlond\LaravelPlus\Abstracts;

use NizarBlond\LaravelPlus\Traits\InstanceHelpers;
use NizarBlond\LaravelPlus\Services\ActivityLogger;
use NizarBlond\LaravelPlus\Constants\ErrorLevel;
use NizarBlond\LaravelPlus\Constants\RecordPriority;
use Exception;

abstract class ListenerBase
{
    use InstanceHelpers;

    /**
     * The listener's event.
     *
     * @var Event
     */
    protected $event;

    /**
     * The listener's event type.
     *
     * @var string
     */
    protected $eventType;

    /**
     * The start time in unix timestamp
     *
     * @var integer
     */
    protected $startedAt;

    /**
     * The schedule time in unix timestamp
     *
     * @var integer
     */
    protected $scheduledAt;

    /**
     * The listener name.
     *
     * @var string
     */
    protected $name;

    /**
     * Whether to return false instead of throwing exception.
     *
     * @var boolean
     */
    protected $suppressException = false;

    /**
     * The default activity error level.
     *
     * @var \NizarBlond\LaravelPlus\Constants\ErrorLevel
     */
    protected $errorLevel = ErrorLevel::NORMAL;

    /**
     * The default record priority.
     *
     * @var \NizarBlond\LaravelPlus\Constants\RecordPriority
     */
    protected $priority = RecordPriority::NORMAL;

    /**
     * Indicates if the listener success should be logged.
     *
     * @var bool
     */
    public $logSuccess = true;
    
    /**
     * Indicates if the listener failure should be logged.
     *
     * @var bool
     */
    public $logFailure = true;
    
    /**
     * The log activity type.
     *
     * @var string
     */
    const ACTIVITY_TYPE = 'listener';
    
    /**
     * Constructor.
     *
     * @param   array     $config
     * @param   array     $extraLog
     *
     * @return  void
     */
    public function __construct()
    {
        $this->name         = $this->getClassName(2);
        $this->scheduledAt  = time();
    }

    /**
     * Initializes the listener right before handling.
     *
     * @param \App\Events\Event
     *
     * @return void
     */
    protected function init($event)
    {
        //
    }

    /**
     * Execute the listener.
     *
     * @param \App\Events\Event
     *
     * @return bool
     */
    public function handle($event)
    {
        // Do not include this in init just in case the user
        // forget to call parent::init()
        $this->event = $event;
        $this->startedAt = time();

        // Initializes the listener
        $this->init($event);

        if ($this->ignore($event)) {
            $this->logSuccess = false;
            // If listener is ignored then returning true (not false) ensures
            // that the listener will not halt the listeners chain.
            return true;
        }

        $logMessage = sprintf(
            "Starting '%s' listener at %s",
            $this->name,
            gmdate('Y-m-d H:i:s', $this->startedAt)
        );
        $this->log($logMessage);

        if (!empty($this->eventType) && get_class($event) !== $this->eventType) {
            self::exception("Invalid event class type.");
        }

        try {
            $response = $this->listen($event);
            $this->onSuccess();
            if ($this->alwaysHaltOnSuccess()) {
                // If response is false, regardless whether it is a halting event
                // or not, the listeners chain will stop.
                return false;
            }
            return $response;
        } catch (Exception $e) {
            $this->onFailure($e);
            if ($this->suppressException) {
                return false;
            }
            throw $e;
        }
    }

    /**
     * Whether to ignore listener.
     *
     * @param \App\Events\Event
     *
     * @return bool
     */
    protected function ignore($event)
    {
        return false;
    }

    /**
     * Whether to halt after success regardless of the response.
     * For more info: illuminate/events/Dispatcher.php
     *
     * References:
     * https://stackoverflow.com/questions/26244817/trouble-with-multiple-model-observers-in-laravel
     *
     * @param \App\Events\Event
     *
     * @return bool
     */
    protected function alwaysHaltOnSuccess()
    {
        return false;
    }

    /**
     * Executes the listener's logic.
     *
     * @return void
     */
    abstract protected function listen($event);

    /**
     * Handle the listener's success event internally
     *
     * @return void
     */
    protected function onSuccess()
    {
        // Get execution time in seconds
        $finishedAt = time();
        $duration = $finishedAt - $this->startedAt;
        
        // Print to log file
        $logMessage = sprintf(
            "Listener '%s' successfully finished at %s",
            $this->name,
            gmdate('Y-m-d H:i:s', $finishedAt)
        );
        $this->log($logMessage);

        // Log successful job activity
        $this->recordActivity($duration);
    }

    /**
     * Handle the listener's failure event internally
     *
     * @param Exception $e
     * @return void
     */
    protected function onFailure($e)
    {
        // Get execution time in seconds
        $finishedAt = time();
        $duration = $finishedAt - $this->startedAt;
        
        // Print to log file
        $logMessage = sprintf(
            "Listener '%s' failed at %s",
            $this->name,
            gmdate('Y-m-d H:i:s', $finishedAt)
        );
        $this->log($logMessage);

        // Log failed job activity
        $this->recordFailedActivity($e, $duration);
    }

    /**
     * Records activity.
     *
     * @param   string      $duration
     * @return  ActivityLog
     */
    protected function recordActivity($duration = null)
    {
        if (!$this->logSuccess) {
            return;
        }

        return ActivityLogger::record(
            $this->scheduledAt,
            $this->startedAt,
            $this->name,
            [ 'event_type' => get_class($this->event) ],
            null, // output
            self::ACTIVITY_TYPE,
            $this->priority,
            $this->getLogData(),
            $duration
        );
    }
    
    /**
     * Records a failed activity to the database.
     *
     * @param Exception     $error      The activity exception.
     * @param string        $duration   The duration in seconds. Optional.
     * @param int           $level      The error level. Optional.
     * @return ActivityLog              The created activity log instance.
     */
    protected function recordFailedActivity($e, $duration = null, $level = null)
    {
        if (!$this->logFailure) {
            return;
        }

        return ActivityLogger::recordFailure(
            $this->scheduledAt,
            $this->startedAt,
            $this->name,
            [ 'event_type' => get_class($this->event) ],
            $e,
            self::ACTIVITY_TYPE,
            $this->priority,
            $this->getLogData(),
            $duration,
            null, // output
            null, // destination
            null, // guid
            !empty($level) ? $level : $this->errorLevel
        );
    }
}
