<?php

namespace NizarBlond\LaravelPlus\Abstracts;

use App\Jobs\Job;
use NizarBlond\LaravelPlus\Traits\InstanceHelpers;
use NizarBlond\LaravelPlus\Support\Que;
use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Support\Time;
use NizarBlond\LaravelPlus\Support\File;
use NizarBlond\LaravelPlus\Support\Dir;
use NizarBlond\LaravelPlus\Services\ActivityLogger;
use NizarBlond\LaravelPlus\Constants\ErrorLevel;
use NizarBlond\LaravelPlus\Constants\RecordPriority;
use NizarBlond\LaravelPlus\Models\JobLog;
use NizarBlond\LaravelPlus\Models\ActivityLog;
use NizarBlond\LaravelPlus\Config\LaravelPlus as LPConfig;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Exception;

abstract class JobBase extends Job implements ShouldQueue
{
    use InstanceHelpers;

    /**
     * The job log instance.
     *
     * @var JobLog
     */
    private $log;

    /**
     * Enable job logging.
     * 
     * @var bool
     */
    protected $loggerEnabled = false;

    /**
     * The job start time in unix timestamp
     *
     * @var integer
     */
    public $startedAt;

    /**
     * The job schedule time in unix timestamp
     *
     * @var integer
     */
    public $scheduledAt;

    /**
     * Indicates if the job success should be logged.
     *
     * @var bool
     */
    public $logSuccess = true;
    
    /**
     * Indicates if the job failure should be logged.
     *
     * @var bool
     */
    public $logFailure = true;

    /**
     * The job's configuration array
     *
     * @var array
     */
    protected $config = [];

    /**
     * The job's guid.
     *
     * @var string
     */
    protected $guid;
    
    /**
     * The job name.
     *
     * @var string
     */
    protected $name;

    /**
     * The job output.
     *
     * @var mixed
     */
    protected $output;
    
    /**
     * The list of required config.
     *
     * @var array
     */
    protected $requiredConfigs = [];

    /**
     * The file system directory that if set, the job will write output (serialized) and errors
     * to in a file named after the job's guid.
     * 
     * Example: If set to /tmp then the job will write to /tmp/{guid}.out or /tmp/{guid}.err.
     * 
     * This is useful for jobs that are run in the background and you want to check their output
     * later on without having to check the database.
     * 
     * @var string
     */
    protected $outputDirectory = null;

    /**
     * The default activity error level.
     *
     * @var \NizarBlond\LaravelPlus\Constants\ErrorLevel
     */
    protected $errorLevel = ErrorLevel::NORMAL;

    /**
     * The default record priority.
     *
     * @var \NizarBlond\LaravelPlus\Constants\RecordPriority
     */
    protected $priority = RecordPriority::NORMAL;

    /**
     * Whether to return false instead of throwing exception.
     *
     * @var boolean
     */
    private $suppressException = false;

    /**
     * The log activity type.
     *
     * @var string
     */
    const ACTIVITY_TYPE = 'job';

    /**
     * Do not actually perform the action if enabled.
     *
     * @var bool
     */
    protected $dryRun = false;

    /**
     * Create a new job instance.
     *
     * @param array     $config
     * @return void
     */
    public function __construct($config = [])
    {
        // Check if the required keys are provided
        foreach ($this->requiredConfigs as $key) {
            if (! array_key_exists($key, $config)) {
                $this->exception("Required key '$key' is missing.");
            }
        }

        // Set job fields
        $this->loggerEnabled    = $config['logger'] ?? $this->loggerEnabled;
        $this->debugMode        = $config['debug'] ?? false;
        $this->dryRun           = $config['dry-run'] ?? $config['dry_run'] ?? false;
        $this->outputDirectory  = $config['output_directory'] ?? $this->outputDirectory;
        $this->name             = $this->getClassName(2);
        $this->guid             = Str::guid();
        $this->config           = array_merge($this->config, $config);
        $this->scheduledAt      = time();

        // Log jobs to file system
        $this->fsRuntimeLog = Str::joinPaths(storage_path(), 'logs', 'jobs.log');
    }

    /**
     * Execute the job.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!empty($this->fsRuntimeLog)) {
            $this->writeFSRuntimeLog(true);
        }

        $this->startedAt = time();

        $logMessage = sprintf(
            "Starting '%s' job (ID='%s') at %s",
            $this->name,
            $this->guid,
            gmdate('Y-m-d H:i:s', $this->startedAt)
        );
        self::log($logMessage, [
            'config' => $this->config,
            'stack'  => $this->getCallingMethod()
        ]);

        try {
            // Execute the job's validation
            $this->validate();
            // Execute the job's initializer
            $this->init();
            // Validate after init
            $this->validateAfterInit();
            // Set logger status to running
            $this->logInfoToDB('Job is running.');
            // Execute the job's logic
            $this->output = $this->execute();
            // Run on complete callback
            $this->onComplete(null);
            // Run on success callback
            $this->onSuccess();
            // Return the job output
            return $this->output;
        } catch (Exception $e) {
            // Run on complete callback
            $this->onComplete($e);
            // Run on failure callback
            return $this->onFailure($e, !$this->suppressException);
        }
    }

    /**
     * Suppresses throwing exception.
     *
     * @return  void
     */
    public function suppressException()
    {
        $this->suppressException = true;
    }

    /**
     * Removes exception throwing suppression.
     *
     * @return void
     */
    public function unsuppressException()
    {
        $this->suppressException = false;
    }

    /**
     * Runs a the current job without queue-ing it.
     *
     * @return  mixed
     */
    public function runNow()
    {
        return $this->handle();
    }

    /**
     * Queues the current job.
     *
     * @param   string|null  $queue
     *
     * @return  mixed
     */
    public function dispatch($queue = null)
    {
        return Que::dispatch($this, $queue);
    }

    /**
     * Queues the current job and waits for the result in a busy-wait loop.
     *
     * This function serves as a workaround for `runNow()` when the job cannot be run 
     * directly by the HTTP server's user (e.g., `nginx`), due to permission restrictions.
     * 
     * For instance, if the `nginx` user lacks the required permissions to execute the job, 
     * the job will be queued and executed by a user with higher privileges (e.g., `root`), 
     * while the function waits for the result to complete.
     * 
     * This ensures that the job is completed even if the HTTP server user does not 
     * have direct execution permissions.
     *
     * @param string|null $queue
     * @param int $timeout
     * @param int $waitInterval
     * @return  mixed
     */
    public function dispatchAndWait($queue = null, int $timeout = 90, int $waitInterval = 1)
    {
        // Set the directive to wait for the result.
        $this->outputDirectory = Str::joinPaths(sys_get_temp_dir(), 'jobs'); 

        // Dispatches the job to queue
        $this->dispatch($queue);

        // Wait for the result
        $timeoutAt = Time::now()->addSeconds($timeout);
        while (true) {
            // Check if the job is completed
            if (File::exists($this->getOutputFilePath())) {
                return unserialize(File::read($this->getOutputFilePath()));
            } elseif (File::exists($this->getErrorFilePath())) {
                $this->exception("Job failed with error: " . File::read($this->getErrorFilePath()));
            }
            // Sleep before checking again
            sleep($waitInterval);
            // Check if the timeout is reached
            if ($timeoutAt < Time::now()) {
                $this->exception("Max timeout reached for obtaining result of {$this->name} job {$this->guid}.");
            }
        }
    }

    /**
     * Returns job name.
     *
     * @return  string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns job data.
     *
     * @return  array
     */
    public function getData()
    {
        return $this->config;
    }

    /**
     * Returns job guid.
     *
     * @return  string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Returns job output.
     *
     * @return  mixed
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * Finds the job activity in the database.
     * 
     * @return  \NizarBlond\LaravelPlus\Models\ActivityLog
     */
    public function findActivity()
    {
        return ActivityLog::getByType('job')
            ->getByName($this->name)
            ->getByHostName(LPConfig::hostName())
            ->getByHostId(LPConfig::hostId())
            ->getByGuid($this->guid)
            ->first();
    }

    /**
     * Sets the default error level.
     *
     * @param   NizarBlond\LaravelPlus\Constants\ErrorLevel
     *
     * @return  void
     */
    protected function setErrorLevel($level)
    {
        if (! is_int($level)) {
            $this->exception("Invalid error level.");
        }

        $this->errorLevel = $level;
    }

    /**
     * Handle the job completion (success/error) event.
     *
     * @param   Exception|null
     *
     * @return  void
     */
    protected function onComplete($error = null)
    {
        if (!empty($this->fsRuntimeLog)) {
            $this->writeFSRuntimeLog(false, $error);
        }
    }

    /**
     * A callback to be executed when the job succeeds.
     *
     * @return void
     */
    protected function onSuccess()
    {
        // Write output to file system
        if ($this->outputDirectory) {
            File::write($this->getOutputFilePath(), serialize($this->output ?? ''));
        }

        // Log success to database
        $this->logInfoToDB('Job completed.', [], 'succeeded');

        // Get execution time in seconds
        $finishedAt = time();
        $duration = $finishedAt - $this->startedAt;

        // Log successful job activity
        $this->recordActivity($duration, $this->config, $this->output);
    }

    /**
     * A callback to be executed when the job fails.
     *
     * @param Exception $e
     * @param bool $throwException
     * @return bool
     * @throws Exception
     */
    protected function onFailure($e, $throwException = true)
    {
        // Write error to file system
        if ($this->outputDirectory) {
            File::write($this->getErrorFilePath(), $e->getMessage());
        }

        // Get execution time in seconds
        $finishedAt = time();
        $duration = $finishedAt - $this->startedAt;
        
        // Log error to database
        $this->logErrorToDB('Job failed.', [
            'trace' => $e->getTrace(),
        ], $e->getMessage());

        // Log failed job activity
        $this->recordFailedActivity($e, $duration, $this->config);

        // Whether to throw exception
        if ($throwException) {
            throw $e;
        }

        return false;
    }

    /**
     * Extends config data.
     *
     * @return void
     */
    protected function extendConfig($config)
    {
        if (! is_array($config)) {
            $this->exception("Invalid log data extension.");
        }

        $this->config = array_merge($this->config, $config);
    }

    /**
     * Executes the job.
     *
     * @return void
     */
    protected function execute()
    {
        //
    }

    /**
     * Validates the job before execution.
     *
     * @return void
     */
    protected function validate()
    {
        //
    }

    /**
     * Validates the job after init.
     *
     * @return void
     */
    protected function validateAfterInit()
    {
        //
    }

    /**
     * Initializes the job right before execution and after validation succeeded.
     *
     * @return void
     */
    protected function init()
    {
        if ($this->outputDirectory) {
            Dir::makeIfNotExists($this->outputDirectory);
        }
        $this->initLog();
    }

    /**
     * Initializes the job log instance if logging is enabled.
     * 
     * @return void
     */
    protected function initLog()
    {
        if ($this->loggerEnabled) {
            $this->log = new JobLog();
            $this->log->name = $this->getClassName();
            $this->log->guid = $this->guid;
            $this->log->timeline = [];
            $input = [];
            foreach ($this->config as $key => $value) {
                $input[$key] = is_object($value) ? get_class($value) . ' object' : $value;
            }
            $this->log->input = $input;
        }
    }

    /**
     * Logs a message to the job log.
     *
     * @param string $message
     * @param string $level
     * @param array $data
     * @param string|null $status
     * @return void
     */
    protected function logToDB(string $message, string $level, array $data = [], ?string $status = null, ?string $output = null, ?string $error = null)
    {
        if (!$this->loggerEnabled) {
            return;
        } elseif (!$this->log) {
            $this->exception('Job log instance is not initialized. Did you call parent::init() in your init method?');
        }
        if ($status) {
            $this->log->status = $status;
        }
        if ($output) {
            $this->log->output = $output;
        }
        if ($error) {
            $this->log->error = $error;
        }
        $this->log->addTimeline($message, $data, $level);
        $this->log->save();
    }

    /**
     * Logs an info message to the job log.
     *
     * @param string $message
     * @param array $data
     * @return void
     */
    protected function logInfoToDB(string $message, array $data = [], ?string $status = 'running', ?string $output = null)
    {
        $this->logToDB($message, 'info', $data, $status, $output);
    }

    /**
     * Logs an error message to the job log.
     *
     * @param string $message
     * @param array $data
     * @return void
     */
    protected function logErrorToDB(string $message, array $data = [], ?string $error = null)
    {
        $this->logToDB($message, 'error', $data, 'failed', null, $error);
    }

    /**
     * Logs a warning message to the job log.
     *
     * @param string $message
     * @param array $data
     * @return void
     */
    protected function logWarningToDB(string $message, array $data = [])
    {
        $this->logToDB($message, 'warning', $data);
    }

    /**
     * Records a job activity to the database.
     *
     * @param   string    $duration
     * @param   array     $args
     * @param   mixed     $output
     *
     * @return  \NizarBlond\LaravelPlus\Models\ActivityLog
     */
    protected function recordActivity(
        $duration,
        $args = [],
        $output = []
    ) {
        if (!$this->logSuccess) {
            return;
        }
        
        if ($output instanceof Model) {
            $output = $output->toArray();
        }

        if (!empty($output) && !is_array($output) && !is_string($output)) {
            $output = 'unable to parse output (json/string expected)';
        }

        return ActivityLogger::record(
            $this->scheduledAt,
            $this->startedAt,
            $this->name,
            $this->prepareArgsForLog(array_merge($args ?? [], $this->config)),
            $output,
            self::ACTIVITY_TYPE,
            $this->priority,
            $this->getLogData(),
            $duration,
            null,
            $this->guid
        );
    }

    /**
     * Records a warning to the database.
     *
     * @param   Exception   $e
     * @param   array       $args
     * @param   string      $output
     *
     * @return  \NizarBlond\LaravelPlus\Models\ActivityLog
     */
    protected function recordActivityWarning($e, $args = [], $output = '')
    {
        return $this->recordFailedActivity(
            $e,
            null, // duration
            $args,
            $output,
            ErrorLevel::WARNING
        );
    }
    
    /**
     * Records a failed job activity to the database.
     *
     * @param   Exception   $e
     * @param   string      $duration
     * @param   array       $args
     * @param   ErrorLevel  $level
     *
     * @return  \NizarBlond\LaravelPlus\Models\ActivityLog
     */
    protected function recordFailedActivity(
        $e,
        $duration = null,
        $args = [],
        $output = "",
        $level = null
    ) {
        if (!$this->logFailure) {
            return;
        }

        return ActivityLogger::recordFailure(
            $this->scheduledAt,
            $this->startedAt,
            $this->name,
            $this->prepareArgsForLog(array_merge($args ?? [], $this->config)),
            $e,
            self::ACTIVITY_TYPE,
            $this->priority,
            $this->getLogData(),
            $duration,
            $output,
            null, // destination
            $this->guid,
            !empty($level) ? $level : $this->errorLevel
        );
    }

    /**
     * Prepares arguments for log.
     *
     * @param array $args
     * @return array
     */
    protected function prepareArgsForLog($args)
    {
        if (empty($args) || ! is_array($args)) {
            return $args;
        }

        return array_map(function ($item) {
            if ($item instanceof Model) {
                return $item->toArray();
            }
            return $item;
        }, $args);
    }

    /**
     * Kills the job without sending log to database.
     *
     * @param string $message
     * @return void
     */
    protected function killWithoutLog(string $message)
    {
        $this->logWarning("Killing the job {$this->name} without logging the failure to DB: '$message'.");
        $this->logFailure = false;
        return $this->exception($message);
    }

    /**
     * Tells whether the job is running in a context of artisan command.
     *
     * @return bool
     */
    protected function isArtisan()
    {
        return ($_SERVER['argv'][0] ?? '') === 'artisan';
    }

    /**
     * Prints the proper operation verb based on dry-run option.
     *
     * @param bool $pluralize
     * @return string
     */
    protected function actionVerb(bool $pluralize = false)
    {
        return $this->dryRun ? 'will be' : ($pluralize ? 'were' : 'was');
    }

    /**
     * Gets the output file path.
     * 
     * @return string
     */
    private function getOutputFilePath()
    {
        return Str::joinPaths($this->outputDirectory, $this->guid . '.out');
    }

    /**
     * Gets the error file path.
     * 
     * @return string
     */
    private function getErrorFilePath()
    {
        return Str::joinPaths($this->outputDirectory, $this->guid . '.err');
    }
}
