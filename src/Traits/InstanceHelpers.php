<?php

namespace NizarBlond\LaravelPlus\Traits;

use NizarBlond\LaravelPlus\Support\Log;
use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Support\Dir;
use NizarBlond\LaravelPlus\Support\Proc;
use NizarBlond\LaravelPlus\Support\Time;
use NizarBlond\LaravelPlus\Support\File;
use NizarBlond\LaravelPlus\Support\Arr;
use NizarBlond\LaravelPlus\Config\ActivityLogger as ALConfig;
use NizarBlond\LaravelPlus\Config\Log as LogConfig;
use NizarBlond\LaravelPlus\Exceptions\Exception;

trait InstanceHelpers
{
    /**
     * The minimum level of log to be written.
     *
     * Possible values: 'debug', 'info', 'warning', 'error', 'critical'.
     *
     * @var string|null
     */
    public $minLoggerLevel = null;

    /**
     * The runtime log file on filesystem.
     * 
     * @var string
     */
    protected $fsRuntimeLog = false;

    /**
     * The debug mode. Possible values: true, false, 'verbose', 'normal'.
     *
     * @var mixed
     */
    protected $debugMode = false;

    /**
     * Whether to write logs in debug mode.
     *
     * @var bool
     */
    protected $debugLogs = true;

    /**
     * Writes runtime log.
     *
     * @param bool $start
     * @param Exception|null $e
     * @return void
     */
    protected function writeFSRuntimeLog(bool $start, $e = null)
    {
        if (!File::exists($this->fsRuntimeLog)) {
            Dir::makeIfNotExists(dirname($this->fsRuntimeLog));
            File::touch($this->fsRuntimeLog, 0777);
        }

        if ($start) {
            $status = 'Processing';
        } elseif ($e) {
            $message = Str::replaceNewlines($e->getMessage());
            $status = sprintf('Failed: "%s"', $message);
        } else {
            $status = 'Succeeded';
        }

        $line = sprintf(
            '[%s][%s][%s][%s][%s]: %s',
            gmdate('Y-m-d H:i:s', time()),
            Proc::currentUser(),
            Proc::currentPID(),
            $this->getClassName(3),
            $this->guid,
            $status
        );

        File::append($this->fsRuntimeLog, $line . PHP_EOL);
    }

    /**
     * Parses runtime log.
     *
     * @param string|null $file
     * @param int $limit
     * @return array
     */
    protected function parseFSRuntimeLog(?string $file = null, int $limit = -1)
    {
        // Get lines from file.
        $file = $file ?? $this->fsRuntimeLog;
        $lines = $limit > 0 ? File::slice($file, 0, $limit, true) : File::toArray($file);
        if (empty($lines)) {
            return [];
        }
        // Parse lines.
        $processToLogs = [];
        $regex = '/^\[(?P<date>[\d\-\s:]+)\]\[(?P<user>.*?)\]\[(?P<pid>\d+)\]\[(?P<class>.*?)\]\[(?P<guid>.*?)\]: (?P<status>(Failed|Succeeded|Processing))(:?\s"?(?P<msg>.*?)"?)?$/';
        foreach ($lines as $line) {
            if (!preg_match($regex, $line, $matches)) {
                continue; // invalid format (e.g. logs with multiple lines).
            }
            $log = $matches;
            $log['date'] = Time::create($log['date']);
            $log['key'] = sprintf('%s:%s:%s:%s', $log['user'], $log['pid'], $log['class'], $log['guid']);
            $processToLogs[$matches['guid']][] = Arr::forgetNumericKeys($log);
        }
        return $processToLogs;
    }

    /**
     * Retrieves the log data shared by the request.
     * 
     * @return array
     */
    public function getLogData()
    {
        return Log::getLogData();
    }

    /**
     * Adds log data shared by the request.
     * 
     * @param string $message
     * @param array $context
     * @param string $level
     * @return void
     */
    public function addLogData($message, $context = [], $level = 'n/a')
    {
        Log::addLogData($message, $context, $level);
    }

    /**
     * Logs the given message and context.
     * 
     * @param string $message
     * @param mixed $context
     * @param string $level
     * @return array
     */
    public function log($message, $context = [], string $level = "info")
    {
        $minLoggerLevel = $this->minLoggerLevel ?? LogConfig::getMinLoggerLevel();
        if (Log::compareLevels($minLoggerLevel, $level) < 0) {
            return false;
        }

        $context = $this->convertToArray($context);
        $context = $this->sanitizeArray($context);

        $this->addLogData($message, $context, $level);
        Log::log($message, $context, $level);

        if ($this->debugMode && $this->debugLogs) {
            $this->debug($message, $context, $level);
        }
    }

    /**
     * Logs the given message and context as info.
     * 
     * @param string $message
     * @param mixed $context
     * @return array
     */
    public function logInfo($message, $context = [])
    {
        $this->log($message, $context, 'info');
    }

    /**
     * Logs the given message and context as error.
     * 
     * @param string $message
     * @param mixed $context
     * @return array
     */
    public function logError($message, $context = [])
    {
        $this->log($message, $context, 'error');
    }

    /**
     * Logs the given message and context as warning.
     * 
     * @param string $message
     * @param mixed $context
     * @return array
     */
    public function logWarning($message, $context = [])
    {
        $this->log($message, $context, 'warning');
    }

    /**
     * Prints the given message and context.
     * 
     * @param string $message
     * @param mixed $context
     * @param string|null $level
     * @return array
     */
    public function print($message, $context = [], ?string $level = null)
    {
        Log::print($message, $context, $level);
    }

    /**
     * Prints a normal debug message and data.
     *
     * @param string $message
     * @param mixed $data
     * @param string $level
     * @return void
     */
    public function debug(string $message, $data = [], string $level = 'debug')
    {
        if ($this->debugMode) {
            $message = sprintf('[%s] %s', Time::now()->toDateTimeString(), $message);
            $this->print($message, $data, $level);
        }
    }

    /**
     * Prints a verbose debug message and data.
     * 
     * @param string $message
     * @param mixed $data
     * @param string $level
     * @return void
     */
    public function debugVerbose(string $message, $data = [])
    {
        if ($this->debugMode === 'verbose') {
            return $this->debug($message, $data, 'verbose');
        }
    }

    /**
     * Sanitize the given value.
     * 
     * @param mixed $value
     * @return mixed
     */
    public function sanitizeArray($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        // Remove present keys (see 'keys_to_remove' in config file)
        $value = Arr::removeKeysRecursive(
            $value,
            ALConfig::keysToRemove()
        );

        // Hide present keys (see 'keys_to_hide' in config file)
        $value = Arr::modifyKeyValuesRecursive(
            $value,
            ALConfig::keysToHide(),
            function ($value) {
                return '[key-hidden-from-log]';
            }
        );

        // Remove content with non-UTF-8 encoding
        $encoding = 'UTF-8';
        $replace = '[removed-from-log:bad-encoding]';
        $value = Arr::walkRecursive($value, function ($v, $isKey) use ($encoding, $replace) {
            if ($isKey || !is_string($v)) {
                return $v;
            }
            if (!mb_detect_encoding($v, $encoding, true)) {
                return $replace;
            }
            return $v;
        });

        return $value;
    }

    /**
     * Throws an exception with the given message and data.
     * 
     * @param string $message
     * @param mixed $data
     * @param bool $log
     * @return array
     */
    public function exception($message, $data = null, bool $log = true)
    {
        if ($log) {
            $this->logError($message, $data);
        }
        throw new Exception($message, $data);
    }

    /**
     * Throws an exception with the given message and data without logging.
     * 
     * @param string $message
     * @param mixed $data
     * @return array
     */
    public function exceptionWithoutLog($message, $data = null)
    {
        return $this->exception($message, $data, false);
    }

    /**
     * Retrieves the class name.
     * 
     * @param int $lastNParts
     * @param mixed $instanceThis
     * @return string
     */
    public function getClassName($lastNParts = null, $instanceThis = null)
    {
        // Get full name with namespace
        $fullname = get_class(!empty($instanceThis) ? $instanceThis : $this);

        // If last N parts is null then return full name
        if (is_null($lastNParts)) {
            return $fullname;
        }

        // Get namespace parts
        $nsParts = explode("\\", $fullname);

        // Slice parts
        $lastNParts = array_slice($nsParts, -$lastNParts);

        // Concat last N parts
        return implode("\\", $lastNParts);
    }

    /**
     * Retrieves the calling method.
     * 
     * @param int $index
     * @param int $limit
     * @param array $stack
     * @return array
     */
    public function getCallingMethod($index = null, $limit = 25, &$stack = null)
    {
        $stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $limit);
        if (empty($index)) {
            return $stack;
        }
        return $stack[$index] ?? null;
    }

    /**
     * Guesses whether the app is running in a testing environment.
     *
     * @return array
     */
    public function isTestingOrLocal()
    {
        return in_array(env('APP_ENV', 'local'), [ 'local', 'test', 'testing', 'dev', 'development' ]);
    }

    /**
     * Retrieves the current time.
     *
     * @param string $to
     * @return array
     */
    public function now($to = 'datetime')
    {
        if ($to === 'datetime') {
            return Time::now()->toDateTimeString();
        }
        return time();
    }

    /**
     * Converts the given context to an array.
     *
     * @param mixed $context
     * @return array
     */
    private function convertToArray($context)
    {
        if (empty($context)) {
            return [];
        } elseif (is_array($context)) {
            return $context;
        } elseif (is_object($context)) {
            return (array) $context;
        }

        return [ 'context' => $context, 'type' => gettype($context) ];
    }
}
