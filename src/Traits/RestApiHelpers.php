<?php

namespace NizarBlond\LaravelPlus\Traits;

use NizarBlond\LaravelPlus\Traits\InstanceHelpers;
use NizarBlond\LaravelPlus\Exceptions\Exception as LPException;
use NizarBlond\LaravelPlus\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use GuzzleHttp\Exception\RequestException;
use Aws\Exception\AwsException;
use Throwable;

trait RestApiHelpers
{
    use InstanceHelpers;

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param \Illuminate\Http\Request $request
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForException(Request $request, Throwable $e)
    {
        switch (true) {
            case $this->isAwsException($e):
                $retval = $this->awsExceptionResponse($e);
                break;
            case $this->isValidationException($e):
                $retval = $this->validationExceptionResponse($e);
                break;
            case $this->isRequestException($e):
                $retval = $this->requestExceptionResponse($e);
                break;
            case $this->isLaravelPlusException($e):
                $retval = $this->laravelPlusExceptionResponse($e);
                break;
            default:
                $retval = $this->generalExceptionResponse($e);
                break;
        }

        return $retval;
    }

    /**
     * Determines if request is an API call.
     *
     * @param \Illuminate\Http\Request $request
     * @return boolean
     */
    protected function isApiRequest(Request $request)
    {
        return strpos($request->getUri(), env('API_PREFIX', '/api/v')) !== false;
    }

    /**
     * Returns formatted bad request.
     *
     * @param string $message
     * @param string $type
     * @param string $code
     * @param string $class
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function badRequest($message, $type, $code, $class, $statusCode)
    {
        $payload = [
            'error' => $message,
            'type'  => $type,
            'code'  => $code,
            'class' => $class
        ];

        return $this->jsonResponse($payload, $statusCode);
    }

    /**
     * Returns json response.
     *
     * @param array $payload
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponse(array $payload, $statusCode)
    {
        $payload = $payload ?: [];

        return response()->json($payload, $statusCode);
    }
    
    /**
     * Checks whether the specified exception is an AWS exception.
     *
     * @param \Throwable $e
     * @return boolean
     */
    public function isAwsException(Throwable $e)
    {
        return $e instanceof AwsException;
    }

    /**
     * Checks whether the specified exception is a Validation exception.
     *
     * @param \Throwable $e
     * @return boolean
     */
    public function isValidationException(Throwable $e)
    {
        return $e instanceof ValidationException;
    }

    /**
     * Checks whether the specified exception is a GuzzleHttp Request exception.
     *
     * @param \Throwable $e
     * @return boolean
     */
    public function isRequestException(Throwable $e)
    {
        return $e instanceof RequestException;
    }

    /**
     * Checks whether the specified exception is a LaravelPlus generic exception.
     *
     * @param \Throwable $e
     * @return boolean
     */
    public function isLaravelPlusException(Throwable $e)
    {
        return $e instanceof LPException;
    }

    /**
     * Handles request exception.
     *
     * @param \Throwable $e
     * @return boolean
     */
    public function requestExceptionResponse(RequestException $e)
    {
        $res = $e->getResponse();
        if (empty($res)) {
            return $this->generalExceptionResponse($e);
        }

        $errorMsg = empty($res)
                    ? $e->getMessage()
                    : sprintf("%s %s", $res->getStatusCode(), $res->getReasonPhrase());

        return $this->badRequest(
            $errorMsg,
            $this->getExceptionTypeFromStatus($res->getStatusCode()),
            $e->getCode(),
            get_class($e),
            $res->getStatusCode()
        );
    }

    /**
     * Handles validation exception.
     *
     * @param \Illuminate\Validation\ValidationException $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function validationExceptionResponse(ValidationException $e)
    {
        $message = null;

        $errors = $e->errors();
        if (!empty($errors)) {
            $message = $this->prettifyValidationError($errors);
        }

        return $this->badRequest(
            !empty($message) ? $message : $e->getMessage(),
            'client',
            $e->getCode(),
            get_class($e),
            $e->status
        );
    }

    /**
     * Handles general HTTP exception.
     *
     * @param \Symfony\Component\HttpKernel\Exception\HttpException $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function httpExceptionResponse(HttpException $e)
    {
        return $this->badRequest(
            empty($e->getMessage()) ? $this->getClassName(1, $e) : $e->getMessage(),
            $this->getExceptionTypeFromStatus($e->getStatusCode()),
            $e->getCode(),
            get_class($e),
            $e->getStatusCode()
        );
    }

    /**
     * Prettyfies validation error.
     * 
     * The array of errors is in the following format:
     * [
     *    'field_name' => [
     *      'error message 1',
     *      'error message 2',
     *   ],
     * ]
     *
     * @param array $errors
     * @return string
     */
    private function prettifyValidationError(array $errors)
    {
        // Messages are set by resources/lang/en/validation.php file.
        $messages = Arr::first($errors);
        return Arr::first($messages);
    }

    /**
     * Handles AWS exception.
     *
     * @param \Aws\Exception\AwsException $e
     * @return \Illuminate\Http\JsonResponse
     */
    private function awsExceptionResponse(AwsException $e)
    {
        return $this->badRequest(
            $e->getAwsErrorMessage(),
            $e->getAwsErrorType(),
            $e->getAwsErrorCode(),
            get_class($e),
            $e->getStatusCode()
        );
    }

    /**
     * Handles generic exception.
     *
     * @param \NizarBlond\LaravelPlus\Exceptions\Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    private function laravelPlusExceptionResponse(LPException $e)
    {
        return $this->generalExceptionResponse($e);
    }

    /**
     * Handles generic exception.
     *
     * @param \Throwable $e
     * @return \Illuminate\Http\JsonResponse
     */
    private function generalExceptionResponse(Throwable $e)
    {
        return $this->badRequest(
            $e->getMessage(),
            'server',
            $e->getCode(),
            get_class($e),
            500
        );
    }

    /**
     * Returns exception type from response status.
     *
     * @param int $statusCode
     * @return int
     */
    private function getExceptionTypeFromStatus($statusCode)
    {
        $level = (int) floor($statusCode / 100);
        if ($level === 5) {
            return 'server';
        } else if ($level === 4) {
            return 'client';
        }
        return 'n/a';
    }
}
