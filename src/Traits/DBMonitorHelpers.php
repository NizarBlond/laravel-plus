<?php

namespace NizarBlond\LaravelPlus\Traits;

use NizarBlond\LaravelPlus\Support\Str;

trait DBMonitorHelpers
{
    /**
     * The executed DB queries.
     *
     * @var array
     */
    public static $sqlQueries = [];

    /**
     * Initializes the command right before execution.
     *
     * @return void
     */
    protected function registerDBMonitorQueryListener()
    {
        // https://laravel.com/api/5.5/Illuminate/Database/Events/QueryExecuted.html
        \Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
            static::$sqlQueries[] = [ $query->time, $query->sql, $query->bindings ];
        });
    }

    /**
     * Prints DB monitor statistics.
     *
     * @return  void
     */
    protected function printDBMonitorStats()
    {
        // DB queries
        if (!empty(static::$sqlQueries)) {
            $totalExecTime = 0;
            $this->print(sprintf('Executed %s DB queries:', count(static::$sqlQueries)));
            $this->print(' ');
            foreach (static::$sqlQueries as $sqlData) {
                // Format the query.
                $line = sprintf('[%sms] %s', $sqlData[0], $sqlData[1]);
                // Add the bindings if any.
                if (!empty($sqlData[2]) && count($sqlData[2]) > 0) {
                    $sqlData[2] = array_map(function ($value) {
                        return Str::limit($value ?? '', 25, '...');
                    }, $sqlData[2]);
                    $line .= ' (' . implode(', ', $sqlData[2]) . ')';
                }
                $this->print($line);
                $totalExecTime += $sqlData[0];
            }
            $this->print(' ');
            $this->print(sprintf('Total DB queries execution time: %sms', $totalExecTime));
        } else {
            $this->print('No DB queries executed.');
        }
        $this->print(' ');
    }
}
