<?php

namespace NizarBlond\LaravelPlus\Traits;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Primitive;
use NizarBlond\LaravelPlus\Support\Cache;
use League\Fractal\Manager;
use NizarBlond\LaravelPlus\Abstracts\ModelBase;
use NizarBlond\LaravelPlus\Serializers\DataArraySerializer;
use Exception;
use Auth;

trait FractalHelpers
{
    /**
     * The fractal manager.
     *
     * @var League\Fractal\Manager
     */
    private $fractalManager = null;

    /**
     * The model to transformer mapper.
     *
     * @var League\Fractal\Manager
     */
    private $transformers = [];

    /**
     * Registers a mapping from existing models to their transformers
     * based on naming conventions.
     *
     * @return void
     */
    protected function registerDefaultModelTransformers()
    {
        $modelFiles = array_map(function ($file) {
            return basename($file, '.php');
        }, glob(app()->path.'/Models/*.php', GLOB_BRACE));

        foreach ($modelFiles as $modelName) {
            if (file_exists(app()->path."/Transformers/{$modelName}Transformer.php")) {
                $this->registerTransformer($modelName);
            }
        }
    }

    /**
     * Registers a mapping from model to transformer.
     *
     * @param string $model
     * @param string $transformer
     *
     * @return void
     */
    protected function registerTransformer($model, $transformer = null)
    {
        $modelsNs = 'App\Models';
        $modelWithNs = $modelsNs.'\\'.$model;

        $transformersNs = '\App\Transformers';
        $transformerWithNs = $transformersNs .'\\'.(!empty($transformer) ? $transformer : "{$model}Transformer");

        $this->transformers[$modelWithNs] = $transformerWithNs;
    }

    /**
     * Initializes fractal manager instance.
     *
     * @return void
     */
    protected function initFractalManager()
    {
        if (empty($this->fractalManager)) {
            $this->fractalManager = new Manager();
            $this->fractalManager->setSerializer(new DataArraySerializer());
        }
    }
    
    /**
     * Guesses the transformer of the given item(s).
     *
     * @param Model|Collection $item The model or collection item.
     *
     * @return \League\Fractal\Resource\ResourceAbstract
     */
    protected function dataToFractalResource($item)
    {
        if ($item instanceof EloquentCollection) {
            if ($item->isEmpty()) {
                $resource =  new Collection([]);
            } else {
                $className = get_class($item->first());
                $resource = new Collection($item, $this->getTransformer($className));
            }
        } else if ($item instanceof EloquentModel) {
            $className = get_class($item);
            $resource = new Item($item, $this->getTransformer($className));
        } else {
            $resource = new Primitive($item);
        }

        return $resource;
    }

    /**
     * Includes available resources.
     *
     * @param string $path The resource path(s).
     *
     * @return void
     */
    protected function includeFractalResource($path)
    {
        $this->fractalManager->parseIncludes(is_array($path) ? join(",", $path) : $path);
    }

    /**
     * Excludes default resources.
     *
     * @param string|array $path The resource path(s).
     *
     * @return void
     */
    protected function excludeFractalResource($path)
    {
        $this->fractalManager->parseExcludes(is_array($path) ? join(",", $path) : $path);
    }

    /**
     * Returns the transformer of the specified class.
     *
     * @param string $className
     *
     * @return void
     */
    protected function getTransformer($className)
    {
        if (! isset($this->transformers[$className])) {
            return null;
        }

        return new $this->transformers[$className];
    }

    /**
     * Returns the transformed data.
     *
     * @param Item|Collection   $resource
     *
     * @return array
     */
    protected function createFractalData($resource)
    {
        if (empty($resource)) {
            return [];
        }

        if ($resource instanceof Primitive) {
            return $resource->getData();
        }

        $model = $resource->getData();
        $scope = $this->fractalManager->createData($resource);

        if ($model instanceof ModelBase && $model->fractalModelCache) {
            // Note that we use $model because $this is ControllerBase
            $data = $model->getModelFractalDataFromCache($model);
            if (empty($data)) {
                $data = $scope->toArray();
                $model->cacheModelFractalData($data);
            }
            return $data;
        }

        return $scope->toArray();
    }

    /**
     * Converts an array of resource values to fractal data.
     *
     * @param array $data
     *
     * @return array
     */
    protected function createCollectionFractalData($data)
    {
        if ($data instanceof EloquentCollection) {
            $dataAsArray = [];
            foreach ($data as $item) {
                $dataAsArray[] = $item;
            }
            $data = $dataAsArray;
        }

        return array_map(function ($item) {
            if ($item instanceof EloquentCollection) {
                return $this->createCollectionFractalData($item);
            }
            $resource = $this->dataToFractalResource($item);
            return $this->createFractalData($resource);
        }, $data);
    }

    /**
     * Transform collection of models.
     *
     * @return array
     */
    protected function transformItems($items)
    {
        return $this->createCollectionFractalData($items);
    }

    /**
     * Transforms model instance.
     *
     * @return array
     */
    protected function transformItem($item)
    {
        $resource = $this->dataToFractalResource($item);
        if (! empty($resource)) {
            $data = $this->createFractalData($resource);
            return $data;
        }
        return $this->exception("Failed to transform item.");
    }

    // THE FOLLOWING ARE PUBLIC BECAUSE THEY CAN BE OVERRIDDEN

    /**
     * Returns model fractal data from cache.
     *
     * @return array
     */
    public function getModelFractalDataFromCache()
    {
        $this->assertCanUseModelCache();
        $cacheKey = $this->getModelFractalDataCacheKey();
        return Cache::get($cacheKey);
    }

    /**
     * Removes model fractal data from cache.
     *
     * @return array
     */
    public function removeModelFractalDataFromCache()
    {
        $this->assertCanUseModelCache();
        $cacheKey = $this->getModelFractalDataCacheKey();
        $prefix = preg_replace('/:\d+$/', ':', $cacheKey);
        Cache::removePrefix($prefix);
    }

    /**
     * Returns model fractal data from cache.
     *
     * @param array $data
     *
     * @return bool
     */
    public function cacheModelFractalData(array $data)
    {
        $this->assertCanUseModelCache();
        $ttl = $this->getModelFractalDataCacheTtl();
        $cacheKey = $this->getModelFractalDataCacheKey();
        if (!Cache::put($cacheKey, $data, $ttl)) {
            $this->logWarning(sprintf('Failed to cache model fractal data (key=%s)', $cacheKey));
        }
    }

    /**
     * Returns fractal data cache TTL in minutes.
     *
     * @return int
     */
    public function getModelFractalDataCacheTtl()
    {
        return 3600 * 24;
    }

    /**
     * Returns model fractal data from cache.
     *
     * @return array
     */
    public function getModelFractalDataCacheKey()
    {
        $this->assertCanUseModelCache();
        $user = Auth::guard()->user();

        return sprintf(
            '_mfd_%s:%s:%s',
            $this->getClassName(1),
            $this->id,
            !empty($user) ? $user->id : 0
        );
    }

    /**
     * Asserts that the current class can use model fractal data cache.
     *
     * @return void
     */
    private function assertCanUseModelCache()
    {
        if (!($this instanceof ModelBase)) {
            $this->exception('This method can be used only by ModelBase instances.');
        }
    }
}
