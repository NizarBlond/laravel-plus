<?php

namespace NizarBlond\LaravelPlus\Traits;

trait Singleton
{
    /**
     * The class singleton instance
     *
     * @var object
     */
    private static $_instance = null;

    /**
     * The constructor.
     *
     * @return object The class singleton.
     */
    final private function __construct()
    {
        $this->init();
    }
    
    /**
     * Returns the class singleton instance
     *
     * @return object The class singleton.
     */
    final public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }

    /**
     * Initializes the instance.
     *
     * @return object The class singleton.
     */
    protected function init()
    {
        //
    }
}
