<?php

namespace NizarBlond\LaravelPlus\Traits;

use NizarBlond\LaravelPlus\Support\Dir;
use NizarBlond\LaravelPlus\Support\File;
use NizarBlond\LaravelPlus\Exceptions\Exception;

trait SafeFileHelpers
{
    /**
     * The runtime log file on filesystem.
     * 
     * @var string
     */
    private $_sfhRuntimeLog = false;

    /**
     * Writes runtime log.
     *
     * @param bool $status
     * @param string $message
     * @param array|null $trace
     * @return void
     */
    private function _sfhWriteLog(bool $status, string $message, ?array $trace = null)
    {
        if (!$this->_sfhRuntimeLog) {
            return;
        }
        
        if (!File::exists($this->_sfhRuntimeLog)) {
            Dir::makeIfNotExists(dirname($this->_sfhRuntimeLog));
            File::touch($this->_sfhRuntimeLog, 0777);
        }
    }

    /**
     * Deletes the specified file.
     *
     * @param string $file
     * @param bool $trash
     * @return bool
     */
    protected function deleteFile(string $file, bool $trash = true, bool $checkProtected = false)
    {

    }
}
