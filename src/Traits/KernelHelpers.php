<?php

namespace NizarBlond\LaravelPlus\Traits;

trait KernelHelpers
{
    use InstanceHelpers;

    /**
     * The timezone for _schedule() function.
     *
     * @var string
     */
    protected $_timezone = null;

    /**
     * The offset for offsetCronMinutes() function.
     *
     * @var int
     */
    protected $_offset = 0;

    /**
     * A mapper from AWS Region to Timezone.
     *
     * @return void
     */
    protected function awsRegionToTimezone($region)
    {
        // Set event timezone
        switch ($region) {
            case 'ap-southeast-2':
                return 'Australia/Sydney';
            case 'eu-central-1':
                return 'Europe/Berlin';
            case 'eu-west-1':
                return 'Europe/Dublin';
            case 'us-east-1':
            case 'us-east-2':
                return 'US/Eastern';
            default:
                return null;
        }
    }

    /**
     * A wrapper for schedule function.
     *
     * @param Schedule $schedule
     * @param string $command
     * @param string $timeFunc
     * @param string|null $arg1
     * @param string|null $arg2
     * @param bool $inBackground
     * @param string|null $timezone
     * @return void
     */
    protected function _schedule(&$schedule, $command, $timeFunc, $arg1 = null, $arg2 = null, $inBackground = false, $timezone = null)
    {
        // Create event
        $event = $schedule->command($command);

        // Set as background
        if ($inBackground) {
            $event = $event->runInBackground();
        }

        // Schedule event
        switch ($timeFunc) {
            case 'everyMinute':
                $event = $event->everyMinute();
                break;
            case 'dailyAt':
                $event = $event->dailyAt($arg1 ?? $this->exception('Invalid dailyAt time.'));
                break;
            case 'hourlyAt':
                $event = $event->hourlyAt($arg1 ?? $this->exception('Invalid hourlyAt time.'));
                break;
            case 'everyNMinutesAt':
                $event = $event->everyMinute()->when(function () use ($arg1, $arg2) {
                   return (intval(date('i')) - $arg2) % $arg1 === 0; // Every $arg1 minutes: offset $arg2
                });
                break;
            case 'weekly':
                $event = $event->weekly();
                break;
            case 'cron':
                $event = $event->cron($arg1 ?? $this->exception('Invalid cron expression.'));
                break;
            default:
                $this->exception('Invalid time function.');
        }

        // Set event timezone
        if ($timezone) {
            $event->timezone($timezone);
        } elseif ($this->_timezone) {
            $event->timezone($this->_timezone);
        }
    }

    /**
     * A wrapper for schedule cron.
     *
     * @param Schedule $schedule
     * @param string $command
     * @param string $expression
     * @return void
     */
    protected function _scheduleCron(&$schedule, $command, $expression, $timezone = null)
    {
        if ($this->_offset) {
            $expression = $this->offsetCronMinutes($expression, $this->_offset);
        }
        $this->_scheduleInBackground($schedule, $command, 'cron', $expression, null, $timezone);
    }

    /**
     * A wrapper for schedule function that runs in background.
     *
     * @param Schedule $schedule
     * @param string $command
     * @param string $timeFunc
     * @param string|null $arg1
     * @param string|null $arg2
     * @return void
     */
    protected function _scheduleInBackground(&$schedule, $command, $timeFunc, $arg1 = null, $arg2 = null, $timezone = null)
    {
        $this->_schedule($schedule, $command, $timeFunc, $arg1, $arg2, true, $timezone);
    }

    /**
     * Offset cron by minutes.
     * 
     * @param string $cron
     * @param int $offset
     * @return string
     */
    protected function offsetCronMinutes(string $cron, int $offset): string
    {
        $cronParts = explode(' ', $cron);
        if (count($cronParts) !== 5) {
            $this->exception('Invalid cron format');
        } elseif ($offset < 0 || $offset > 59) {
            $this->exception('Invalid offset');
        } elseif ($offset === 0 || $cronParts[0] === '*' || $cronParts[0] === '*/1') {
            return $cron;
        } elseif (preg_match('/^\d+$/', $cronParts[0])) {
            // Convert 5 to 10
            $cronParts[0] = ($cronParts[0] + $offset) % 60;
        } elseif (preg_match('/^\d+-\d+$/', $cronParts[0])) {
            // Convert 5-10 to 5-15
            $cronParts[0] = implode('-', array_map(function($part) use ($offset) {
                return ($part + $offset) % 60;
            }, explode('-', $cronParts[0])));
        } elseif (preg_match('/^\*\/\d+$/', $cronParts[0])) {
            // Convert */5 to 0,5,10,15,20,25,30,35,40,45,50,55
            $cronParts[0] = implode(',', array_map(function($part) use ($offset) {
                return ($part + $offset) % 60;
            }, range(0, 59, intval(substr($cronParts[0], 2)))));
        } elseif (preg_match('/^\d+(,\d+)+$/', $cronParts[0])) {
            // Convert 5,10,15 to 10,15,20
            $cronParts[0] = implode(',', array_map(function($part) use ($offset) {
                return ($part + $offset) % 60;
            }, explode(',', $cronParts[0])));
        } else {
            return $cron;
        }
        return implode(' ', $cronParts);
    }
}
