<?php

namespace NizarBlond\LaravelPlus\Traits;

use NizarBlond\LaravelPlus\Support\Log;
use NizarBlond\LaravelPlus\Support\Arr;
use NizarBlond\LaravelPlus\Support\Time;
use NizarBlond\LaravelPlus\Config\ActivityLogger as ALConfig;
use NizarBlond\LaravelPlus\Config\Log as LogConfig;
use NizarBlond\LaravelPlus\Exceptions\Exception;

trait ClassHelpers
{
    /**
     * Whether to write debug log.
     *
     * @var bool
     */
    protected static $debugMode = false;
    /**
     * Retrieves the log data shared by the request.
     * 
     * @return array
     */
    public static function getLogData()
    {
        return Log::getLogData();
    }

    /**
     * Adds log data shared by the request.
     * 
     * @param string $message
     * @param array $context
     * @param string $level
     * @return void
     */
    public static function addLogData($message, $context = [], $level = 'n/a')
    {
        Log::addLogData($message, $context, $level);
    }

    /**
     * Logs the given message and context.
     * 
     * @param string $message
     * @param mixed $context
     * @param string $level
     * @return array
     */
    public static function log($message, $context = [], string $level = "info")
    {
        if (Log::compareLevels(LogConfig::getMinLoggerLevel(), $level) < 0) {
            return;
        }

        if (!is_array($context)) {
            if (empty($context)) {
                $context = [];
            } else if (is_object($context)) {
                $context = (array) $context;
            } else {
                self::exception("Log context must be array/object.");
            }
        }

        Log::log($message, $context, $level);
        self::addLogData($message, $context, $level);
    }

    /**
     * Logs the given message and context as error.
     * 
     * @param string $message
     * @param mixed $context
     * @return array
     */
    public static function logError($message, $context = [])
    {
        return self::log($message, $context, 'error');
    }

    /**
     * Logs the given message and context as warning.
     * 
     * @param string $message
     * @param mixed $context
     * @return array
     */
    public static function logWarning($message, $context = [])
    {
        return self::log($message, $context, 'warning');
    }

    /**
     * Prints the given message and context.
     * 
     * @param string $message
     * @param mixed $context
     * @param string|null $level
     * @return array
     */
    public static function print($message, $context = [], ?string $level = null)
    {
        Log::print($message, $context, $level);
    }

    /**
     * Prints a debug message and data.
     *
     * @param string $message
     * @param array $data
     * @param string $level
     * @return void
     */
    public static function debug(string $message, array $data = [], string $level = 'debug')
    {
        if (self::$debugMode) {
            $message = sprintf('[%s] %s', Time::now()->toDateTimeString(), $message);
            self::print($message, $data, $level);
        }
    }

    /**
     * Sanitize the given value.
     * 
     * @param mixed $value
     * @return mixed
     */
    public static function sanitizeArray($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        // Remove present keys (see 'keys_to_remove' in config file)
        $value = Arr::removeKeysRecursive(
            $value,
            ALConfig::keysToRemove()
        );

        // Hide present keys (see 'keys_to_hide' in config file)
        $value = Arr::modifyKeyValuesRecursive(
            $value,
            ALConfig::keysToHide(),
            function ($value) {
                return '[key-hidden-from-log]';
            }
        );

        // Remove content with non-UTF-8 encoding
        $encoding = 'UTF-8';
        $replace = '[removed-from-log:bad-encoding]';
        $value = Arr::walkRecursive($value, function ($v, $isKey) use ($encoding, $replace) {
            if ($isKey || !is_string($v)) {
                return $v;
            }
            if (!mb_detect_encoding($v, $encoding, true)) {
                return $replace;
            }
            return $v;
        });

        return $value;
    }

    /**
     * Throws an exception with the given message and data.
     * 
     * @param string $message
     * @param mixed $data
     * @return array
     */
    public static function exception($message, $data = null)
    {
        throw new Exception($message, $data);
    }

    /**
     * Retrieves the class name.
     * 
     * @param int $lastNParts
     * @param mixed $instanceThis
     * @return string
     */
    public static function getClassName($instanceThis, $lastNParts = null)
    {
        // Get full name with namespace
        $fullname = get_class($instanceThis);

        // If last N parts is null then return full name
        if (is_null($lastNParts)) {
            return $fullname;
        }

        // Get namespace parts
        $nsParts = explode("\\", $fullname);

        // Slice parts
        $lastNParts = array_slice($nsParts, -$lastNParts);

        // Concat last N parts
        return implode("\\", $lastNParts);
    }

    /**
     * Retrieves the calling method.
     * 
     * @param int $index
     * @param int $limit
     * @param array $stack
     * @return array
     */
    public static function getCallingMethod($index = null, $limit = 10, &$stack = null)
    {
        $stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $limit);
        if (empty($index)) {
            return $stack;
        }
        return $stack[$index] ?? null;
    }

    /**
     * Guesses whether the app is running in a testing environment.
     *
     * @return array
     */
    public static function isTestingOrLocal()
    {
        return in_array(env('APP_ENV', 'local'), [ 'local','testing', 'dev' ]);
    }
}
