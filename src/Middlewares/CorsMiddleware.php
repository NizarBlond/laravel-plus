<?php

namespace NizarBlond\LaravelPlus\Middlewares;

use Closure;
use Illuminate\Http\Response;
use NizarBlond\LaravelPlus\Config\LaravelPlus as LPConfig;
use NizarBlond\LaravelPlus\Traits\InstanceHelpers;

/**
 * CorsMiddleware.
 *
 * For some reason fruitcake/laravel-cors does not support preflight mode handling.
 * This is a temporary solution for the issue.
 *
 * Reference:
 * https://stackoverflow.com/questions/46996130/lumen-api-cors-ajax-405-method-not-allowed/47012740#47012740
 *
 */
class CorsMiddleware
{
    use InstanceHelpers;

    /**
     * The cors settings.
     * 
     * @var array
     */
    protected $settings;

    /**
     * Constructor.
     *
     * @param   array     $config
     * @param   array     $extraLog
     *
     * @return  void
     */
    public function __construct()
    {
        $this->settings = LPConfig::cors();
    }

    /**
     * Handles an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return \Illuminate\Http\Response
     */
    public function handle($request, Closure $next)
    {
        // Return preflight response for prefight request
        if ($this->isPreflightRequest($request)) {
            $response = $this->handlePreflightRequest($request);
            $this->setVaryHeader($response, 'Access-Control-Request-Method');
            return $response;
        }

        // Handle request
        $response = $next($request);

        // Configure CORS headers
        return $this->setCorsHeaders($request, $response);
    }

    /**
     * Sets cors header and handles preflight mode.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Illuminate\Http\Response $response
     *
     * @return void
     */
    protected function setCorsHeaders($request, $response)
    {
        if ($request->isMethod('OPTIONS')) {
            $this->setVaryHeader($response, 'Access-Control-Request-Method');
        }

        $this->setAllowedOrigin($request, $response);
        $this->setExposeHeaders($request, $response);
        $this->setAllowCredentials($request, $response);

        return $response;
    }

    /**
     * Sets the origin.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed                    $response
     *
     * @return void
     */
    protected function setAllowedOrigin($request, $response)
    {
        $origin = $this->settings['origin'];
        if (is_callable($origin)) {
            $origin = call_user_func(
                $origin,
                $this->setRequestHeader($request, 'Origin')
            );
        }
        $this->setResponseHeader($response, 'Access-Control-Allow-Origin', $origin);
    }

    /**
     * Sets expose headers.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Illuminate\Http\Response $response
     *
     * @return void
     */
    protected function setExposeHeaders($request, $response)
    {
        if (!empty($this->settings['exposeHeaders'])) {
            $exposeHeaders = $this->settings['exposeHeaders'];
            if (is_array($exposeHeaders)) {
                $exposeHeaders = implode(', ', $exposeHeaders);
            }
            $this->setResponseHeader($response, 'Access-Control-Expose-Headers', $exposeHeaders);
        }
    }

    /**
     * Sets max age.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Illuminate\Http\Response $response
     *
     * @return void
     */
    protected function setMaxAge($request, $response)
    {
        if (isset($this->settings['maxAge'])) {
            $this->setResponseHeader($response, 'Access-Control-Max-Age', $this->settings['maxAge']);
        }
    }

    /**
     * Sets allow credentials.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Illuminate\Http\Response $response
     *
     * @return void
     */
    protected function setAllowCredentials($request, $response)
    {
        if (isset($this->settings['allowCredentials']) && $this->settings['allowCredentials'] === true) {
            $this->setResponseHeader($response, 'Access-Control-Allow-Credentials', 'true');
        }
    }

    /**
     * Sets allow methods.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Illuminate\Http\Response $response
     *
     * @return void
     */
    protected function setAllowMethods($request, $response)
    {
        if (!empty($this->settings['allowMethods'])) {
            $allowMethods = $this->settings['allowMethods'];
            if (is_array($allowMethods)) {
                $allowMethods = implode(', ', $allowMethods);
            }
            $this->setResponseHeader($response, 'Access-Control-Allow-Methods', $allowMethods);
        }
    }

    /**
     * Set allow headers.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Illuminate\Http\Response $response
     *
     * @return void
     */
    protected function setAllowHeaders($request, $response)
    {
        if (!empty($this->settings['allowHeaders'])) {
            $allowHeaders = $this->settings['allowHeaders'];
            if (is_array($allowHeaders)) {
                $allowHeaders = implode(', ', $allowHeaders);
            }
        } else {
            $allowHeaders = $this->setRequestHeader($request, 'Access-Control-Request-Headers');
        }

        $this->setResponseHeader($response, 'Access-Control-Allow-Headers', $allowHeaders);
    }

    /**
     * Sets the response header.
     *
     * @param mixed     $response
     * @param string    $key
     * @param string    $value
     *
     * @return void
     */
    private function setResponseHeader($response, $key, $value)
    {
        if (!$response->headers->has($key)) {
            $response->headers->set($key, $value);
        }
    }

    /**
     * Sets the request header.
     *
     * @param mixed     $request
     * @param string    $key
     * @param string    $default
     *
     * @return void
     */
    private function setRequestHeader($request, $key, $default = null)
    {
        if (!$request->headers->has($key)) {
            $request->headers->set($key, $default);
        }
    }

    /**
     * Tells whether the request is preflight.
     *
     * @param mixed $request
     *
     * @return bool
     */
    private function isPreflightRequest($request)
    {
        return  $request->getMethod() === 'OPTIONS' &&
                $request->headers->has('Access-Control-Request-Method');
    }

    /**
     * Handles the preflight request.
     *
     * @param mixed $request
     *
     * @return mixed
     */
    private function handlePreflightRequest($request)
    {
        $response = new Response();
        $response->setStatusCode(204);
        return $this->addPreflightRequestHeaders($response, $request);
    }

    /**
     * Adds the preflight request headers.
     *
     * @param mixed $response
     * @param mixed $request
     *
     * @return mixed
     */
    private function addPreflightRequestHeaders($response, $request)
    {
        $this->setAllowedOrigin($request, $response);
        $this->setMaxAge($request, $response);
        $this->setAllowCredentials($request, $response);
        $this->setAllowMethods($request, $response);
        $this->setAllowHeaders($request, $response);
        return $response;
    }

    /**
     * Adds a Vary response header.
     *
     * @param mixed $response
     * @param string $header
     *
     * @return mixed
     */
    private function setVaryHeader($response, $header)
    {
        if (!$response->headers->has('Vary')) {
            $response->headers->set('Vary', $header);
        } elseif (!in_array($header, explode(', ', (string) $response->headers->get('Vary')))) {
            $response->headers->set('Vary', ((string) $response->headers->get('Vary')) . ', ' . $header);
        }
        return $response;
    }
}
