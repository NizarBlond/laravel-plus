<?php

namespace NizarBlond\LaravelPlus\Commands;

use NizarBlond\LaravelPlus\Support\Proc;
use NizarBlond\LaravelPlus\Support\Dir;
use NizarBlond\LaravelPlus\Support\Que;
use NizarBlond\LaravelPlus\Models\ActivityLog;
use NizarBlond\LaravelPlus\Jobs\EmptyJob;
use Queue;
use DB;

class DiagnoseSystem extends DiagnoseBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-plus:diagnose-system
        {--debug}
        {--tests=fs,db,que}
        {--scripts-dir=}
        {--queues=default}
        {--db-total-size-warn-mb=100}
        {--db-tbl-size-warn-mb=10}
        {--fs-dirs=/var/log/nginx,/tmp}
        {--fs-dir-size-warn-mb=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs a generic diagnosis of the system.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function process()
    {
        // Run the provided tests
        $testsToRun = explode(',', $this->option('tests'));
        foreach ($testsToRun as $testName) {
            switch ($testName) {
                case 'que':
                case 'queue':
                    $this->jobQueues();
                    break;
                case 'db':
                case 'database':
                    $this->databaseStatus();
                    break;
                case 'fs':
                case 'filesystem':
                    $this->fsDiskspace();
                    break;
                default:
                    $this->warn("The '$testName' test is not available.");
                    break;
            }
        }

        // Always run custom scripts
        $this->customScripts();
    }

    private function fsDiskspace()
    {
        $this->headline("FILE SYSTEM DISKSPACE");

        $fsDirs = $this->option('fs-dirs');
        if (empty($fsDirs)) {
            $this->debug('No directories provided.');
            return;
        }

        $fsDirsAsArr = explode(',', $fsDirs);
        foreach ($fsDirsAsArr as $fsDir) {
            if (!Dir::exists($fsDir)) {
                $this->warn("$fsDir does not exist.");
                continue;
            }
            $sizeInByte = Dir::size($fsDir);
            $sizeInMB = $this->numberFormat($sizeInByte / (1024*1024));
            $reportMsg = "$fsDir size={$sizeInMB}MB";
            if ($sizeInMB > $this->option('fs-dir-size-warn-mb')) {
                $this->warning($reportMsg);
            } else {
                $this->success($reportMsg);
            }
        }
    }

    private function databaseStatus()
    {
        $this->headline("DATABASE STATUS");

        $tablesInfo = DB::select('SHOW TABLE STATUS');
        $totalDbSize = 0;
        
        // Sort tables by size descending
        usort($tablesInfo, function($a, $b) {
            return ($b->Data_length + $b->Index_length) - ($a->Data_length + $a->Index_length);
        });

        // Tables
        foreach ($tablesInfo as $tbl) {
            $tblSizeInMB = $this->numberFormat(($tbl->Data_length + $tbl->Index_length) / (1024*1024));
            $totalDbSize += $tblSizeInMB;
            $this->debug("Table: {$tbl->Name}");
            $this->debug("Size: {$tblSizeInMB}MB");
            $this->debug("Rows: {$tbl->Rows}");
            $this->debug("================================");
            $reportMsg = "$tbl->Name table size={$tblSizeInMB}MB (has {$tbl->Rows} rows)";
            if ($tblSizeInMB > $this->option('db-tbl-size-warn-mb')) {
                $this->warning($reportMsg);
            } else {
                $this->success($reportMsg);
            }
        }

        // Database
        $reportMsg = "total database size={$totalDbSize}MB";
        if ($totalDbSize > $this->option('db-total-size-warn-mb')) {
            $this->warning($reportMsg);
        } else {
            $this->success($reportMsg);
        }
    }

    private function jobQueues()
    {
        $queues = explode(',', $this->option('queues'));
        if (empty($queues)) {
            $this->debug('No job queues provided.');
            return;
        }

        $this->headline("JOB QUEUES");

        foreach ($queues as $queue) {
            $size = Queue::size($queue);
            $this->debug("Queue: $queue");
            $this->debug("Jobs: $size");
            $this->debug("Info:", Que::info($queue));
            $this->debug("================================");
            if ($size > 10) {
                $this->warn("$queue queue size=$size (probably stuck).");
            } else {
                $this->success("$queue queue size=$size.");
            }
            $this->debug("Testing '$queue' functionality...");
            $job = new EmptyJob;
            $waitFor = 3;
            $this->debug("Dispatching an empty job...");
            Que::dispatch($job, $queue);
            sleep($waitFor);
            $this->debug("Waiting for $waitFor seconds until job (guid={$job->getGuid()}) is completed...");
            $jobLog = ActivityLog::getByGuid($job->getGuid())->first();
            if (empty($jobLog)) {
                $this->failure("job failed to be run by '$queue' queue.");
            } else {
                $this->success("job has been run successfully by '$queue' queue.");
            }
        }
    }

    private function customScripts()
    {
        $testsDir = $this->option('scripts-dir');
        if (empty($testsDir)) {
            $this->debug('No custom scripts provided.');
            return;
        }

        $this->headline("CUSTOM SCRIPTS (dir=$testsDir)");

        $testFiles = Dir::files($testsDir, "sh");
        foreach ($testFiles as $testFile) {
            $this->debug("Executing '$testFile' test script...");
            $output = Proc::runScript($testFile);
            echo $output;
        }
    }
}
