<?php

namespace NizarBlond\LaravelPlus\Commands;

use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Support\Proc;
use NizarBlond\LaravelPlus\Support\File;
use NizarBlond\LaravelPlus\Support\Dir;
use NizarBlond\LaravelPlus\Support\Que;
use NizarBlond\LaravelPlus\Abstracts\CommandBase;
use NizarBlond\LaravelPlus\Models\ActivityLog;
use NizarBlond\LaravelPlus\Jobs\EmptyJob;
use Queue;
use DB;

class DiagnoseLaraval extends DiagnoseBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-plus:diagnose-laravel {--queues=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs a generic diagnosis of the system.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function process()
    {
        // $this->fsDiskspace();
        // $this->jobQueues();
        // $this->databaseStatus();
        // $this->customScripts();
    }
}
