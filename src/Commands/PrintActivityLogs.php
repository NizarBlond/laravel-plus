<?php

namespace NizarBlond\LaravelPlus\Commands;

use NizarBlond\LaravelPlus\Abstracts\CommandBase;
use NizarBlond\LaravelPlus\Models\ActivityLog;
use NizarBlond\LaravelPlus\Constants\ErrorLevel;
use \Carbon\Carbon;

class PrintActivityLogs extends CommandBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-plus:print-activity-logs {--ids=} {--guids=} {--last-n=10} {--type=} {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prints one or more activity logs.';

    /**
     * The activity logs to be printed.
     *
     * @var Collection
     */
    private $activityLogs = null;

    /**
     * Initializes the command right before execution.
     *
     * @return void
     */
    protected function init()
    {
        $query = ActivityLog::query();

        if (!empty($this->option('ids'))) {
            $this->line("Filter by ids: ".$this->option('ids'));
            $ids = explode(',', $this->option('ids'));
            $query = ActivityLog::getByIds($ids);
        }

        if (!empty($this->option('guids'))) {
            $this->line("Filter by guids: ".$this->option('guids'));
            $guids = explode(',', $this->option('guids'));
            $query = $query->getByGuids($guids);
        }

        if (!empty($this->option('type'))) {
            $this->line("Filter by type: ".$this->option('type'));
            $type = $this->option('type');
            $query = $query->getByType($type);
        }

        if (!empty($this->option('name'))) {
            $this->line("Filter by name: ".$this->option('name'));
            $name = $this->option('name');
            $query = $query->getByName($name);
        }

        $lastN = intval($this->option('last-n'));
        if ($lastN > 0) {
            $query = $query->orderByDesc('id')->limit($lastN);
        }

        $this->activityLogs = $query->get();

        parent::init();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function process()
    {
        foreach ($this->activityLogs as $activity) {
            $message = sprintf("#%s", $activity->id);
            $data = $activity->toArray();
            $this->alert($message);
            $this->print(' ', $data);
        }
    }
}
