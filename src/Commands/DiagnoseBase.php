<?php

namespace NizarBlond\LaravelPlus\Commands;

use NizarBlond\LaravelPlus\Abstracts\CommandBase;

abstract class DiagnoseBase extends CommandBase
{
    protected function verbose($msg)
    {
        if ($this->option('debug')) {
            $this->line($msg);
        }
    }

    protected function numberFormat($number, $decimalPlaces = 2)
    {
        return number_format($number, $decimalPlaces, '.', '');
    }

    protected function headline($msg)
    {
        $this->line(''); // Empty line
        $this->alert("$msg");
    }

    protected function failure($msg)
    {
        $this->error("TEST FAILED: $msg");
    }

    protected function warning($msg)
    {
        $this->warn("TEST WARNING: $msg");
    }

    protected function success($msg)
    {
        $this->info("TEST PASSED: $msg");
    }
}
