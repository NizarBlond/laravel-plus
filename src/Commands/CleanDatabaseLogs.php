<?php

namespace NizarBlond\LaravelPlus\Commands;

use NizarBlond\LaravelPlus\Abstracts\CommandBase;
use NizarBlond\LaravelPlus\Models\ActivityLog;
use NizarBlond\LaravelPlus\Constants\ErrorLevel;
use NizarBlond\LaravelPlus\Config\LaravelPlus as LPConfig;
use \Carbon\Carbon;

class CleanDatabaseLogs extends CommandBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-plus:clean-db-logs
        {days-ago           : The number of days ago to delete the logs.}
        {--exclude-errors   : Exclude error logs.}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans database logs older than the specified days.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function process()
    {
        $days = $this->argument('days-ago');
        if (empty($days) || $days <= 0) {
            $this->error('Invalid days ago value.');
            return;
        }

        $logs = ActivityLog::where('scheduled_at', '<', Carbon::now()->subDays($days))
                            ->getByHostName(LPConfig::hostName())
                            ->getByHostId(LPConfig::hostId());

        if ($this->option('exclude-errors')) {
            $logs = $logs->where('error_level', '=', ErrorLevel::NO_ERROR);
        }

        $deleted = $logs->delete();
        $this->info(sprintf('Deleted %s logs.', $deleted));
    }
}
