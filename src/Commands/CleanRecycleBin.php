<?php

namespace NizarBlond\LaravelPlus\Commands;

use NizarBlond\LaravelPlus\Abstracts\CommandBase;
use NizarBlond\LaravelPlus\Support\File;
use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Support\Dir;
use NizarBlond\LaravelPlus\Config\File as FileConfig;
use Carbon\Carbon;

class CleanRecycleBin extends CommandBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-plus:clean-recycle-bin {days-ago} {--path=} {--dry-run} {--debug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans recycle bin files older than the specified days.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function process()
    {
        $path = $this->option('path');
        $binRoot = FileConfig::recycleBinPath();
        if (!$binRoot) {
            $this->warn('Recycle bin is not configured.');
            return;
        }

        $daysAgo = $this->argument('days-ago');
        $binPath = Str::joinPaths($binRoot, $this->option('path') ?? '');
        $files = Dir::glob($binPath, '*_1*');
        $now = Carbon::now();
        $size = 0;
        $count = 0;
        foreach ($files as $file) {
            if (!preg_match('/_(\d{9,10})/', $file, $match)) {
                $this->warn(sprintf('Ignored %s: pattern not matched.', $file));
                continue;
            }
            $deletedAt = Carbon::createFromTimestamp($match[1]);
            if ($deletedAt->clone()->addDays($daysAgo) < $now) {
                $bytes = $this->deleteFile($file);
                if (!$bytes) {
                    continue;
                }
                $size += $bytes;
                $count++;
                if ($count % 500 === 0) {
                    $total = Str::bytesToReadable($size);
                    $this->line(sprintf('So far deleted %s files and %s.', $count, $total));
                }
            }
        }

        $total = Str::bytesToReadable($size);
        $this->line(sprintf('Deleted in total %s files and %s.', $count, $total));
    }

    /**
     * Permanently delete file or folder.
     *
     * @param string $file
     * @return bool
     */
    private function deleteFile(string $file)
    {
        if (!File::exists($file)) {
            return false;
        }

        if ($this->dryRun) {
            $this->warn(sprintf('%s will be deleted.', $file));
            return 0;
        }

        $size = 0;
        $deleted = false;
        if (Dir::isDir($file)) {
            $size = Dir::size($file);
            $deleted = Dir::delete($file, false) ;
        } else {
            $size = File::size($file);
            $deleted = File::delete($file, false);
        }

        if (!$deleted) {
            $this->error(sprintf('Failed to delete %s.', $file));
            return false;
        } elseif ($this->option('debug')) {
            $this->info(sprintf('%s was deleted.', $file));
        }

        return $size;
    }
}
