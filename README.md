Laravel Plus
============

## Important Notice

This is an internal developer tool and has not been tested, thus stability is NOT guaranteed. Please use at your own risk.
I just felt like sharing it for it might be useful to someone.

### Install

#### Install Through Composer

Add the following settings to composer.json

``` json
    "require": {
        // ...
        "nizarblond/laravel-plus": "*",
        // ...
    },
    // ...
    "repositories": [
        // ...
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/NizarBlond/laravel-plus.git"
        },
        // ...
    ],
```

#### Add Service Provider

Add this service provider to your `bootstrap/app.php` file.

``` php
$app->register(NizarBlond\LaravelPlus\LaravelPlusServiceProvider::class);
```

#### Publish Config & Migrations

``` bash
php artisan vendor:publish --provider="NizarBlond\LaravelPlus\LaravelPlusServiceProvider"
```

Then, run the DB migrator so the table will be added to database.

#### Schedule DB commands

Add the following to your `app/Console/Kernel.php` 

``` php
\NizarBlond\LaravelPlus\Commands\CleanDatabaseLogs::class,
```

Then, you may schedule the job to clean your database

``` php
        // Delete all successful logs from 3 days ago
        $schedule->command('aws-builder:clean-db-logs 3 --exclude-errors')
                 ->dailyAt();

        // Delete all logs (including errors) from 7 days ago
        $schedule->command('aws-builder:clean-db-logs 7')
                 ->weekly();
```

Enjoy and don't forget, this is an incomplete package :)