<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaravelPlusJobLogsTable extends Migration
{
    /**
     * The table name.
     *
     * @var string
     */
    private $tableName = 'job_logs';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('guid')->unique();
            $table->string('name');
            $table->json('input')->nullable();
            $table->json('timeline')->nullable();
            $table->enum('status', ['pending', 'running', 'succeeded', 'failed'])->default('pending')->index();
            $table->longText('output')->nullable();
            $table->longText('error')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
