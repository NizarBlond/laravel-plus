<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaravelPlusActivityLogsTable extends Migration
{
    /**
     * The table name.
     *
     * @var string
     */
    private $tableName;

    /**
     * The constructor.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->tableName = config('laravel-plus.activity_logger.table.name');
        if (empty($this->tableName) || !is_string($this->tableName)) {
            throw new \Exception("Missing table name.");
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('scheduled_at')->nullable();
            $table->dateTime('started_at')->nullable();
            $table->string('host_id')->nullable();
            $table->string('host_name')->nullable();
            $table->string('host_user')->nullable();
            $table->string('guid')->nullable();
            $table->string('type')->nullable();
            $table->smallInteger('priority');
            $table->string('name');
            $table->string('dest_id')->nullable();
            $table->longText('input')->nullable();
            $table->longText('output')->nullable();
            $table->longText('logs')->nullable();
            $table->integer('duration_s')->nullable();
            $table->longText('error_msg')->nullable();
            $table->longText('error_stack')->nullable();
            $table->smallInteger('error_level')->nullable();

            $table->index([ 'priority' ]);
            $table->index([ 'error_level', 'type', 'name' ]);
            $table->index([ 'host_name', 'host_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
