<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Local Host Options
    |--------------------------------------------------------------------------
    |
    | The local host machine options:
    |
    | (1) id (string):      The current host ID.
    | (2) name (string):    The current host name.
    |
    */

    'host' => [

        'id' => env('HOST_ID', 'n/a'),
        'name' => env('HOST_NAME', 'n/a'),
    ],

    /*
    |--------------------------------------------------------------------------
    | CORS settings
    |--------------------------------------------------------------------------
    |
    | The cors settings.
    |
    */

    'cors' => [

        'origin' => '*',
        'allowMethods' => 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
        'exposeHeaders' => [],
        'maxAge' => 0,
        'allowCredentials' => false,
        'allowHeaders' => '*',
    ],

    /*
    |-------------------------------------------------------------------------------------
    | Logger Module Options
    |-------------------------------------------------------------------------------------
    |
    | Advanced options added to extend the Laravel Logger:
    |
    | (1) stdout (array):               The list of levels that to log them to stdout stream
    |                                   in addition to logging them to file when in debug mode
    |                                   (APP_DEBUG env variable is set to TRUE).
    |
    | (2) min_logger_level (string):    The min log level for the logger to write logs.
    |
    | (3) min_history_level (string):   The min log level for the log to be added to the history
    |                                   data captured by the instance/class helpers trait. Must
    |                                   be greater or equal than min_level.
    |
    | (4) debug_mode (bool):            If enabled, all logs will be copied to /logs/laravel-plus/*.log
    |                                   files named after the classes/instances they belong to.
    |
    | (5) log_process_id (bool):        If enabled, all logs will contain a unique identifier of the
    |                                   current PHP process. Useful to differentiate between processes.
    |
    */

    'log' => [

        'stdout' => [

            'error',
            'warning',
        ],

        'min_logger_level'  => env('LP_MIN_LOGGER_LEVEL', 'info'),
        'min_history_level' => env('LP_MIN_LOG_HISTORY_LEVEL', 'info'),
        'debug_mode'        => env('LP_DEBUG_MODE', false),
        'log_process_id'    => env('LP_LOG_PROC_ID', false),
    ],

    /*
    |--------------------------------------------------------------------------
    | Queue Module Options
    |--------------------------------------------------------------------------
    |
    | (1) The default queue name. (Leave empty to use Laravel's default).
    |
    */

    'queue' => [

        'default' => null,
    ],

    /*
    |--------------------------------------------------------------------------
    | String Settings
    |--------------------------------------------------------------------------
    |
    | Advanced settings for the Str.php class
    |
    | (1) Number of bytes in a KB.
    |
    */

    'string' => [

        'bytes_to_kilobyte' => 1024,
    ],

    /*
    |--------------------------------------------------------------------------
    | File Settings
    |--------------------------------------------------------------------------
    |
    | Advanced settings for the File.php and Dir.php classes
    |
    | (1) The recycle bin path (leave false for permanent deletion).
    |
    */

    'file' => [

        'recycle_bin_path' => env('LP_RECYCLE_BIN_PATH', false),
    ],

    /*
    |--------------------------------------------------------------------------
    | Process Module Options
    |--------------------------------------------------------------------------
    |
    | Advanced options added to extend the Proc module:
    |
    | (1) default_timeout (integer):   The default timeout for a process.
    |
    */

    'proc' => [

        'default_timeout' => 60,
    ],

    /*
    |--------------------------------------------------------------------------
    | ActivityLogger Service Options
    |--------------------------------------------------------------------------
    |
    | Options to add database logging service to various modules.
    |
    | (*) table.name (string):              The database tablename.
    |
    | (*) table.max_used_rows (int):        The max number of rows the table can have (zero or less for unlimited).
    |
    | (*) table.min_free_rows (int):        The guaranteed free rows if the table gets full.
    |
    | (*) table.check_rows_pivot (int):     The pivot that specifies the number of inserts until rows check is
    |                                       performed if max_used_rows is set.
    |
    | (*) table.max_text_column_size (int): The max long text column size (logs,input,output).
    |
    | (*) table.error_msg_no_repeat (int):  The duration in seconds to wait until the same message can be logged again.
    |
    | (*) modules (array):                  The activity types to enable the service for.
    |
    | (*) keys_to_remove (array):           The input/output column data keys to remove before saving to the database.
    |
    | (*) keys_to_hide (array):             The input/output column data keys to hide before saving to the database.
    |
    */

    'activity_logger' => [

        'table' => [

            'name' => 'laravel_plus_activity_logs',
            'max_used_rows' => env('LP_MAX_USED_ROWS', 0),
            'min_free_rows' => env('LP_MIN_FREE_ROWS', 0),
            'check_rows_pivot' => env('LP_CHECK_ROWS_PIVOT', 1000),
            'max_text_column_size' => env('LP_MAX_TEXT_COLUMN_SIZE', 10000),
            'error_msg_no_repeat' => env('LP_ERROR_MESSAGE_NO_REPEAT', 0)
        ],

        'modules' => [
            // Internal
            'proc',
            'job',
            'command',
            'http_client',
            // Custom (add yours!)
        ],

        'keys_to_remove' => [
            // 'FileContent'
        ],

        'keys_to_hide' => [
            // 'Password'
        ],
    ],
];
