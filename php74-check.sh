#!/bin/bash

CURR_DIR="$PWD"

PHP74="/Applications/mampstack-7.4.30-1/php/bin/php"

find . -name \*.php -type f -exec $PHP74 -l {} \; | grep -v "No syntax errors"

